import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatRadioChange, MatDatepickerInputEvent } from '@angular/material';
import { Alarm } from './alarm.model';
import * as moment from 'moment';

const ACKNOWLEDGE = {
  woGenerated: '',
  wo: '',
  downTime: ''
};

@Component({
  selector: 'app-acknowledge-dialog',
  templateUrl: './acknowledge-dialog.component.html',
  styleUrls: ['./acknowledge-dialog.component.scss']
})
export class AcknowledgeDialogComponent implements OnInit {
  // TODO: add interface
  public acknowledge: any = ACKNOWLEDGE;
  public startD: any = new Date();
  public booleanOptions: string[] = ['YES', 'NO'];
  public alarm: Alarm;
  public isFormValid = true;

  constructor(
    public dialogRef: MatDialogRef<AcknowledgeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.alarm = data;
    this.acknowledge = data['alarmAllData'];
    this.acknowledge.woGenerated = 'YES';
    this.acknowledge.downTime = 'YES';
    this.acknowledge.downtime_schedule = moment().toISOString();
    this.acknowledge.time = {
      hour: 0,
      minute: 0,
      second: 0
    };
  }

  ngOnInit() { }

  public back() {
    this.dialogRef.close();
  }

  public isWoDisabled(isWoGenerated) {
    return isWoGenerated === 'YES' ? false : true;
  }


  public changeDowntimeSchedule(type: string, event: MatDatepickerInputEvent<Date>) {
    this.acknowledge.downtime_schedule = moment(event.value).toISOString();
  }

  public woGeneratedChange(event: MatRadioChange) {
    if (event && event.value === 'NO' && this.acknowledge.wo) {
      this.acknowledge.wo = '';
    }
  }

  public save() {
    // if (this.acknowledge.woGenerated === 'YES') {
      if (!this.acknowledge.work_order) {
        this.isFormValid = false;
        return;
      }

      if (!this.acknowledge.work_order_lead) {
        this.isFormValid = false;
        return;
      }
    // }

    // if (this.acknowledge.downTime === 'YES') {
      if (!this.acknowledge.timestamp) {
        this.isFormValid = false;
        return;
      }

      if (!this.acknowledge.time) {
        this.isFormValid = false;
        return;
      }
    // }

    this.dialogRef.close(this.acknowledge);
  }
}
