import { TestBed, inject } from '@angular/core/testing';

import { UploadMediaService } from './upload-media.service';

describe('UploadMediaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UploadMediaService]
    });
  });

  it('should be created', inject([UploadMediaService], (service: UploadMediaService) => {
    expect(service).toBeTruthy();
  }));
});
