import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../shared/token.service';
import { environment } from '../../../environments/environment';

export const MIME_TYPES = {
  txt: 'text/plain',
  pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  ppt: 'application/vnd.ms-powerpoint',
  pdf: 'application/pdf',
  docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  doc: 'application/msword',
  csv: 'text/csv',
  xls: 'application/vnd.ms-excel',
  xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  epub: 'application/epub+zip',
  gif: 'image/gif',
  ico: 'image/vnd.microsoft.icon',
  tiff: 'image/tiff',
  tif: 'image/tiff',
  webp: 'image/webp',
  svg: 'image/svg+xml',
  bmp: 'image/bmp',
  jpeg: 'image/jpeg',
  jpg: 'image/jpeg',
  png: 'image/png',
};

@Injectable({
  providedIn: 'root',
})
export class UploadMediaService {
  constructor(private http: HttpClient, private token: TokenService) {}

  postFile(fileToUpload, site, asset, component) {
    let params = '';
    if (component && component.length > 0) {
      params = `/component-${component}`;
    }
    const endpoint = `${environment.apiURL}/external-media?site=${site}&path=asset-${asset}${params}`;

    return this.http.post(endpoint, JSON.stringify(fileToUpload), {
      headers: this.createHttpHeader(),
    });
  }

  getFileList(site, asset, component) {
    let params = '';
    if (component && component.length > 0) {
      params = `/component-${component}`;
    }
    const endpoint = `${environment.apiURL}/external-media?site=${site}&path=asset-${asset}${params}`;

    return this.http.get(endpoint, { headers: this.createHttpHeader() });
  }

  getFileByName(site, asset, component, fileName) {
    let params = '';
    if (component && component.length > 0) {
      params = `/component-${component}`;
    }
    const encodedName = encodeURI(fileName);
    const endpoint = `${environment.apiURL}/external-media?site=${site}&path=asset-${asset}${params}&file_name=${encodedName}`;

    return this.http.get(endpoint, { headers: this.createHttpHeader() });
  }

  deleteFile(site, asset, component, fileName) {
    let params = '';
    if (component && component.length > 0) {
      params = `/component-${component}`;
    }
    const encodedName = encodeURI(fileName);
    const endpoint = `${environment.apiURL}/external-media?site=${site}&path=asset-${asset}${params}&file_name=${encodedName}`;

    return this.http.delete(endpoint, { headers: this.createHttpHeader() });
  }

  isEncoded(uri) {
    uri = uri || '';
    console.log(uri !== decodeURIComponent(uri));
    console.log(uri);
    return uri !== decodeURIComponent(uri);
  }

  fullyDecodeURI(uri) {
    while (this.isEncoded(uri)) {
      uri = decodeURIComponent(uri);
    }

    return uri;
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString(),
    });
  }
}
