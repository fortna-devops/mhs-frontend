import { Injectable } from '@angular/core';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { ComplianceService } from '../compliance.service';

@Injectable({
  providedIn: 'root',
})
export class RegionService {
  constructor(private complianceService: ComplianceService) {}

  public getOverviewData(leftMenuData, params) {
    let overviewData = {};
    let regionData = {};

    leftMenuData['countries']
      .filter((item) => item['name'].replace(/\s/gi, '_') === params['country'])
      .forEach((country) => {
        regionData = country['regions'].find(
          (item) => item['name'].replace(/\s/gi, '_') === params['region']
        );
        if (regionData) {
          overviewData = regionData['overview'];
        }
      });

    return {
      overviewData,
      regionData,
    };
  }

  public getFacilities(overview, regionData) {
    const paths = {};
    const facilities = [];

    regionData['facilities'].forEach((facility) => {
      const facility_areas = facility.facility_areas;
      facility['facility_areas'].forEach((facility_area) => {
        paths[facility_area['name']] = `./${facility['site']}/${facility_area['name']}`;
      });

      facilities.push({
        name: facility.name.replace(/_/g, ' '),
        site_name: facility.name,
        site: facility.site,
        facility_areas: facility_areas,
        num_red_assets: 0,
        num_yellow_assets: 0,
        num_green_assets: 0,
      });
    });

    return facilities.map((facility) => {
      const found = overview['facilities'].find((item) => item.name === facility.site_name);
      if (found) {
        facility['num_red_assets'] += found['num_red_assets'];
        facility['num_yellow_assets'] += found['num_yellow_assets'];
        facility['num_green_assets'] += found['num_green_assets'];
      }

      facility['normal'] = this.complianceService.getHealthPercent(facility);
      facility['color'] = this.complianceService.getHealthColor(facility);
      facility['class'] = `health-${facility['color']}`;
      facility['path'] = `./${facility['site']}`;
      return facility;
    });
  }

  public getLayoutMap(theme, country) {
    if (theme && theme === 'theme-dark') {
      let countryPath = country.replace(/\s/gi, '_');
      if (country.toLowerCase() === 'italy') {
        countryPath = countryPath.toLowerCase();
      }

      return `./assets/images/compliance/map/dark-map-${countryPath}.svg`;
    } else {
      return `./assets/images/compliance/map/light-map-${country.replace(/\s/gi, '_')}.svg`;
    }
  }
}
