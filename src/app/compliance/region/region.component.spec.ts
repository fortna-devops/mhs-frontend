import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplianceRegionComponent } from './region.component';

describe('ComplianceRegionComponent', () => {
  let component: ComplianceRegionComponent;
  let fixture: ComponentFixture<ComplianceRegionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplianceRegionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplianceRegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
