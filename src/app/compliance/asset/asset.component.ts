import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  AfterViewInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplianceService, LocationCode, METRICS_TITLES } from '../compliance.service';
import { Subscription } from 'rxjs';
import { FavoritesService } from '../favorites/favorites.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import {
  AssetService,
  SELECT_COMPONENT_MESSAGE,
  GENERAL_ERROR_MESSAGER,
  ALARM_TYPE,
  DEFAULT_PAGE_SIZE_DESKTOP,
} from './asset.service';
import { MatDialog, MatPaginator } from '@angular/material';
import { ChartDialogComponent } from '../../shared/chart-dialog/chart-dialog.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as moment from 'moment';
import { CacheService } from '../../shared/cache.service';
import { MetricLineChartComponent } from '../../shared/metric-line-chart/metric-line-chart.component';
import { NvD3Component } from 'ng2-nvd3';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { TimezoneService } from '../../shared/timezone/timezone.service';

const PERIOD_TYPES = [
  {
    name: '1h',
    value: '1h',
    type: 'minute',
  },
  {
    name: '3h',
    value: '3h',
    type: 'minute',
  },
  {
    name: '1d',
    value: '24h',
    type: 'minute',
  },
  {
    name: '1w',
    value: '1w',
    type: 'hour',
  }
];
@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.scss'],
})
export class AssetComponent implements OnInit, OnDestroy {
  @ViewChild('nvd3') nvd3;

  @ViewChild('alarmsByDateChart') alarmsByDateChart;
  @ViewChild('alarmDateCountChart') alarmDateCountChart;
  @ViewChild('alarmsByTypePieChart') alarmsByTypePieChart;
  // @ViewChild('genericDataChart') genericDataChart;
  @ViewChildren(MetricLineChartComponent) genericDataChartList!: QueryList<
    MetricLineChartComponent
  >;
  @ViewChild('paginator') paginator: MatPaginator;

  public selectedIndex: number | null;
  private tabsObj = {
    layout: 0,
    overview: 1,
    analysis: 2,
  };

  public periodTypes = PERIOD_TYPES;
  public asset: string;
  private assetName: string;
  private assetId: string;
  public formattedDateRange: string;
  public href: string;
  public isFavorite = false;
  private favoritesubscription: Subscription;
  private routeSubscription: Subscription;
  public site: string;
  public acknowledgements: any;
  public alarms: any;
  public acknowledgementAlarms: any;
  public criticality: string;
  public assetType: string;
  public nameplateInfo: Array<any> = [];
  public startD: any;
  public endD: any;
  public selectedToday = true;

  public assetAlarms = {
    data: [],
    loading: false,
  };

  public alarmDateCount = {
    data: [],
    loading: false,
  };

  public assetAnalizeData = {
    alarmDateCounts: {},
    assetAlarms: '',
    assetDeatils: '',
    alarmsPercentage: [],
    startD: new Date(),
    endD: new Date(),
  };

  private alarmsPercentage = [];

  public genericChartDataList: {
    d3Options: string;
    data: any[];
    title: any;
    metricName: string;
    units: string;
    message: string;
    loading: boolean;
  }[] = [];

  private genericChartDataListAll: {
    d3Options: string;
    data: any[];
    title: any;
    metricName: string;
    units: string;
    message: string;
    loading: boolean;
  }[];

  public donutChart = {
    title: '',
    loading: false,
    data: [],
    d3Options: {},
    normal: '',
    errorMessage: '',
  };

  private alarmDateCountAll: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
    errorMessage: '',
  };

  public alarmDateCounts: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  private queryParams: any;
  public isLoading = false;
  private componentChartList: any;
  public componentIds: Array<any> = [];
  private detailList: any;
  public runtimeStats: any;
  public components: Array<string> = [];
  public sensor: string;

  public alarmCountAll: any;
  public alarmPercentageData: any;
  public chartMessage = GENERAL_ERROR_MESSAGER;

  private allIssues: Array<any> = [];
  public issues: Array<any> = [];

  public showPaginator = true;
  public pageSize: number;
  public logLength = 0;
  public activeVibration = 'rms_velocity_x';
  public transitions: Array<any> = [];
  public metricsList: Array<any> = [];
  public metricIds: Array<number> = [];
  public metrics: Array<any> = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private complianceService: ComplianceService,
    private favoritesService: FavoritesService,
    private topNavService: TopNavService,
    private timezoneService: TimezoneService,
    private assetService: AssetService,
    private dialog: MatDialog,
    private leftMenuService: LeftMenuService,
    private deviceDetectorService: DeviceDetectorService,
    private cacheService: CacheService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.queryParams = {
        componentType: params['componentType'],
        tagName: params['tagName'],
        timestamp: params['timestamp'],
        sensor: params['sensor'],
      };
    });
  }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      // this.asset = `Asset ${params['asset']}`;
      this.assetId = params['asset'].replace('asset-', '');
      this.href = this.getUrlWithoutParams();
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;
      if (params['tab']) {
        this.selectedIndex = params['tab'];
      } else {
        this.selectedIndex = 0;
      }
      this.site = params['site'];
      this.timezoneService.setSite(params['site'])
      this.initData();
      this.getAlarmsData();
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const levels = this.leftMenuService.getLeftMenuLevels();
      const componentList = this.getComponents(leftMenuData, levels, params)
      this.components = this.setComponents(componentList);
      this.componentIds = componentList.map(item => item.id);
      this.getDetails();
      this.getAlarmCounts();
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
      }
    });
  }

  getUrlWithoutParams() {
    const urlTree = this.router.parseUrl(this.router.url);
    urlTree.queryParams = {};
    return urlTree.toString();
  }

  private setActiveTabItem(tab) {
    this.selectedIndex = tab ? tab : 0;
  }

  public changeActiveTab(tabIndex) {
    this.selectedIndex = tabIndex;
    setTimeout(() => {this.updateCharts()}, 500);
  }

  private updateCharts() {
    if (typeof(Event) === 'function') {
      // modern browsers
      window.dispatchEvent(new Event('resize'));
    } else {
      // for IE and other old browsers
      // causes deprecation warning on modern browsers
      var resizeEvent = window.document.createEvent('UIEvents'); 
      resizeEvent.initUIEvent('resize', true, false, window, 0); 
      window.dispatchEvent(resizeEvent);
    }
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }

  // get data for metric charts from /data API
    // dates, sampleRate, metricId - optional params, for the selected time duration on top of chart(1h, 3h ...)
  private getChartData(dates = null, sampleRate = null, metricIds = null, metricName = null) {
    const offset = this.timezoneService.getTimezoneOffset();
    const {startD, endD} = this.timezoneService.getDateRangeInUTC({startD: this.startD, endD: this.endD}, offset, dates)

    const apiParams = {
      site: this.site,
      metric_ids: metricIds ? metricIds : this.metricIds.join(','),
      sample_rate: sampleRate ? sampleRate : 'hour',
      startDate: startD,
      endDate: endD,
      group_by: 'component_id'
    };

    this.componentChartList = [];

    if(!dates) {
      this.genericChartDataList = this.metricsList
      .map((metric) => {
        return {
          d3Options: '',
          data: [],
          title: metric['display_name'],
          metricName: metric['display_name'],
          metricId: metric['id'],
          selectedPeriod: null,
          sampleRate: sampleRate,
          units: metric['units'], 
          message: '',
          loading: true,
        };
      });
    } else {
      // if duration option selected, then only selected chart needs to be changed
      const index = this.genericChartDataList.findIndex(obj => obj['metricName'] == metricName)
      this.genericChartDataList[index]['sampleRate'] = sampleRate;
      this.genericChartDataList[index]['loading'] = true;
      this.genericChartDataList[index]['message'] = '';
      this.genericChartDataList[index]['data'] = [];
    }

    this.genericChartDataListAll = this.genericChartDataList;

    if (this.componentIds.length === 0) {
      this.genericChartDataList.map((chart) => {
        chart['loading'] = false;
        return chart; 
      });
      return;
    }


    // TODO: Get latest request result
    this.complianceService.getChartData(apiParams).subscribe(
      (response) => {
        const responseList = response['data'];

        const groupByKey = (list, key) => list.reduce((hash, obj) => ({...hash, [obj[key]]:( hash[obj[key]] || [] ).concat(obj)}), {})
        const groupResponseList = groupByKey(responseList, 'group_key')

        Object.keys(groupResponseList).forEach((key) => {
          // setting all data needed, including thresholds and alarms
          const resultData = this.setChartData(groupResponseList[key], key);
          this.componentChartList.push(...resultData);
        });

        this.genericChartDataList.map((chart) => {
          const foundData = this.componentChartList.filter((item) => item['metricName'] === chart['metricName']);
          if(!dates || dates && foundData.length > 0 ) {
            let data = foundData.map((item) => {
              item['values']['metricName'] = item['metricName'];
              return item;
            });
  
            let meta = foundData.map((item) => item['meta']);
            let decimalPlaces = meta.filter((item) => item['decimal_places']);
            const maxDecimalPlace = Math.max.apply(Math, decimalPlaces.map((o) => o.decimal_places));
  
            chart['data'] = data;
            chart['loading'] = false;
            chart['options'] = this.assetService.getChartOptions({decimalPlace: maxDecimalPlace});
          }
          return chart;
        });

        if(dates) {
          const index = this.genericChartDataList.findIndex(obj => obj['metricName'] == metricName)
          this.genericChartDataList[index]['loading'] = false; 
        }
        this.genericChartDataListAll = this.genericChartDataList;

      },
      (error) => {
        this.isLoading = false;
        this.genericChartDataList.map((chart) => {
          chart['loading'] = false;
          return chart;
        });
        // this.toasterService.pop(ERROR_STATUS, ``, `An error occurred.`);
      }
    );
  }

  private setChartData(componentMetricsData, componentId) {
    const componentObj = this.components.find(comp => comp['value'] == componentId);
    const { color, classed } = this.complianceService.getComponentChartSettings(
      componentObj['type'].toUpperCase()
    );

    return componentMetricsData.map(metric => {
      const foundMetric = this.metricsList.find(item => {
        return item['metric_ids'].find(metricId => metricId == metric['metric_id'])
      });
      return {
          values: metric['data'].sort(this.complianceService.dateCompare),
          key: componentObj['label'],
          id: componentObj['value'],
          name: componentObj['label'],
          metricId: metric['metric_id'],
          meta: metric['meta'],
          metricName: foundMetric ? foundMetric['display_name'] : 'Metric',
          color: color,
          classed: classed, 
        };
    });
  }

  private getDetails() {
    const apiParams = {
      site: this.site,
      asset_ids: this.assetId,
      startDate: this.startD,
      endDate: this.endD,
    };
    this.complianceService.getAssetDetails(apiParams).subscribe(
      (result) => {
        const details = result['asset_details'][0];
        this.setDetailsData(details)
        this.runtimeStats = details['runtime_stats']
        // this.setNameplateInfo(result['asset']);
        this.cacheService.setAssetDetails(details, this.site, this.assetId);
      },
      (error) => {
        this.donutChart.loading = false;
        this.assetAlarms.loading = false;
        this.alarmDateCount.loading = false;
        this.alarmDateCounts.loading = false;
      }
    );
  }

  // get Alarms By Date and Alarms By Type
  private getAlarmCounts() {
    const {startD, endD} = this.timezoneService.getDateCountRangeInUTC({startD: this.startD, endD: this.endD})

    const apiParams = {
      site: this.site,
      component_ids: this.componentIds.join(','),
      startDate: startD,
      endDate: endD,
      timezone: this.timezoneService.getTimezoneOffset()
    };

    this.complianceService.getAlarmCounts(apiParams).subscribe((result) => {
      const counts = result['counts'] ? result['counts'] : [];
      const dateCounts = result['date_counts'] ? result['date_counts'] : [];

      const alarmDateCounts = this.setAlarmDateCounts(dateCounts, startD, endD);
      const cacheCounts = {
        data: counts,
        site: this.site,
        startDate: apiParams.startDate,
        endDate: apiParams.endDate,
        assetId: this.assetId,
      };

      const cacheDateCounts = {
        data: dateCounts,
        site: this.site,
        startDate: apiParams.startDate,
        endDate: apiParams.endDate,
        assetId: this.assetId,
      };
      this.cacheService.setAlarmCounts(cacheCounts);
      this.cacheService.setAlarmDateCounts(cacheDateCounts);

      this.initDonutChart();
      this.alarmCountAll = counts.map((item) => {
        const foundComponent = this.components.find(comp => comp['value'] == item['component_id'])
        const { color, classed } = this.complianceService.getComponentChartSettings(
          foundComponent['type']
        );
        item['componentLabel'] = item['component_name'];
        item['color'] = color;
        return item;
      });
      this.populateDonutChart(this.alarmCountAll);

      this.alarmDateCount = Object.assign({}, alarmDateCounts, {loading: false});

      this.alarmDateCountAll = alarmDateCounts;
      this.alarmDateCounts['options'] = this.alarmDateCountAll['options'];
      this.alarmDateCounts['data'] = JSON.parse(JSON.stringify(this.alarmDateCountAll['data']));
      this.alarmDateCounts['loading'] = false;
      this.alarmDateCounts.errorMessage = this.getErrorMessage();
    });

  }

  private setNameplateInfo(assetDetails) {
    if (
      !assetDetails ||
      !assetDetails['nameplate_info'] ||
      Object.keys(assetDetails['nameplate_info']).length === 0
    ) {
      return;
    }

    this.nameplateInfo = Object.keys(assetDetails['nameplate_info']).map((item) => {
      return {
        label: item,
        value: assetDetails['nameplate_info'][item],
      };
    });
  }

  private populateDonutChart(alarmCounts) {
    this.donutChart.loading = false;

    if (
      !alarmCounts ||
      !Array.isArray(alarmCounts) ||
      alarmCounts.length === 0
    ) {
      this.donutChart.errorMessage = this.getErrorMessage();
      return;
    }

    const resultCount = [];
    alarmCounts.forEach((alarmCount) => {
      alarmCount['metrics']
        .forEach((metric) => {
          const foundMetric = resultCount.find(metr => metr['metricName'] === metric['metric_display_name'])
          if (foundMetric) {
            metric['counts'].forEach((count) => {
              foundMetric['counts'][count['alarm_type_name']] += count['num_alarms'];
            });
          } else {
            metric['counts'].forEach((count) => {
              const obj = {
                metricName: metric['metric_display_name'],
                counts: {
                  [count['alarm_type_name']]: count['num_alarms'],
                }
              };
              resultCount.push(obj);
            });
          }
        });
    });


    const countSum: any = resultCount.map(item => {
      return Object.values(item['counts']).reduce((a: number, b: number) => a + b, 0);
    }).reduce((a: number, b: number) => a + b, 0);

    if (!countSum || countSum === 0) {
      this.donutChart.errorMessage = `No events detected in this time interval`;
      return;
    }

    const alarmsByTypeData = [];
    resultCount.forEach((metric) => {
      Object.keys(metric['counts']).forEach(alarmType => {
        const percentage = Math.floor((metric['counts'][alarmType] / countSum) * 100);
        alarmsByTypeData.push({
          label: `${ALARM_TYPE[alarmType]} ${metric['metricName']}`,
          value: metric['counts'][alarmType],
          color: this.assetService.getMetricTypeColor(alarmType, metric['metricName']),
          tooltipLabel: `${ALARM_TYPE[alarmType]} ${metric['metricName']} - ${metric['counts'][alarmType]} (${percentage} %)`,
        });
      });
    });

    this.donutChart.d3Options = this.assetService.getDonutChartD3Options();
    this.donutChart.data = alarmsByTypeData; 
  }

  private getErrorMessage() {
    if (
      this.components &&
      this.components.length > 0 &&
      this.components.every((component) => component['checked'] === false)
    ) {
      return SELECT_COMPONENT_MESSAGE;
    }

    return `No events detected in this time interval`;
  }

  private setPagination(totalCount) {
    if (this.paginator) {
      this.paginator.firstPage();
    }
    this.pageSize = this.assetService.getPageSize(
      totalCount,
      this.deviceDetectorService.isTablet()
    );
    this.logLength = totalCount;
    this.issues = this.allIssues.slice(0, this.pageSize);
  }

  private getCssClass(color) {
    return `alarm-${color.toLowerCase()} main-text-color alarm-link`;
  }

  private initDonutChart() {
    this.donutChart.d3Options = {};
    this.donutChart.data = [];
    this.donutChart.loading = true;
    this.donutChart.normal = '';
    this.donutChart.errorMessage = '';
  }

  private getAlarms(alarms) {
    return alarms.map((item) => {
      item['description'] = item['type_name'].toUpperCase() === 'CAMERA' ? item['type_name'] : item['description'];
      item['criteria'] = item['trigger_criteria'];
      item['class'] = `circle circle--${item['color'].toLowerCase()}`;
      return item;
    });
  }

  public getTagName(metric) {
    return metric.toLowerCase().indexOf('velocity') !== -1 ? 'vibrations' : 'temperature';
  }

  private setDetailsData(details) {
    this.metricsList = details['metrics'];
    this.metricIds = [];
    this.asset = details['name'];
    this.transitions = details['transitions'];
    this.metricsList.map(metric => {
      this.metricIds.push(...metric['metric_ids']);
      this.metrics.push(metric);
    });

    this.getChartData();
  }

  
  private setAlarmDateCounts(alarmDateCountList, startD, endD) {
    const alarmDateCounts = {
      data: [],
      options: {},
    };

    const alarmDateCountsResult = [];
    alarmDateCountList.forEach((item) => {
      const foundComponent = this.components.find(comp => comp['value'] == item['component_id'])
      const { color, classed } = this.complianceService.getComponentChartSettings(
        foundComponent['type']
      );
      alarmDateCountsResult.push({
        color: color,
        name: item['component_name'],
        id: item['component_id'],
        key: item['component_name'],
        values: this.getAlarmByDateCount(item['counts'], startD, endD),
      });
    });

    alarmDateCounts['data'] = alarmDateCountsResult;
    alarmDateCounts['options'] = this.assetService.getGroupedChartOptions({});

    return alarmDateCounts;
  }

  public setComponents(components) {
    return components.map((item) => {
      const { color } = this.complianceService.getComponentChartSettings(item['type']['name'].toUpperCase());
      return {
        value: item['id'],
        label: item['name'],
        type: item['type']['name'],
        color: color,
        checked: true,
      };
    });
  }

  public onDateRangeChange(date) {
    this.startD = date.startD;
    this.endD = date.endD;
    this.selectedToday = moment(this.endD).isSame(moment(), 'day');
    this.initData();
    this.getAlarmsData();
    this.getAlarmCounts();
    this.getChartData();
  }

  private getAlarmByDateCount(alarmCount, startD, endD) {
    let start = moment(startD);
    let end = moment(endD)
    const dateList = this.complianceService.getArrayOfDatesInRange(start, end.set({date: end.date() - 1}));

    return dateList.map((date) => {
      const alarmCountItem = alarmCount.find((alarm) => moment(alarm['date']).isSame(date));
      return {
        x: date,
        y: alarmCountItem && alarmCountItem['num_alarms'] ? alarmCountItem['num_alarms'] : 0,
      };
    });
  }
  
  // get Event Log 
  private getAlarmsData() {
      const alarmsForAllSites = this.cacheService.getAlarms();
      let alarms = {};
  
      if (Array.isArray(alarmsForAllSites)) {
        alarms = alarmsForAllSites.find((item) => item['site'] === this.site);
      }
  
      if (Object.entries(alarms).length > 0 && alarms.constructor === Object) {
        const alarmList = alarms['result'].filter((item) => item['asset']['id'] === this.assetId);
        this.setEventLog(alarmList);
      } else {
        this.complianceService
        .getAssetAlarms({ site: this.site, asset_ids: this.assetId, count: true })
        .subscribe((res) => {
          let limit = 300;
          let parts = 0;
          let diff = +res['count'] / limit;
          parts = diff > 0 && diff < 1 ? 1 : diff;

          // no need for request if no alarms
          if(parts === 0) { 
            return 
          }

          // sending alarms request using set limit so the request won't fail
          for(let i = 0; i < parts; i++) { 
            this.complianceService
            .getAssetAlarms({ 
              site: this.site, 
              asset_ids: this.assetId,
              limit: limit,
              offset: limit * i
            }).subscribe((result) => {
              this.setEventLog(result['alarms']);
            });
          }
        });
      }
  }

  private getAcknowledgements(alarmList) {
    const cachedAcknowledgements = this.cacheService.getAcknowledgements();
    let acknowledgements = {};

    if (Array.isArray(cachedAcknowledgements)) {
      acknowledgements = cachedAcknowledgements.find((item) => item['site'] === this.site);
    }

    if (acknowledgements && Object.entries(acknowledgements).length > 0) {
      this.acknowledgements = acknowledgements['data'];
      this.acknowledgementAlarms = this.assetService.setAlarms(alarmList, this.site);
      this.setEventLog(alarmList);
    } else {
      this.complianceService
        .getAcknowledgements({ site: this.site, asset_ids: this.assetId })
        .subscribe((result) => {
          this.acknowledgements = result ? result : [];
          this.cacheService.setAcknowledgements(this.acknowledgements, this.site);
          this.acknowledgementAlarms = this.assetService.setAlarms(alarmList, this.site);
          this.setEventLog(alarmList);
        });
    }
  }

  private setEventLog(alarmList) {
    // this.assetAlarms = { data: this.getAlarms(alarmList), loading: false };
    this.assetAlarms.data.push(...this.getAlarms(alarmList));
    this.assetAlarms.data = this.assetAlarms.data.slice();
    this.assetAlarms.loading = false;
  } 

  public radioChange(event, chart) {
    let dates = this.getDateByType(event.value);
    const metric = this.metrics.find(obj => obj.display_name === chart.metricName);
    const metricIds = metric.metric_ids;
    if(!event.value) { // if the close button clicked, request data for the operational dates
      this.getChartData(dates, null, metricIds, chart.metricName)
      return
    }
    const period = this.periodTypes.find(obj => obj.value === event.value);
    this.getChartData(dates, period.type, metricIds, chart.metricName); 
  }

  public getDateByType(activePeriodType) {
    // item.isChecked = event.checked;
    switch (activePeriodType) {
      case '1h':
        return {
          startD: moment().subtract('1', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '3h':
        return {
          startD: moment().subtract('3', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '24h':
        return {
          startD: moment().subtract('24', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '1w':
        return {
          startD: moment().subtract('6', 'days').toISOString(),
          endD: moment().toISOString(),
        };
      default:
        return {
          startD: moment().subtract('6', 'days').toISOString(),
          endD: moment().toISOString(),
        };
    }
  }

  public showChartDialog(type, metricName = null) {
    let dialogData: any;
    switch (type) {
      case 'alarmsByType':
        dialogData = {
          title: 'Events by Type',
          chartData: this.donutChart['data'],
          options: this.assetService.getDonutChartD3Options(350),
        };
        break;
      case 'alarmsByDate':
        dialogData = {
          title: 'Events by date',
          chartData: this.alarmDateCounts['data'],
          options: this.assetService.getGroupedChartOptions({ height: 350 }),
        };
        break;
      case 'generic':
        const chartData = this.genericChartDataList.find((chart) => chart.metricName === metricName);
        const genericDateRange = this.getZoomRange(chartData['data']);
        debugger
        dialogData = {
          title: chartData.title,
          chartData: chartData['data'],
          options: this.complianceService.getLineChartD3Options({range: genericDateRange}),
        };
        break;
    }

    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: dialogData,
      width: '85%',
      height: '450px',
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: {},
      });
    });
  }

  private getZoomRange(resultData) {
    let startTime = this.startD;
    let endTime = this.endD;

    const activeTimestamp = this.queryParams['timestamp'];
    const minDateTEST = new Date();
    minDateTEST.setHours(0, 0, 0, 0);
    const activeZoomTime = moment(new Date(+activeTimestamp));

    const chartData = resultData[0]['values'];
    let chartStartTime = '';
    let chartEndTime = '';

    if (chartData && chartData.length > 0) {
      const reasultLenght = chartData.length;
      chartStartTime = chartData[0]['x'];
      chartEndTime = chartData[reasultLenght - 1]['x'];
    }
    const isInRange =
      moment(chartStartTime).isBefore(activeZoomTime) &&
      moment(chartEndTime).isAfter(activeZoomTime);

    if (isInRange) {
      // activeTimestamp &&
      // activeZoomTime.isAfter(moment(minDateTEST).subtract(21, 'days')) &&
      // currentAlart['sensorId'] === this.activeSensorId &&
      // currentAlart['metric'] === this.activeTag.toLocaleLowerCase()) {
      startTime = moment(new Date(activeTimestamp), 'YYYY-MM-DD HH:mm:ss.SSSS').add(-30, 'h');
      endTime = moment(new Date(activeTimestamp), 'YYYY-MM-DD HH:mm:ss.SSSS').add(0, 'minutes');
    } else {
      startTime = chartStartTime;
      endTime = chartEndTime;
      const percentageOffset = (moment(endTime).diff(moment(startTime), 'minutes') * 5) / 100;
      startTime = moment(startTime).add(percentageOffset, 'minutes');
      endTime = moment(endTime).add(-percentageOffset, 'minutes');
    }

    return {
      startTime,
      endTime,
    };
  }

  public changeComponent(event) {
    if (this.components.every((component) => component['checked'] === false)) {
      this.chartMessage = SELECT_COMPONENT_MESSAGE;
    } else {
      this.chartMessage = 'There was an issue retrieving data.';
    }

    this.initDonutChart();

    this.genericChartDataList = this.genericChartDataListAll.map((chart) => {
      const newChart = { ...chart };
      newChart['data'] = newChart['data'].filter((item) => {
        return (
          this.components
            .filter((component) => component['checked'] === true)
            .filter((component) => component['value'] === item['id']).length > 0
        );
      });
      return newChart;
    });

    const pieChartData = this.alarmCountAll.filter((item) => {
      const components = this.components
      .filter((component) => component['checked'] === true)
      .filter((component) => {
        return component['value'] === item['component_id']
      })
      return components.length > 0
    });

    this.populateDonutChart(pieChartData);

    const activeAlarmCountData = this.alarmDateCountAll['data'].filter((item) => {
      const components = this.components
      .filter((component) => component['checked'] === true)
      .filter((component) => {
        return component['value'] === item['id']
      })
      return components.length > 0
    });

    this.alarmDateCounts['data'] = activeAlarmCountData;

    if (
      !this.alarmDateCounts['data'] ||
      !Array.isArray(this.alarmDateCounts['data']) ||
      this.alarmDateCounts['data'].length === 0
    ) {
      this.alarmDateCounts.errorMessage = this.getErrorMessage();
      return;
    }
  }

  private initData() {
    this.donutChart.data = [];
    this.donutChart.loading = true;
    this.alarmDateCounts.data = [];
    this.alarmDateCounts.loading = true;
    this.assetAlarms.data = [];
    this.assetAlarms.loading = true;
    this.alarmDateCount['data'] = [];
    this.alarmDateCount.loading = true;
  }

  private getComponents(menuResult, leves, paramList) {
    const result = [];

    if (leves === 2) {
      menuResult['facilities']
        .filter((item) => item['site'] === paramList['site'])
        .forEach((facility) => {
          facility['facility_areas']
          .filter((facilityArea) => facilityArea['name'].replace(/\s/gi, '_') === paramList['facilityArea'])
          .forEach((facilityArea) => {
            facilityArea['assets']
            .filter((asset) => asset['id'] == paramList['asset'].replace('asset-', ''))
            .forEach((asset) => {
              result.push(...asset['components']);
            });
          });
        });
    }

    if (leves === 4) {
      menuResult['countries']
        .filter(
          (country) =>
            country['name'].replace(/\s/gi, '_') === paramList['country'].replace(/\s/gi, '_')
        )
        .forEach((country) => {
          country['regions']
            .filter(
              (region) =>
              region['name'].replace(/\s/gi, '_') ===
                paramList['region'].replace(/\s/gi, '_')
            )
            .forEach((mainRegion) => {
              mainRegion['facilities']
                .filter((facility) => facility['site'] === paramList['site'])
                .forEach((facility) => {
                  facility['facility_areas']
                  .filter((facilityArea) => facilityArea['name'].replace(/\s/gi, '_') === paramList['facilityArea'])
                  .forEach((facilityArea) => {
                    facilityArea['assets']
                    .filter((asset) => asset['id'] == paramList['asset'].replace('asset-', ''))
                    .forEach((asset) => {
                      result.push(...asset['components']);
                    });
                  });
                });
            });
        });
    }

    return result;
  }
}
