import { Component, OnInit, Input, QueryList, ViewChildren } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { ComplianceService } from '../../compliance/compliance.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { LeftMenuService } from '../left-menu.service';
import { MatExpansionPanel } from '@angular/material';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss'],
})
export class AssetsComponent implements OnInit {
  @Input() facilityAreaInputList;
  @Input() url;
  @Input('facilitiesElementRef') facilitiesElementRef: QueryList<MatExpansionPanel>;

  @ViewChildren('assetGroupExpansionPanel') assetGroupExpansionPanel: QueryList<MatExpansionPanel>;
  @ViewChildren('assetExpansionPanel') assetExpansionPanel: QueryList<MatExpansionPanel>;

  public facilityAreas: Array<any>;
  public sensorFacilityAreaData = {};
  public activePath: string;
  public activeAsset = '';
  private activeMatExpansionPanel: MatExpansionPanel;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private complianceService: ComplianceService,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService
  ) {}

  ngOnInit() {
    this.facilityAreas = this.getFacilityAreas(
      this.facilityAreaInputList,
      this.topNavService.getTheme()
    );
    this.complianceService.setActiveAssetList(this.facilityAreas);

    const locationPathsLevels = document.location.pathname.split('/').slice(2);
    this.setAsset(locationPathsLevels);

    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const pathsLevels = ev['url'].substr(5).split('/');
        this.setAsset(pathsLevels);
      }
      if (ev instanceof NavigationEnd) {
        this.setActiveAsset();
      }
    });

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.facilityAreas = this.getFacilityAreas(this.facilityAreaInputList, theme);
    });
  }

  private setActiveAsset() {
    this.facilityAreas.map((facilityArea) => {
        facilityArea['assets'].map((asset) => {
          asset['isActive'] = this.getIsActiveAsset(asset['id']);
          return asset;
        });
      return facilityArea;
    });
  }

  private setAsset(pathsLevels) {
    let activeSiteName: string;
    let activeFacilityAreaName: string;
    let activeAssetName: string;
    let activeComponentName: string;

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      activeSiteName = pathsLevels[0] ? pathsLevels[0].replace(/\s/gi, '_') : pathsLevels[0];
      activeFacilityAreaName = pathsLevels[1]
        ? pathsLevels[1].replace(/\s/gi, '_')
        : pathsLevels[1];
      activeAssetName = pathsLevels[2] ? pathsLevels[2].replace(/\s/gi, '_') : pathsLevels[2];
      activeAssetName = pathsLevels[3] ? pathsLevels[3].replace(/\s/gi, '_') : pathsLevels[3];
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      activeSiteName = pathsLevels[2] ? pathsLevels[2].replace(/\s/gi, '_') : pathsLevels[2];
      activeFacilityAreaName = pathsLevels[3]
        ? pathsLevels[3].replace(/\s/gi, '_')
        : pathsLevels[3];
      activeAssetName = pathsLevels[4] ? pathsLevels[4].replace(/\s/gi, '_') : pathsLevels[4];
      activeComponentName = pathsLevels[5] ? pathsLevels[5].replace(/\s/gi, '_') : pathsLevels[5];
    }

    this.facilityAreas.map((facilityArea) => {
      facilityArea['isActive'] = false;
      facilityArea['assets'] = facilityArea['assets'].map((asset) => {
        asset['isActiveAsset'] = false;
        return asset;
      });
      return facilityArea;
    });

    this.facilityAreas.map((facilityArea) => {
      if (
        facilityArea['name'].replace(/\s/gi, '_') === activeFacilityAreaName &&
        facilityArea['site'].replace(/\s/gi, '_') === activeSiteName &&
        !activeAssetName
      ) {
        facilityArea['isActive'] = true;
      }

      facilityArea['assets'] = facilityArea['assets'].map((asset) => {
        if (asset['name'].replace(/\s/gi, '_') === activeAssetName && !activeComponentName) {
          asset['isActiveAsset'] = true;
        }

        const assetIconFile = `${this.topNavService.getTheme()}-asset-${
          asset['color'].toLowerCase()
        }.svg`;
        asset['icon'] = `assets/images/compliance/icons/${assetIconFile}`;
        return asset;
      });
      return facilityArea;
    });

    this.facilityAreas.map((facilityArea) => {
      if (
        facilityArea['name'].replace(/\s/gi, '_') === activeFacilityAreaName &&
        facilityArea['site'].replace(/\s/gi, '_') === activeSiteName &&
        activeAssetName
      ) {
        facilityArea['panelOpenState'] = true;
        const panelId = `${facilityArea['name']}-${facilityArea['site']}`;
        this.expandParentPanel(panelId);

        facilityArea['assets'] = facilityArea['assets'].map((asset) => {
          if (asset['name'].replace(/\s/gi, '_') === activeAssetName && activeComponentName) {
            asset['panelOpenState'] = true;
            const panelComponentId = `${asset['name']}-${facilityArea['name']}-${facilityArea['site']}`;
            this.expandAssetParentPanel(panelComponentId);
          }
          return asset;
        });
      }
      return facilityArea;
    });
  }

  private getFacilityAreas(assets, theme) {
    return assets.map((facilityArea) => {
      return {
        name: facilityArea['name'],
        site: facilityArea['site'],
        label: facilityArea['name'].replace(/_/gi, ' '),
        panelOpenState: false,
        link: `${facilityArea['site']}/${facilityArea['name'].replace(/\s/gi, '_')}`,
        isActive: this.isActiveFacilityArea(facilityArea),
        isOpen: this.isOpenedFacilityArea(facilityArea),
        openedByClick: false,
        assets: facilityArea['assets']
          .map((asset) => {
            return {
              name: asset['name'],
              id: asset['id'],
              label: `${facilityArea['name'].replace(/\s/gi, '_')}/asset-${asset['id']}`,
              panelOpenState: false,
              link: `${facilityArea['site']}/${facilityArea['name'].replace(/\s/gi, '_')}/asset-${
                asset['id']
              }`,
              isActive: this.getIsActiveAsset(asset['id']),
              isOpen: this.isOpenedAsset(asset['id']),
              openedByClick: false,
              color: asset['color'],
              // isActiveAsset: this.getIsActiveAsset(asset['id']),
              idItem: `${asset['name']}-${facilityArea['name']}-${facilityArea['site']}`,
              components: asset['components'].map((component) => {
                if (this.leftMenuService.getLeftMenuLevels() === 2) {
                  component[
                    'link'
                  ] = `${facilityArea['site']}/${facilityArea['name']}/asset-${asset['id']}`;
                } else {
                  component['link'] = `${this.url}/${facilityArea['name']}/asset-${asset['id']}`;
                }
                component['asset'] = asset['id'];
                return component;
              }),
            };
          })
          .sort((a, b) => {
            return b.color - a.color;
          }),
        icon: `assets/images/compliance/icons/${theme}-area-${
          facilityArea['color'].toLowerCase()
        }.svg`,
      };
    });
  }

  public setOpened(item, isOpened) {
    item.openedByClick = isOpened;
  }

  private isActiveFacilityArea(facilityArea) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const pathsLenght = paths.length;
    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

    if (leftMenuLevels === 4) {
      if (pathsLenght === 5) {
        return facilityArea['name'] === paths[4];
      } else {
        return false;
      }
    }

    if (leftMenuLevels === 2) {
      if (pathsLenght === 3) {
        return facilityArea['name'] === paths[2];
      } else {
        return false;
      }
    }
  }

  private isOpenedFacilityArea(facilityArea) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

    if (leftMenuLevels === 4) {
      return paths[5] && facilityArea['name'].replace(/\s/gi, '_') === paths[4];
    }

    if (leftMenuLevels === 2) {
      return paths[3] && facilityArea['name'].replace(/\s/gi, '_') === paths[2];
    }
  }

  private isOpenedAsset(assetId) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

    if (leftMenuLevels === 4) {
      return paths[6] && assetId == paths[5].replace('asset-', '');
    }

    if (leftMenuLevels === 2) {
      return paths[4] && assetId == paths[3].replace('asset-', '');
    }
  }

  private getIsActiveAsset(currentAssetId) {
    const paths = document.location.pathname.substr(1).split('/').filter(Boolean);
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      const activeAsset = paths[5];
      const activeComponent= paths[6];
      return activeAsset ? !activeComponent && currentAssetId == activeAsset.replace('asset-', '') : false;
    }

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const activeAsset = paths[3];
      const activeComponent= paths[4];
      return activeAsset ? !activeComponent && currentAssetId == activeAsset.replace('asset-', '') : false;
    }

    return false;
  }

  private expandParentPanel(facility) {
    if (this.assetGroupExpansionPanel && this.assetGroupExpansionPanel['_results']) {
      const elements = this.assetGroupExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === facility;
      });

      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  private expandAssetParentPanel(asset) {
    if (this.assetExpansionPanel && this.assetExpansionPanel['_results']) {
      const elements = this.assetExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === asset;
      });

      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  public goToPage(path) {
    let url = '';
    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      url = `./${path.link}`;
    }
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      url = `${this.url}/${path['label']}`;
    }

    this.router.navigate([url.replace(/\s/gi, '_')], {
      relativeTo: this.route,
    });
  }

  public expandPanel(facilityArea: any, matExpansionPanel: MatExpansionPanel, event: Event) {
    this.activeMatExpansionPanel = matExpansionPanel;
    this.leftMenuService.expandPanel(matExpansionPanel, event);
    if (this.leftMenuService.isExpansionIndicator(event.target)) {
      facilityArea.panelOpenState = !facilityArea.panelOpenState;
      return;
    }
    this.goToPage(facilityArea);
  }
}
