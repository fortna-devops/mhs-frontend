import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RoutesModule } from './routes.module';
import { MainMaterialModule } from './main-material.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule } from 'angular2-toaster';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComplianceModule } from './compliance/compliance.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpLoaderFactory } from './httpLoaderFactory';
import { LeftMenuModule } from './left-menu/left-menu.module';
import { LeftMenuService } from './left-menu/left-menu.service';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { CookieService } from 'ngx-cookie-service';
import 'd3';
import 'nvd3';
import { HttpErrorInterceptor } from './shared/http-error.interceptor';
import {UploadMediaComponent} from './compliance/upload-media/upload-media.component';
import { LoginComponent } from './login/login.component';
import { ReportModule } from './report/report.module';

export function initCustomersCall(configService: LeftMenuService) {
  return () => configService.isCustomers();
}

@NgModule({
  declarations: [
    AppComponent,
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToasterModule.forRoot(),
    MainMaterialModule,
    CoreModule,
    SharedModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpModule,
    HttpClientModule,
    DeviceDetectorModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RoutesModule,
    ComplianceModule,
    LeftMenuModule,
    ReportModule,
  ],
  providers: [
    CookieService,
     {
          provide: APP_INITIALIZER,
          useFactory: initCustomersCall,
          deps: [LeftMenuService],
          multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
}
