import { trigger, transition, state, animate, style } from '@angular/animations';

export const assetAnimation = trigger('assetAnimation', [
  state(
    'hideAsset',
    style({
      opacity: 0,
      display: 'none',
    })
  ),
  state(
    'showAsset',
    style({
      opacity: 1,
      display: 'block',
    })
  ),

  transition('* => hideAsset', [animate('0.5s')]),
  transition('* => showAsset', [animate('0.5s')]),
]);
