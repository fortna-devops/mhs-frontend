import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TopNavService } from '../../../shared/top-nav/top-nav.service';
import { LeftMenuService } from '../../../left-menu/left-menu.service';

const LAYOUT_COLORS = {
  'GREEN': '#179F6D',
  'YELLOW': '#FFA817',
  'RED': '#E65566',
};

@Component({
  selector: 'app-facility-layout',
  templateUrl: './facility-layout.component.html',
  styleUrls: ['./facility-layout.component.scss'],
})
export class FacilityLayoutComponent implements OnInit {
  @ViewChild('layoutContainer') layoutContainer: ElementRef;
  public alertList: Array<any> = [];
  public modalMessage: string;
  public modalVisible = false;
  private alertAssetList: object;
  public showPngImage: boolean;
  public imgSrc: string;
  private site: string;
  private activeFilter = 'GREEN,YELLOW,RED';
  private theme: string;
  public error = true;
  public errorMessage: string;
  public errorDescription: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private topNavService: TopNavService,
    private leftMenuService: LeftMenuService
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.errorMessage = '';
      this.error = true;
      this.errorDescription = '';
      this.site = params.get('site');
      this.theme = this.topNavService.getTheme();
      const layoutMap = this.getLayoutMap(this.site);
      this.setData(layoutMap);
    });
  }

  private setData(layoutMap) {
    const that = this;
    const menuResult = this.leftMenuService.getLeftMenuData();
    this.alertAssetList = this.getFacilitiesAssetsCount(menuResult, this.activeFilter);

    d3.xml(layoutMap)
      .mimeType('image/svg+xml')
      .get(function (error, xml) {
        if (!xml || error) {
          const layoutUnderConstruction = `./assets/images/layouts/under-construction.png`;
          d3.xml(layoutUnderConstruction)
            .mimeType('image/png')
            .get(function (errorUC, xmlUC) {
              if (that.layoutContainer.nativeElement['hasChildNodes']()) {
                that.layoutContainer.nativeElement['removeChild'](
                  that.layoutContainer.nativeElement['lastChild']
                );
              }
              that.imgSrc = layoutUnderConstruction;
              that.showPngImage = true;
              that.error = true;
              that.errorMessage = 'Layout is currently not available';
            });
        } else {
          const tooltip = d3
            .select('body')
            .append('div')
            .attr('class', 'asset-region-tooltip')
            .style('opacity', 0);
          that.showPngImage = false;
          if (that.layoutContainer.nativeElement['hasChildNodes']()) {
            that.layoutContainer.nativeElement['removeChild'](
              that.layoutContainer.nativeElement['lastChild']
            );
          }
          that.layoutContainer.nativeElement['appendChild'](xml.documentElement);
          const svg = d3.select(that.layoutContainer.nativeElement.children[0]);
          svg.selectAll('circle').each(function (d, i) {
            d3.select(this)
              .style('fill', (item) => {
                if (
                  d3.select(this).attr('id') &&
                  that.alertAssetList[d3.select(this).attr('id')] &&
                  that.alertAssetList[d3.select(this).attr('id')]['color']
                ) {
                  return LAYOUT_COLORS[that.alertAssetList[d3.select(this).attr('id')]['color']];
                } else {
                  return 'transparent';
                }
              })
              .on('click', function () {
                if (
                  d3.select(this).attr('id') &&
                  that.alertAssetList[d3.select(this).attr('id')] &&
                  that.alertAssetList[d3.select(this).attr('id')]['color']
                ) {
                  tooltip.transition().style('opacity', 0);
                  const alert = that.alertAssetList[d3.select(this).attr('id')];
                  that.goToAsset(`${alert['path'].replace(/\s/gi, '_')}`);
                }
              })
              .on('mouseover', function () {
                if (
                  d3.select(this).attr('id') &&
                  that.alertAssetList[d3.select(this).attr('id')] &&
                  that.alertAssetList[d3.select(this).attr('id')]['color']
                ) {
                  d3.select(this).attr('stroke', 'gray');
                  d3.select(this).style('cursor', 'hand');
                  tooltip.transition().duration(300).style('opacity', 0.9).style('z-index', 777777);
                  tooltip
                    .html(`Asset: ${that.alertAssetList[d3.select(this).attr('id')]['name']}`)
                    .style('left', d3.event['pageX'] + 10 + 'px')
                    .style('top', d3.event['pageY'] - 60 + 'px');
                }
              })
              .on('mouseout', function () {
                d3.select(this).attr('stroke', 'none');
                tooltip.transition().duration(500).style('opacity', 0).style('z-index', 0);
              });
          });
        }
      });
  }

  private getLayoutMap(site) {
    return `./assets/images/layouts/blueprint-${site}.svg`;
  }

  private goToAsset(url) {
    this.router.navigate([`./${url.replace(/\s/gi, '_')}`], {
      relativeTo: this.activatedRoute,
    });
  }

  private getFacilitiesAssetsCount(menuResult, activeFilter) {
    const facilitiesAssets = {};
    const facilitiesStatus = {};
    const facilitiesFullName = {};

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      menuResult['facilities'].forEach((facility) => {
        facilitiesAssets[facility['site']] = [];
        facilitiesStatus[facility['site']] = facility['color'];
        facilitiesFullName[facility['site']] = facility['fullName'];
        facility['facility_areas'].forEach((facilityArea) => {
          facilityArea['assets'].map((item) => {
            item['region'] = facilityArea['name'].replace(/\s/gi, '_');
            return item;
          });
          facilitiesAssets[facility['site']].push(...facilityArea['assets']);
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      menuResult['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((facility) => {
            facilitiesAssets[facility['site']] = [];
            facilitiesStatus[facility['site']] = facility['color'];
            facilitiesFullName[facility['site']] = facility['fullName'];
            facility['facility_areas'].forEach((facilityArea) => {
              facilitiesAssets[facility['site']].push(
                ...facilityArea['assets'].map((item) => {
                  item['path'] = `${facilityArea['name'].replace(/\s/gi, '_')}/asset-${item['id']}`;
                  return item;
                })
              );
            });
          });
        });
      });
    }

    const arrayToObject = (array) =>
      array.reduce((obj, item) => {
        obj[item.name] = item;
        return obj;
      }, {});

    return arrayToObject(
      facilitiesAssets[this.site].filter((item) => {
        return (
          activeFilter
            .split(',')
            .map((filterItem) => filterItem)
            .indexOf(item['color']) !== -1
        );
      })
    );
  }

  public filter(value) {
    this.activeFilter = value;
    const layoutMap = this.getLayoutMap(this.site);
    this.setData(layoutMap);
  }
}
