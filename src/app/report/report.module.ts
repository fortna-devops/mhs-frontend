import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportLeftMenuComponent } from './report-left-menu/report-left-menu.component';
import { ReportComponent } from './report.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { NvD3Module } from 'ng2-nvd3';
import { SharedModule } from '../shared/shared.module';
import { MainMaterialModule } from '../main-material.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LeftMenuModule } from '../left-menu/left-menu.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TrendsComponent } from './trends/trends.component';
import { ReportsComponent } from './reports/reports.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ResizableModule } from 'angular-resizable-element';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule,
    NvD3Module,
    SharedModule,
    MainMaterialModule,
    RouterModule,
    TranslateModule,
    LeftMenuModule,
    CKEditorModule,
    NgbModule,
    ResizableModule,
  ],
  declarations: [
    ReportComponent,
    ReportLeftMenuComponent,
    TrendsComponent,
    ReportsComponent,
  ],
  entryComponents: [],
})
export class ReportModule {}
