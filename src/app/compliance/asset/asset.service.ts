import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { PRIORITY_COLORS, PRIORITY_COLORS_HEX } from '../../shared/mapping';
import { Subject } from 'rxjs';
import { TimezoneService } from '../../shared/timezone/timezone.service';

export const DEFAULT_PAGE_SIZE_DESKTOP = 2;
const DEFAULT_PAGE_SIZE_TABLET = 3;

export const SELECT_COMPONENT_MESSAGE = 'Please select the component.';
export const GENERAL_ERROR_MESSAGER = 'No data available.';

export const ALARM_TYPE = {
  iso_spike: 'Spike',
  iso_persistent: 'Persistent',
}

export const TAG_TYPE_LIST = {
  iso_spike_rms_velocity_x_num: {
    type: 'Spike Vibration X',
    color: '#FF72D8',
  },
  iso_spike_rms_velocity_z_num: {
    type: 'Spike Vibration Z',
    color: '#92E9FF',
  },
  iso_spike_temperature_num: { type: 'Spike Temperature', color: '#A313FF' },
  iso_persistent_rms_velocity_x_num: { type: 'Sustained Vibration X', color: '#673AB7' },
  iso_persistent_rms_velocity_z_num: { type: 'Sustained Vibration Z', color: '#795548' },
  iso_persistent_temperature_num: { type: 'Sustained Temperature', color: '#D03997' },
  iso_persistent_temperature: {
    type: 'Sustained Temperature',
    color: '#9B96FF',
  },
};

@Injectable({
  providedIn: 'root',
})
export class AssetService {
  constructor(private topNavService: TopNavService, private timezoneService: TimezoneService) {}

  public getDonutChartD3Options(height = 0, errorMessage = 'No Data Available'): any {
    return {
      chart: {
        noData: errorMessage,
        type: 'pieChart',
        showLegend: true,
        height: height || 260,
        margin: {
          top: 20,
          right: 0,
          bottom: 20,
          left: 20,
        },
        x: function (d) {
          return d.label;
        },
        y: function (d) {
          return d.value;
        },
        text: function (d) {
          return d.value;
        },
        showLabels: false,
        labelType: 'percent',
        donut: true,
        donutRatio: 0.81,
        legendPosition: 'right',
        legend: {
          margin: {
            top: 60,
          },
          updateState: false,
          maxKeyLength: 100,
        },
        tooltip: {
          contentGenerator: (d) => {
            return `
              <table>
                <tbody>
                  <tr>
                  <td class="legend-color-guide">
                    <div style="background-color: ${d.color};"></div>
                  </td>
                  <td class="key">${d.data.tooltipLabel}</td>
                </tr>
                </tbody>
              </table>`;
          },
        },
      },
    };
  }

  public getStackedChartOptions(params, errorMessage = 'No Data Available') {
    const lineChartD3Options = {
      chart: {
        noData: errorMessage,
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'stackedAreaChart',
        height: 260,
        margin: {
          top: 60,
          right: 10,
          bottom: 40,
          left: 40,
        },
        x: (d) => moment(d.x).toDate(),
        y: (d) => d.y,
        useVoronoi: false,
        clipEdge: true,
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        // brushExtent: [ new Date(params['startTime']), new Date(params['endTime'])],
        duration: 1000,
        xAxis: {
          axisLabel: '',
          tickFormat: (d) => d3.time.format('%b %d %H:%M')(moment(d).toDate()),
          rotateLabels: '15',
        },
        yAxis: {
          // axisLabel: params['title'],
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10,
        },
        zoom: {
          enabled: true,
          scaleExtent: [1, 10],
          useFixedDomain: false,
          useNiceScale: false,
          horizontalOff: false,
          verticalOff: true,
          unzoomEventType: 'dblclick.zoom',
        },
        // forceY: [0, params['maxValue'] + (params['maxValue'] * 20 / 100)],
        xScale: d3.time.scale.utc(),
        pointSize: 30,
        legendPosition: 'right',
        tooltip: {
          position: function () {
            const brect = this.chart.getBoundingClientRect();
            return {
              top: brect.top + brect.height / 2,
              left: brect.left + brect.width / 2,
            };
          },
        },
        x2Axis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
        },
      },
    };

    // if (params['maxValue'] && params['maxValue'] < 0.05) {
    //   lineChartD3Options.chart.yAxis['ticks'] = 0;
    // } else {
    //   lineChartD3Options.chart.yAxis['ticks'] = 5;
    // }

    return lineChartD3Options;
  }

  public getGroupedChartOptions(params, errorMessage = 'No Data Available') {
    const lineChartD3Options = {
      chart: {
        noData: errorMessage,
        type: 'multiBarChart',
        showControls: false,
        showLegend: false,
        stacked: false,
        height: params['height'] || 260,
        margin: {
          // top: 60,
          right: 10,
          bottom: 40, 
          left: 60,
        },

        x: (d) => moment(d.x).toDate(),
        y: (d) => d.y,
        useVoronoi: false,
        clipEdge: true,
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        duration: 1000,
        xAxis: {
          axisLabel: '',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
          rotateLabels: '15',
        },
        yAxis: {
          tickFormat: (d) => {
            if(Math.floor(d) != d)
            {
                return;
            }
    
            return d;
        },
          axisLabelDistance: 10,
        },
        // yDomain: [0, 1],
        pointSize: 30,
        legendPosition: 'right',
        tooltip: {
          position: function () {
            const brect = this.chart.getBoundingClientRect();
            return {
              top: brect.top + brect.height / 2,
              left: brect.left + brect.width / 2,
            };
          },
        },
        groupSpacing: 0.5,
      },
    };
    return lineChartD3Options;
  }

  public getMetricTypeColor(alarmType, metricName){
    let color = '';
    let metric = metricName.toLowerCase();

    switch (alarmType) {
      case 'iso_spike':
        switch (metric) {
          case 'rms vibrational velocity x':
            color = '#FF72D8';
            break;
          case 'rms vibrational velocity z':
            color = '#FF1679';
            break;
          case 'temperature':
            color = '#A313FF';
            break;
          case 'voltage':
            color = '#9150A6';
            break;
          case 'speed':
            color = '#56569A';
            break;
          case 'current':
            color = '#F0A5B5';
            break;
          case 'mechanical_power':
            color = '#FFDEEA';
            break;
          default:
            color = '#0086FF';
            break;
        }
        break;
      case 'iso_persistent':
        switch (metric) {
          case 'rms vibrational velocity x':
            color = '#673AB7';
            break;
          case 'rms vibrational velocity z':
            color = '#795548';
            break;
          case 'temperature':
            color = '#9B96FF';
            break;
          case 'voltage':
            color = '#92E9FF';
            break;
          case 'speed':
            color = '#008299';
            break;
          case 'current':
            color = '#BC808D';
            break;
          case 'mechanical_power':
            color = '#D3C9E1';
            break;
          default:
            color = '#0086FF';
            break;
        }
        break;
      default:
        color = '#0086FF';
        break;
    }

    return color;
  }

  public getPageSize(count, isTablet) {
    if (isTablet && count > 2) {
      return DEFAULT_PAGE_SIZE_TABLET;
    } else {
      return DEFAULT_PAGE_SIZE_DESKTOP;
    }
  }

  public getTagList(sensorNumber, activeTag, sensorList) {
    const tags = [];
    sensorList
      .filter((item) => item['name'] === sensorNumber)
      .forEach((item) => {
        item['tags'].filter((tag) => tag !== activeTag).forEach((tag) => tags.push(tag));
      });

    return tags;
  }

  public getChartOptions(params, hasDate = true) {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineChart',
        height: params.heightMult ? 260 * params.heightMult : 260,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          // left: 40,
        },
        x: (d) => {
          return (hasDate ?  this.timezoneService.getDateWithOffset(d.x) : d.x)
        },
        y: (d) => d.y,
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        // brushExtent: [ new Date(params['startTime']), new Date(params['endTime'])],
        duration: 1000,
        xAxis: {
          axisLabel: '',
          tickFormat: (d) => {
            return hasDate ? d3.time.format('%b %d %H:%M')(new Date(d)) : d3.format('d')(d)
          },
          rotateLabels: '15',
        },
        yAxis: {
          // axisLabel: params['title'],
          tickFormat: (d) => params.decimalPlace ? d3.format('.' + params.decimalPlace + 'f')(d) : d3.format('.2f')(d),
          axisLabelDistance: 10,
        },
        forceY: params.isAlarmed ? [params.meta['min'] * 0.99, params.meta['max']] : [],
        xScale: d3.time.scale.utc(),
        // yScale: {
        //   domain: ([params.meta['min'], params.meta['max']])
        // },
        pointSize: 30,
        interactiveLayer: {
          tooltip: {
            contentGenerator: (e) => {
              const seriesCount = e.series.length;
              const header = `
                      <strong style="padding-left: 5px;">${d3.time.format('%b %d %H:%M')(
                        moment(e.value).toDate()
                      )}</strong>
                    `;

              let rows = '';
              for (let i = 0; i < seriesCount; i++) {
                const d = e.series[i];
                let label = d.value;
                // if (d.data.x !== moment(e.value).format('YYYY-MM-DD HH:mm:ss.000')) {
                //   label = null;
                // }

                if (!d.key.includes('Alarm peak')) {
                  rows += `<tr>
                  <td class="legend-color-guide">
                     <div style="background-color: ${d.color};"></div>
                   </td>
                   <td class="key">${d.key}</td>
                   <td class="value" style="text-align: center;">${
                     label || label === 0 ? d.value : '\u2013'
                   }</td>
                 </tr>`;
                }
              }
              return header + '<table>' + '<tbody>' + rows + '</tbody>' + '</table>';
            },
          },
        },
        x2Axis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
        },
      },
    };

    return lineChartD3Options;
  }

  public getReportChartOptions(params, hasDate = true) {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineChart',
        height: 260,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          left: 70,
        },
        x: (d) => {
          return (hasDate ?  this.timezoneService.getDateWithOffset(d.x) : d.x)
        },
        y: (d) => d.y,
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        // brushExtent: [ new Date(params['startTime']), new Date(params['endTime'])],
        duration: 1000,
        xAxis: {
          axisLabel: '',
          tickFormat: (d) => d3.time.format('%b %d %H:%M')(moment(d).toDate()),
          rotateLabels: '15',
        },
        yAxis: {
          // axisLabel: params['title'],
          tickFormat: (d) => params.decimalPlace ? d3.format('.' + params.decimalPlace + 'f')(d) : d3.format('.2f')(d),
          axisLabelDistance: 10,
        },
        forceY: params.isAlarmed ? [params.meta['min'] * 0.99, params.meta['max']] : [],
        xScale: d3.time.scale.utc(),
        pointSize: 30,
        tooltip: {
          position: function () {
            const brect = this.chart.getBoundingClientRect();
            return {
              top: brect.top + brect.height / 2,
              left: brect.left + brect.width / 2,
            };
          },
        },
        interactiveLayer: {
          tooltip: {
            contentGenerator: (e) => {
              const seriesCount = e.series.length;
              const header = `
                      <strong style="padding-left: 5px;">${d3.time.format('%b %d %H:%M')(
                        moment(e.value).toDate()
                      )}</strong>
                    `;

              let rows = '';
              for (let i = 0; i < seriesCount; i++) {
                const d = e.series[i];
                let label = d.value;
                if (d.data.x !== moment(e.value).format('YYYY-MM-DD HH:mm:ss.000')) {
                  label = null;
                }

                if (!d.key.includes('Alarm peak')) {
                  rows += `<tr>
                  <td class="legend-color-guide">
                     <div style="background-color: ${d.color};"></div>
                   </td>
                   <td class="key">${d.key}</td>
                   <td class="value" style="text-align: center;">${
                     d.value
                   }</td>
                 </tr>`;
                }
              }
              return header + '<table>' + '<tbody>' + rows + '</tbody>' + '</table>';
            },
          },
        },
        x2Axis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
        },
      },
    };

    // if (params['maxValue'] && params['maxValue'] < 0.05) {
    //   lineChartD3Options.chart.yAxis['ticks'] = 0;
    // } else {
    //   lineChartD3Options.chart.yAxis['ticks'] = 5;
    // }

    return lineChartD3Options;
  }

  public getAlarmChartOptions(params) {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineChart',
        height: 260,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          left: 70,
        },
        x: (d) => moment(d.x).toDate(),
        y: (d) => d.y,
        callback: function (chart) {
          /* Adding thresholds */
          const g = d3.select(`#${params.chartId} svg`).select('g.nvd3');
          const x1 = chart.xScale()(chart.x()({ x: params.xValue1 }));
          const x2 = chart.xScale()(chart.x()({ x: params.xValue2 }))

          // g.append('line')
          //   .attr('x1', chart.xScale()(chart.x()({ x: params.xValue1 })))
          //   .attr('y1', 0)
          //   .attr('x2', chart.xScale()(chart.x()({ x: params.xValue1 })))
          //   .attr('y2', 200)
          //   .style('stroke-width', 1)
          //   .style('stroke', '#D7292E');

          g.append('polygon')
				    .attr('points', `${x1},0 ${x1},200 ${x2},200 ${x2},0`)
            .attr('fill', PRIORITY_COLORS_HEX[params.color.toLowerCase()])
            .attr('fill-opacity', '0.5');
        },
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        // brushExtent: [ new Date(params['startTime']), new Date(params['endTime'])],
        duration: 1000,
        xAxis: {
          axisLabel: '',
          tickFormat: (d) => d3.time.format('%b %d %H:%M')(moment(d).toDate()),
          rotateLabels: '15',
        },
        yAxis: {
          // axisLabel: params['title'],
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10,
        },
        // yDomain: [0, 1],
        xScale: d3.time.scale.utc(),
        pointSize: 30,
        tooltip: {
          position: function () {
            const brect = this.chart.getBoundingClientRect();
            return {
              top: brect.top + brect.height / 2,
              left: brect.left + brect.width / 2,
            };
          },
        },
        x2Axis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
        },
      },
    };

    // if (params['maxValue'] && params['maxValue'] < 0.05) {
    //   lineChartD3Options.chart.yAxis['ticks'] = 0;
    // } else {
    //   lineChartD3Options.chart.yAxis['ticks'] = 5;
    // }

    return lineChartD3Options;
  }

  public getAssetStatusApiParams(asset, site) {
    const minDate = new Date();
    minDate.setHours(0, 0, 0, 0);

    return {
      site: site,
      asset: asset,
      currentDay: {
        startDate: new Date(moment(minDate).subtract(1, 'days').toISOString()),
        endDate: new Date(new Date()),
      },
      lastSevenDays: {
        startDate: new Date(moment(minDate).subtract(7, 'days').toISOString()),
        endDate: new Date(new Date()),
      },
      lastMonth: {
        startDate: new Date(moment(minDate).subtract(30, 'days').toISOString()),
        endDate: new Date(new Date()),
      },
    };
  }

  public setAlarms(alarmList, site) {
    return alarmList
      .map((item) => {
        item['site'] = site; 
        item['class'] = `alarm-${item.color.toLowerCase()} main-text-color alarm-link`;
        item['criteria'] = item.trigger_criteria;
        item['value'] = item.trigger_value;
        item['alarmAllData'] = item;
        return item;
      })
      .sort((firstItem, secondItem) => secondItem.severity - firstItem.severity);
  }

  public getTagName(metric) {
    return metric.toLowerCase().indexOf('velocity') !== -1 ? 'vibrations' : 'temperature';
  }
}
