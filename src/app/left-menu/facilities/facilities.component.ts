import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { MatExpansionPanel } from '@angular/material';
import { LeftMenuService } from '../left-menu.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-facilities',
  templateUrl: './facilities.component.html',
  styleUrls: ['./facilities.component.scss'],
})
export class FacilitiesComponent implements OnInit {
  @Input() facilities;
  @Input() levels;
  @Input() url;
  @ViewChildren('facilitiesExpansionPanel') facilitiesExpansionPanel: QueryList<MatExpansionPanel>;

  public panelOpenState = false;
  public facilityList: Array<any>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService
  ) {}

  ngOnInit() {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const paths = ev['url'].split('/');
        const pathsLenght = paths.length;
        const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

        if (leftMenuLevels === 4) {
          this.facilityList.map((facility) => {
            facility['isActive'] = false;
            return facility;
          });

          const currentFacility = paths[4];
          if (pathsLenght === 5) {
            this.facilityList.map((facility) => {
              if (facility['site'] === currentFacility) {
                facility['isActive'] = true;
              }
              return facility;
            });
          }
          if (pathsLenght > 5) {
            this.facilityList.map((facility) => {
              if (facility['site'] === currentFacility) {
                facility['panelOpenState'] = true;
                this.expandParentPanel(currentFacility);
              }
              return facility;
            });
          }
        }

        if (leftMenuLevels === 2) {
          if (ev['url'] === `/MHS/${this.topNavService.getSiteId()}`) {
            this.facilityList.map((facility) => {
              if (facility['site'] === this.topNavService.getSiteId()) {
                facility['isActive'] = true;
              } else {
                facility['isActive'] = false;
              }
              return facility;
            });
          } else {
            this.facilityList.map((facility) => {
              if (this.topNavService.getSiteId() === facility['site']) {
                facility['panelOpenState'] = true;
              }
              facility['isActive'] = false;
              return facility;
            });

            this.expandParentPanel(this.topNavService.getSiteId());
          }
        }
      }
    });

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.facilityList = this.getFacilityList(this.facilities, theme);
    });

    this.facilityList = this.getFacilityList(this.facilities, this.topNavService.getTheme());
  }

  private expandParentPanel(facility) {
    if (this.facilitiesExpansionPanel && this.facilitiesExpansionPanel['_results']) {
      const elements = this.facilitiesExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === facility;
      });

      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  private getFacilityList(facilities, theme) {
    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      return facilities.map((facility) => {
        return {
          name: facility['name'].replace(/_/g, ' '),
          site: facility['site'],
          url: this.url ? `./${this.url}/${facility['name']}` : ``,
          facilityArea: facility['facility_areas'].map((item) => {
            item['site'] = facility['site'];
            return item;
          }),
          panelOpenState: false,
          isActive: this.isActiveFacility(facility),
          icon: `assets/images/compliance/icons/${theme}-facility-${
            facility['color'].toLowerCase()
          }.svg`,
        };
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      return facilities.map((facility) => {
        return {
          name: facility['name'].replace(/_/g, ' '),
          site: facility['site'],
          url: `./${facility['country']}/${facility['regionName']}/${facility['site']}`,
          facilityAreas: facility['facility_areas'].map((item) => {
            item['site'] = facility['site'];
            return item;
          }),
          panelOpenState: false,
          isActive: this.isActiveFacility(facility),
          isOpen: this.isOpenedFacility(facility),
          openedByClick: false,
          icon: `assets/images/compliance/icons/${theme}-facility-${
            facility['color'].toLowerCase()
          }.svg`,
        };
      });
    }
  }

  public goToPage(facility) {
    if (this.levels === 2) {
      this.router.navigate([`./${facility['site']}`], { relativeTo: this.activatedRoute });
    } else {
      // const url = `./${this.url}/${facility.name}`;
      this.router.navigate([facility['url'].replace(/\s/gi, '_')], {
        relativeTo: this.activatedRoute,
      });
    }
  }

  public expandPanel(facility: any, matExpansionPanel: MatExpansionPanel, event: Event) {
    this.leftMenuService.expandPanel(matExpansionPanel, event);
    if (this.leftMenuService.isExpansionIndicator(event.target)) {
      facility.panelOpenState = !facility.panelOpenState;
      return;
    }
    this.goToPage(facility);
  }

  setOpened(facility, isOpened) {
    facility.openedByClick = isOpened;
  }

  private isActiveFacility(facility) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const pathsLenght = paths.length;
    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

    if (leftMenuLevels === 4) {
      if (pathsLenght === 4) {
        const currentFacility = paths[3];
        return facility['site'] === currentFacility;
      } else {
        return false;
      }
    }

    if (leftMenuLevels === 2) {
      if (pathsLenght === 2) {
        return facility['site'] === this.topNavService.getSiteId();
      } else {
        return false;
      }
    }
  }

  private isOpenedFacility(facility) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

    if (leftMenuLevels === 4) {
      return paths[4] && facility['site'] === paths[3];
    }

    if (leftMenuLevels === 2) {
      return paths[2] && facility['site'] === this.topNavService.getSiteId();
    }
  }
}
