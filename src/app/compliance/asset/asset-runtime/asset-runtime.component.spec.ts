import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetRuntimeComponent } from './asset-runtime.component';

describe('AssetRuntimeComponent', () => {
  let component: AssetRuntimeComponent;
  let fixture: ComponentFixture<AssetRuntimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AssetRuntimeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetRuntimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
