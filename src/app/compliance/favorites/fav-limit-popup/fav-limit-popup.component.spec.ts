import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavLimitPopupComponent } from './fav-limit-popup.component';

describe('FavLimitPopupComponent', () => {
  let component: FavLimitPopupComponent;
  let fixture: ComponentFixture<FavLimitPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavLimitPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavLimitPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
