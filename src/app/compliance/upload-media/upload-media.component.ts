import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplianceService } from '../compliance.service';
import { Subscription } from 'rxjs';
import { UploadMediaService, MIME_TYPES } from './upload-media.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { ToasterService } from 'angular2-toaster';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { LeftMenuService } from '../../left-menu/left-menu.service';

const ERROR_STATUS = 'error';
const SUCCESS_STATUS = 'success';
const FILE_DELETED_SUCCESSFULLY = 'File was deleted successfully.';
const FILE_ADDED_SUCCESSFULLY = 'File was added successfully.';
const DOWNLOAD_STARTED_SUCCESSFULLY = 'File is downloading.';
const GET_FILE_FROM_SERVER = 'Getting file from server...';
const DELETE_DIALOG_WIDTH = '300px';
const DELETE_MESSAGE = 'Are you sure you want to delete this file?';

export interface DialogData {
  file_type: string;
  is_image: boolean;
  file_content: string;
}

@Component({
  selector: 'image-popup',
  templateUrl: 'image-popup.html',
  styles: [
    `
      .popup-container {
        width: 50vw;
        height: 80vh;
      }

      img {
        object-fit: contain;
        width: 100%;
      }
    `,
  ],
})
export class FilePopupComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}

@Component({
  selector: 'app-upload-media',
  templateUrl: './upload-media.component.html',
  styleUrls: ['./upload-media.component.scss'],
})
export class UploadMediaComponent implements OnInit {
  public component: string;
  public runtime: string;
  public asset: string;
  private site: string;
  public isLoading = true;
  public description: any;
  private routeSubscription: Subscription;
  public uploadFileInfo = {};
  public fileList = [];
  details: any;
  componentName: any;
  assetName: any;

  constructor(
    private router: Router,
    private uploadMediaService: UploadMediaService,
    private complianceService: ComplianceService,
    private toasterService: ToasterService,
    private leftMenuService: LeftMenuService,
    private activatedRoute: ActivatedRoute,
    private topNavService: TopNavService,
    public dialog: MatDialog,
    private location: Location
  ) {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.component = params['component'] ? params['component'].replace('component-', '') : null;
      this.asset = params['asset'].replace('asset-', '');
      this.site = params['site'];
      this.assetName = this.leftMenuService.getAssetName(this.asset, params);
      if(this.component) {
        this.getDetailsData()
      }
    });
  }

  ngOnInit() {
    this.getFileList();
  }

  private getDetailsData() {
    this.complianceService
      .getComponentDetails({ site: this.site, component_ids: this.component }) 
      .subscribe((response) => {
        this.details = response['component_details'][0];
        this.componentName = this.details['name'];
      });
  }
  
  public handleFileInput(files: FileList) {
    console.log(files)
    if (files && files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        const reader = new FileReader();
        reader.readAsDataURL(files[i]);
        reader.onload = () => {
          this.uploadFileInfo = {
            file_name: files[i].name,
            // type: file.type,
            file_content: reader['result'].split(',')[1],
          };
          // console.log(this.uploadFileInfo);
          if(files.length - 1 === i) {
            this.uploadFileToActivity(true);
          } else {
            this.uploadFileToActivity();
          }
        };
      }
    }
  }

  public isImage(fileName) {
    return fileName.match(/.(jpg|jpeg|png|gif|jfif)$/i);
  }

  public deleteFile(fileName) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: DELETE_MESSAGE },
      width: DELETE_DIALOG_WIDTH,
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe(
      (dialogConfirmation) => {
        if (dialogConfirmation === 'confirm') {
          this.uploadMediaService
            .deleteFile(this.site, this.asset, this.component, fileName)
            .subscribe((data: any) => {
              this.toasterService.pop(SUCCESS_STATUS, ``, FILE_DELETED_SUCCESSFULLY);
              this.getFileList();
            });
        }
      },
      (error) => {
        this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${error}`);
      }
    );
  }

  public b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  public openFile(fileName, fileContent) {
    const ext = this.getFileExtension(fileName);
    this.toasterService.pop(SUCCESS_STATUS, ``, GET_FILE_FROM_SERVER);

    this.uploadMediaService
      .getFileByName(this.site, this.asset, this.component, fileName)
      .subscribe((data: any) => {
        // downloadLink.href = linkSource;
        // downloadLink.download = fileName;
        this.toasterService.pop(SUCCESS_STATUS, ``, DOWNLOAD_STARTED_SUCCESSFULLY);
        // downloadLink.click();
        const fileUrl = `data:${MIME_TYPES[ext]};base64,${data.file_content}`;

        this.urlToFile(fileUrl, fileName).then((file) => {
          const blob = new Blob([file], { type: 'application/octet-stream' });
          const blobURL = window.URL.createObjectURL(blob);
          const link = document.createElement('a');

          link.href = blobURL;
          link.setAttribute('download', fileName);
          document.body.appendChild(link);
          link.click();
          link.remove();
        });
    });
  }

  public openImage(fileName) {
    const ext = this.getFileExtension(fileName);
    this.toasterService.pop(SUCCESS_STATUS, ``, GET_FILE_FROM_SERVER);

    this.uploadMediaService
      .getFileByName(this.site, this.asset, this.component, fileName)
      .subscribe((data: any) => {
        const dialogRef = this.dialog.open(FilePopupComponent, {
          data: {
            file_type: MIME_TYPES[ext],
            is_image: true,
            file_content: data.file_content,
          },
          panelClass: this.topNavService.getTheme(),
        });

        dialogRef.afterClosed().subscribe((result) => {
          console.log(`Dialog result: ${result}`);
        });
      });
  }

  public goBack() {
    this.location.back();
  }

  private urlToFile(url, filename) {
    return fetch(url)
      .then((res) => {
        return res.arrayBuffer();
      })
      .then((buf) => {
        return new File([buf], filename);
      });
  }

  private getFileExtension(fileName) {
    const re = /(?:\.([^.]+))?$/;
    return re.exec(fileName)[1];
  }

  public getFileList() {
    this.uploadMediaService
      .getFileList(this.site, this.asset, this.component)
      .subscribe((data: any) => {
        this.fileList = data;
      });
  }
  public uploadFileToActivity(loadFileList = false) {
    this.uploadMediaService
      .postFile(this.uploadFileInfo, this.site, this.asset, this.component)
      .subscribe(
        (data) => {
          // do something, if upload success
          this.uploadFileInfo = {};
          this.toasterService.pop(SUCCESS_STATUS, ``, FILE_ADDED_SUCCESSFULLY);
          if(loadFileList) {
            this.getFileList()
          }
        },
        (error) => {
          this.uploadFileInfo = {};

          console.log(error);
        }
      );
  }
}
