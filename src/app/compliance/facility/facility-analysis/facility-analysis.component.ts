import { Component, OnInit, ViewChild } from '@angular/core';
import { AssetService, ALARM_TYPE } from '../../asset/asset.service';
import { TopNavService } from '../../../shared/top-nav/top-nav.service';
import { ComplianceService, DONUT_CHART } from '../../compliance.service';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDialogComponent } from '../../../shared/chart-dialog/chart-dialog.component';
import { MatDialog } from '@angular/material';
import { LeftMenuService } from '../../../left-menu/left-menu.service';
import { TokenService } from '../../../shared/token.service';
import { TimezoneService } from '../../../shared/timezone/timezone.service';

@Component({
  selector: 'app-facility-analysis',
  templateUrl: './facility-analysis.component.html',
  styleUrls: ['./facility-analysis.component.scss'],
})
export class FacilityAnalysisComponent implements OnInit {
  @ViewChild('alarmsByDateChart') alarmsByDateChart;
  @ViewChild('alarmsByTypePieChart') alarmsByTypePieChart;

  public donutChart = DONUT_CHART;

  private alarmCountAllData: any;
  public alarmPercentageData: any;
  private alarmDateCountAllData: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  public alarmCountData: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    errorMessage: '',
    loading: false,
  };

  public leftMenuData: any;
  public startD: any = '';
  public endD: any = '';
  public minStartDate: any;
  public maxStartDate: any;
  public minEndDate: any;
  public maxEndtDate: any;
  public maxDate: any;
  public minDate: any;
  public fromDate: any;
  public toDate: any;
  public formattedDateRange: string;

  private site: string;
  public facilityAreas: Array<any>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private assetService: AssetService,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService,
    private tokenService: TokenService,
    private timezoneService: TimezoneService,
    private complianceService: ComplianceService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.leftMenuData = this.leftMenuService.getLeftMenuData();
    this.activatedRoute.params.subscribe((urlParams) => {
      this.site = urlParams['site'];
      this.initDonutChart();
      this.initBarChart();
      this.getAlarmCounts();
    });
  }

  private chartUpdate() {
      if (this.alarmsByDateChart && this.alarmsByDateChart.chart) {
        this.alarmsByDateChart.chart.update();
      }
      if (this.alarmsByTypePieChart && this.alarmsByTypePieChart.chart) {
        this.alarmsByTypePieChart.chart.update();
      }
  }

  // get Alarms By Date and Alarms By Type
  private getAlarmCounts() {
    const {startD, endD} = this.timezoneService.getDateCountRangeInUTC({startD: this.startD, endD: this.endD})
    
    const apiParams = {
      site: this.site,
      startDate: startD,
      endDate: endD,
      timezone: this.timezoneService.getTimezoneOffset()
    };
    
    this.complianceService.getAlarmCounts(apiParams).subscribe(
      (result) => {
        const counts = result['counts'] ? result['counts'] : [];
        const dateCounts = result['date_counts'] ? result['date_counts'] : [];
        this.alarmDateCountAllData = dateCounts;
        this.alarmCountAllData = counts;
        this.setFacilitiyAreas();
        this.populateAlarmsDateCountChart(this.alarmDateCountAllData, startD, endD);

        this.populateDonutChart(this.alarmCountAllData);
      },
      (error) => {}
    );
  }

  private setFacilitiyAreas() {
    const facilityAreas = this.complianceService.getFacilityAreas(this.leftMenuData, this.site);
    this.facilityAreas = facilityAreas
      .sort(function (a, b) {
        return a.name.localeCompare(b.name);
      })
      .map((item, index) => {
        return {
          value: item['id'],
          label: item['name'].replace(/_/gi, ' '),
          name: item['name'],
          color: this.complianceService.getFacilityColors(index),
          checked: true,
        };
      });
  }

  public onDateRangeChange(date) {
    this.startD = date.startD;
    this.endD = date.endD;
    this.getAlarmCounts();
  }

  private initDonutChart() {
    this.donutChart.d3Options = {};
    this.donutChart.data = [];
    this.donutChart.loading = true;
    this.donutChart.normal = '';
  }

  private initBarChart() {
    this.alarmCountData.d3Options = {};
    this.alarmCountData.data = [];
    this.alarmCountData.loading = true;
    this.alarmCountData.normal = '';
  }

  public populateAlarmsDateCountChart(alarms, startD, endD) {
    const alarmsList = [];
    this.facilityAreas.forEach((facilityArea) => {
      const filteredAlarms = alarms.find(
        (alarm) => alarm['facility_area_id'] === facilityArea['value']
      );
      if(filteredAlarms) {
        alarmsList.push({
          alarm_counts: filteredAlarms ? filteredAlarms['counts'] : [],
          id: facilityArea['value'],
          label: facilityArea['label'],
        });
      }
    });

    alarmsList.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });

    this.alarmDateCountAllData['data'] = alarmsList.map((item, index) => {
      return {
        color: this.complianceService.getFacilityColors(index),
        name: item['label'],
        id: item['id'],
        key: item['label'],
        values: this.getAlarmByDateCount(item['alarm_counts'], startD, endD),
      };
    });

    if (
      !this.alarmDateCountAllData['data'] ||
      !Array.isArray(this.alarmDateCountAllData['data']) ||
      this.alarmDateCountAllData['data'].length === 0
    ) {
      this.alarmCountData['data'] = [];
      this.alarmCountData.errorMessage = `No events detected in this time interval`;
      return;
    }

    this.alarmDateCountAllData['options'] = this.assetService.getGroupedChartOptions({});
    this.alarmCountData['data'] = JSON.parse(JSON.stringify(this.alarmDateCountAllData['data']));
    this.alarmCountData['options'] = this.alarmDateCountAllData['options'];
    this.chartUpdate();
  }

  private getAlarmByDateCount(alarmCount, startD, endD) {
    let start = moment(startD);
    let end = moment(endD)
    const dateList = this.complianceService.getArrayOfDatesInRange(start, end.set({date: end.date() - 1}));

    return dateList.map((date) => {
      const alarmCountItem = alarmCount.find((alarm) => moment(alarm['date']).isSame(date));
      return {
        x: date,
        y: alarmCountItem && alarmCountItem['num_alarms'] ? alarmCountItem['num_alarms'] : 0,
      };
    });
  }

  public showChartDialog(type) { 
    let dialogData: any;
    switch (type) {
      case 'alarmsByType':
        dialogData = {
          title: 'Events by Type',
          chartData: this.donutChart['data'],
          options: this.assetService.getDonutChartD3Options(350),
        };
        break;
      case 'alarmsByDate':
        dialogData = {
          title: 'Events by date',
          chartData: this.alarmCountData['data'],
          options: this.assetService.getGroupedChartOptions({ height: 350 }),
        };
        break;
    }

    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: dialogData,
      width: '85%',
      height: '450px',
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: {},
      });
    });
  }

  private populateDonutChart(alarmCountList) {
    if (
      Array.isArray(alarmCountList) &&
      alarmCountList.length === 0 &&
      Array.isArray(this.alarmCountAllData) &&
      this.alarmCountAllData.length > 0
    ) {
      this.donutChart['data'] = [];
      this.donutChart.errorMessage = `Please select the component.`;
      return;
    }

    if (
      !alarmCountList ||
      !Array.isArray(alarmCountList) ||
      alarmCountList.length === 0
    ) {
      this.donutChart['data'] = [];
      this.donutChart.errorMessage = `No events detected in this time interval`;
      return;
    }

    const resultCount = [];
    alarmCountList.forEach((alarmCount) => {
      alarmCount['metrics']
        .forEach((metric) => {
          const foundMetric = resultCount.find(metr => metr['metricName'] === metric['metric_display_name'])
          if (foundMetric) {
            metric['counts'].forEach((count) => {
              foundMetric['counts'][count['alarm_type_name']] += count['num_alarms'];
            });
          } else {
            metric['counts'].forEach((count) => {
              const obj = {
                metricName: metric['metric_display_name'],
                counts: {
                  [count['alarm_type_name']]: count['num_alarms'],
                }
              };
              resultCount.push(obj);
            });
          }
        });
    });

    const countSum: any = resultCount.map(item => {
      return Object.values(item['counts']).reduce((a: number, b: number) => a + b, 0);
    }).reduce((a: number, b: number) => a + b, 0);

    if (!countSum || countSum === 0) {
      this.donutChart.errorMessage = `No events detected in this time interval`;
      return;
    }

    const alarmsByTypeData = [];
    resultCount.forEach((metric) => {
      Object.keys(metric['counts']).forEach(alarmType => {
        const percentage = Math.floor((metric['counts'][alarmType] / countSum) * 100);
        alarmsByTypeData.push({
          label: `${ALARM_TYPE[alarmType]} ${metric['metricName']}`,
          value: metric['counts'][alarmType],
          color: this.assetService.getMetricTypeColor(alarmType, metric['metricName']),
          tooltipLabel: `${ALARM_TYPE[alarmType]} ${metric['metricName']} - ${metric['counts'][alarmType]} (${percentage} %)`,
        });
      });
    });

    this.donutChart.data = alarmsByTypeData;
    this.donutChart.d3Options = this.assetService.getDonutChartD3Options();
  }

  public changeFacilityArea(event) {
    this.initDonutChart();
    this.initBarChart();

    const pieChartData = this.alarmCountAllData.filter((item) => {
      const facilityAreas = this.facilityAreas
      .filter((facilityArea) => facilityArea['checked'] === true)
      .filter((facilityArea) => {
        return facilityArea['value'] == item['facility_area_id']
      })
      return facilityAreas.length > 0
    });

    this.populateDonutChart(pieChartData);
    this.populateAlarmCountData();
  }

  private populateAlarmCountData() {
    const activeAlarmCountData = this.alarmDateCountAllData['data'].filter((item) => {
      return (
        this.facilityAreas
          .filter((facilityArea) => facilityArea['checked'] === true)
          .filter((facilityArea) => facilityArea['value'] == item['id']).length > 0
      );
    });

    if (
      Array.isArray(activeAlarmCountData) &&
      activeAlarmCountData.length === 0 &&
      Array.isArray(this.alarmDateCountAllData['data']) &&
      this.alarmDateCountAllData['data'].length > 0
    ) {
      this.alarmCountData.errorMessage = `Please select the component.`;
    }

    this.alarmCountData['data'] = activeAlarmCountData;
  }
}
