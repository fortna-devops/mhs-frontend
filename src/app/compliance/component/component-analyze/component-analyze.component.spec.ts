import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentAnalyzeComponent } from './component-analyze.component';

describe('ComponentAnalyzeComponent', () => {
  let component: ComponentAnalyzeComponent;
  let fixture: ComponentFixture<ComponentAnalyzeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComponentAnalyzeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentAnalyzeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
