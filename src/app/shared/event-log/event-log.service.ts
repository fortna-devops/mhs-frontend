import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../token.service';
import { forkJoin } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class EventLogService {
  private alarmsFromAllSites: Array<any>;

  constructor(
    private http: HttpClient,
    private token: TokenService,
  ) { }

  public getAlarms(customer) {
    let urlAlarms = `${environment.apiURL}/customer-events?customer=${encodeURIComponent(customer)}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }

  public fileExists(url: string){
    return this.http.get(url,{ responseType: 'blob' }).pipe(map((res) => {
      console.log(res)
      return true;
    }),
    catchError((err) => {
      console.log(err)
      return of(false);
    }));
}

  public setAlarmsFromAllSites(alarms) {
    this.alarmsFromAllSites = alarms;
  }

  public getAlarmsFromAllSites() {
    return this.alarmsFromAllSites;
  }

  public setAlarmsCount(count) {
    this.token.setAlarmsCount(count)
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString()
    });
  }
}
