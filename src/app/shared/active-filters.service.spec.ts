import { TestBed, inject } from '@angular/core/testing';

import { ActiveFiltersService } from './active-filters.service';

describe('ActiveFiltersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveFiltersService]
    });
  });

  it('should be created', inject([ActiveFiltersService], (service: ActiveFiltersService) => {
    expect(service).toBeTruthy();
  }));
});
