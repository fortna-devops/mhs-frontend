import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChildren,
  QueryList,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import * as moment from 'moment';
import { NgbDate, NgbCalendar, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ComplianceService,
  DONUT_CHART,
  ERROR_STATUS,
  METRICS_KEYS,
  METRICS_TITLES,
} from '../../compliance.service';
import { ALARM_TYPE, AssetService, TAG_TYPE_LIST } from '../../asset/asset.service';
import { ToasterService } from 'angular2-toaster';
import { TopNavService } from '../../../shared/top-nav/top-nav.service';
import { MatDialog, MatIconRegistry } from '@angular/material';
import { ChartDialogComponent } from '../../../shared/chart-dialog/chart-dialog.component';
import { last } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { CacheService } from '../../../shared/cache.service';
import { NvD3Component } from 'ng2-nvd3';
import { MetricLineChartComponent } from '../../../shared/metric-line-chart/metric-line-chart.component';
import { PRIORITY_COLORS, PRIORITY_COLORS_HEX } from '../../../shared/mapping';
import { flatten } from '@angular/compiler';
import { TimezoneService } from '../../../shared/timezone/timezone.service';
import { TokenService } from '../../../shared/token.service';

const PERIOD_TYPES = [
  {
    name: '1h',
    value: '1h',
    type: 'minute',
  },
  {
    name: '3h',
    value: '3h',
    type: 'minute',
  },
  {
    name: '1d',
    value: '24h',
    type: 'minute',
  },
  {
    name: '1w',
    value: '1w',
    type: 'hour',
  }
];

@Component({
  selector: 'app-component-analyze',
  templateUrl: './component-analyze.component.html',
  styleUrls: ['./component-analyze.component.scss'],
})
export class ComponentAnalyzeComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @ViewChild('dp') datePicker: NgbInputDatepicker;
  @ViewChild('alarmsByDateChart') alarmsByDateChart;
  @ViewChild('avgVibrationChart') avgVibrationChart;
  @ViewChild('avgTemperatureChart') avgTemperatureChart;
  @ViewChild('alarmsByTypePieChart') alarmsByTypePieChart;
  @ViewChildren(MetricLineChartComponent) genericDataChartList!: QueryList<
    MetricLineChartComponent
  >;
  @Output() voted = new EventEmitter<boolean>();
  @Output() loading = new EventEmitter<boolean>();
  @Input() metricIds = [];
  @Input() metricsList = [];
  @Input() details;

  public periodTypes = PERIOD_TYPES;
  public site: string;
  private asset: string;
  public componentId: string;
  public componentTypeName: string;
  public donutChart = DONUT_CHART;
  public alarmPercentageData: any;

  public alarmDateCounts: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  public genericChartDataList: {
    d3Options: string;
    data: any[];
    title: any;
    metricName: string;
    metricId: string;
    selectedPeriod: any;
    sampleRate: any;
    units: string;
    message: string;
    loading: boolean;
  }[] = [];

  private genericChartDataListAll: {
    d3Options: string;
    data: any[];
    title: any;
    metricName: string;
    metricId: string;
    selectedPeriod: any;
    sampleRate: any;
    units: string;
    message: string;
    loading: boolean;
  }[];

  private queryParams: any;

  public startD: any = '';
  public endD: any = '';
  public minStartDate: any;
  public maxStartDate: any;
  public minEndDate: any;
  public maxEndtDate: any;
  public maxDate: any;
  public minDate: any;
  public fromDate: any;
  public toDate: any;
  public formattedDateRange: string;
  public hoveredDate: NgbDate;

  public activatedRouteObserver: any;
  public activeThresholds: any;
  public alarmList: any;
  public selectedToday = true;

  public activeVibration = 'rms_velocity_x';
  maxValue: any;
  minValue: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private complianceService: ComplianceService,
    private assetService: AssetService,
    private calendar: NgbCalendar,
    private toasterService: ToasterService,
    private topNavService: TopNavService,
    private dialog: MatDialog,
    private timezoneService: TimezoneService,
    private tokenService: TokenService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private cacheService: CacheService
  ) {
    // tslint:disable-next-line:max-line-length
    iconRegistry.addSvgIcon(
      'date-range-calendar',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/compliance/icons/date-range-calendar.svg'
      )
    );

    this.activatedRoute.queryParams.subscribe((params) => {
      this.queryParams = {
        showChartDialog: params['showChartDialog'],
        metric: params['metric'],
      };
    });
    this.activatedRouteObserver =  this.activatedRoute.params.subscribe((params) => {
      this.loading.emit(true);
      this.asset = params['asset'].replace('asset-', '');
      this.site = params['site'];
      this.componentId = params['component'].replace('component-', '');
      this.initDonutChart();
    });
  }

  ngOnInit() { 
    this.componentTypeName = this.details['type']['name'];   
    this.complianceService.getChangeScreenWidthEvent().subscribe((event) => {
      this.updateCharts();
    });
  }

  ngAfterViewInit() {
    this.updateCharts();
  }

  ngOnDestroy() {
    this.activatedRouteObserver.unsubscribe();
  }

  updateCharts() {
    if (typeof(Event) === 'function') {
      // modern browsers
      window.dispatchEvent(new Event('resize'));
    } else {
      // for IE and other old browsers
      // causes deprecation warning on modern browsers
      var resizeEvent = window.document.createEvent('UIEvents'); 
      resizeEvent.initUIEvent('resize', true, false, window, 0); 
      window.dispatchEvent(resizeEvent);
    }
  }

  // get event list for metric charts from /alarms API
  // dates, sampleRate, metricId - optional params, for the selected time duration on top of chart(1h, 3h ...)
  private getAlarmList(dates = null, sampleRate = null, metricId = null) {
    const offset = this.timezoneService.getTimezoneOffset();
    let {startD, endD} = this.timezoneService.getDateRangeInUTC({startD: this.startD, endD: this.endD}, offset, dates)

    this.complianceService
        .getComponentAlarmsCount({ 
          site: this.site, 
          component_ids: this.componentId, 
          startDate: startD,
          endDate: endD,
          count: true 
        }).subscribe((res) => {
          // sending alarms request using set limit so the request won't fail
          let limit = 300;
          let parts = 0;
          let diff = +res['count'] / limit;
          parts = diff > 0 && diff < 1 ? 1 : diff;

          // if duration option selected, then only selected chart needs to be changed
          if(dates || dates && parts === 0) {
            this.alarmList = this.alarmList.filter(obj => obj['metric']['id'] != metricId)
          } else { 
            this.alarmList = [];
          }

          const apiParams = {
            site: this.site, 
            component_ids: this.componentId,
            startDate: startD,
            endDate: endD, 
            metric_ids: metricId,
            limit: limit,
            offset: 0
          };

          if(!parts) {
            this.getChartData(apiParams, sampleRate, dates);
          }

          this.complianceService
          .getComponentRangeAlarms(apiParams, parts).subscribe((result) => {
            const alarms = flatten(result.map((item) => item['alarms']))
            this.alarmList.push(...this.assetService.setAlarms(alarms, this.site));
            this.getChartData(apiParams, sampleRate, dates);
          });
        });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.details && changes.details.currentValue.id) {
      this.componentTypeName = this.details['type']['name'];   
      this.componentId = this.activatedRoute.snapshot.paramMap.get('component').replace('component-', '');
      this.site = this.activatedRoute.snapshot.paramMap.get('site');
      // alarms for the metric charts need to be requested first to add then the alarm line to the metric data
      this.getAlarmList();
      this.getAlarmCounts(); 
    }
  }

  // get data for metric charts from /data API
  private getChartData(params, sampleRate = null, dates) {
    const chartData: Array<any> = [];

    if(!dates) {
      this.genericChartDataList = this.metricsList
      .map((metric) => {
        return {
          d3Options: '',
          data: [],
          title: metric['display_name'],
          metricName: metric['display_name'],
          metricId: metric['id'],
          selectedPeriod: null,
          sampleRate: sampleRate,
          units: metric['units'], 
          message: '',
          loading: true,
        };
      });
    } else {
      // if duration option selected, then only selected chart needs to be changed
      const index = this.genericChartDataList.findIndex(obj => obj['metricId'] == params['metric_ids'])
      this.genericChartDataList[index]['sampleRate'] = sampleRate;
      this.genericChartDataList[index]['loading'] = true;
      this.genericChartDataList[index]['message'] = '';
      this.genericChartDataList[index]['data'] = [];
    }

    this.genericChartDataListAll = this.genericChartDataList;
    this.updateCharts();
    
    const apiParams = {
      site: this.site,
      metric_ids: params.metric_ids ? params.metric_ids : this.metricIds.join(','),
      sample_rate: sampleRate ? sampleRate : 'hour',
      startDate: params['startDate'],
      endDate: params['endDate'],
    };

    this.complianceService.getChartData(apiParams).subscribe(
      (response) => {
        const responseList = response['data'];
        this.loading.emit(false);

        responseList.forEach((item) => {
          const foundMetric = this.metricsList.find(metric =>  metric['id'] == item['metric_id']);
          // setting all data needed, including thresholds and alarms
          const resultData = this.setChartData(item, foundMetric, apiParams);
          const result = {
            metricId: foundMetric['id'],
            metricName: foundMetric['display_name'],
            data: resultData['data'],
            meta: resultData['meta'],
            isAlarmed: resultData['isAlarmed'] // if chart has alarms, different styling may be needed
          }; 
          chartData.push(result);
        });

        this.genericChartDataList.map((chart) => {
          const foundData = chartData.filter((item) => item['metricId'] === chart['metricId']);
          if(!dates || dates && foundData.length > 0 ) { 
            let data = foundData
            .map((item) => {
              item['data']['metricName'] = item['metricName'];
              return item['data'];
            }).reduce((acc, val) => acc.concat(val), []);
          let isAlarmed = foundData.find((item) => item['isAlarmed'])
          const thresholdsOptions = {
            red: false,
            orange: false,
            low_red: false,
            low_orange: false
          };
          if(isAlarmed) isAlarmed['data'].map((item) => {if(item['level']) thresholdsOptions[item['level']] = true})
          let meta = foundData.map((item) => item['meta']).reduce((acc, val) => acc.concat(val), []);
          let decimalPlaces = meta.filter((item) => item['decimal_places']);
          const maxDecimalPlace = Math.max.apply(Math, decimalPlaces.map((o) => o.decimal_places));

          chart['data'] = data;
          chart['loading'] = false;
          chart['options'] = this.assetService.getChartOptions({
            isAlarmed: isAlarmed, 
            decimalPlace: maxDecimalPlace, 
            heightMult: (thresholdsOptions['low_orange'] || thresholdsOptions['low_red']) && 
                        (thresholdsOptions['red'] || thresholdsOptions['orange']) ? 1.8 : 
                        (thresholdsOptions['red'] && thresholdsOptions['orange']) ? 1.5 : null,
            meta: this.getMaxMinValues(meta)});
          }
             
          return chart;
        });

        if(dates) {
          const index = this.genericChartDataList.findIndex(obj => obj['metricId'] == params['metric_ids'])
          this.genericChartDataList[index]['loading'] = false; 
        }
        this.genericChartDataListAll = this.genericChartDataList;
 
        // TODO: check
        if (this.queryParams['showChartDialog'] === 'true') {
          this.showChartDialog(
            'generic',
            this.queryParams['metric']
          ); 
        }
      },
      (error) => {
        this.genericChartDataList.map((chart) => {
          chart['loading'] = false;
          return chart;
        });
        this.loading.emit(false);
        this.toasterService.pop(ERROR_STATUS, ``, `An error occurred.`);
      }
    );
  }

  private getMaxMinValues(meta) {
    const min = Math.min.apply(
      Math,
      meta.map(function (o) {
        return o.min_value;
      })
    );
    const max = Math.max.apply(
      Math,
      meta.map(function (o) {
        return o.max_value;
      })
    );
    return {max, min}
  }

  public setChartId(metric) {
    return `${metric.replace(/\s/gi, '_')}`;
  }

  private getComponentChartSettings(chartType) {
    let name = '';
    let color = '';
    let classed = '';

    switch (chartType) {
      case 'raw':
        name = 'Measured';
        const measuredData = this.complianceService.getComponentChartSettings(this.componentTypeName); //TODO componentTypeName
        color = measuredData.color || '#1897F2';
        classed = 'stroke-width-two';
        break;
      case 'calculated':
        name = 'Expected';
        color = '#9AD5FF';
        classed = 'dashed stroke-width-two';
        break;
      case 'red':
        name = 'Threshold Red';
        color = '#FF5252';
        classed = 'dashed stroke-width-one';
        break;
      case 'orange':
        name = 'Threshold Yellow';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-one';
        break;
      case 'peak':
        name = 'Alarm peak';
        classed = 'stroke-width-zero';
        break;
    }

    return {
      name,
      color,
      classed,
    };
  }

  private setChartData(item, foundMetric, apiParams) {
    if (!item || item['data'].length === 0) {
      return [];
    }

    const { name, color, classed } = this.getComponentChartSettings(item['group_key']);

    const resultData =  [{
        values: item['data'].sort(this.complianceService.dateCompare),
        key: name || item['group_key'],
        name: name || item['group_key'],
        area: false,
        metricName: foundMetric ? foundMetric['display_name'] : 'Metric',
        metricId: foundMetric ? foundMetric['id'] : 'Metric',
        color: color,
        classed: classed,
      }];
    const meta = [item['meta']];

    const activeAlarms = this.alarmList ? this.alarmList.filter((alarm) => alarm['metric']['id'] === foundMetric['id'] 
    && !moment(alarm['started_at']).isBefore(resultData[0]['values'][0]['x'])
    && alarm['type_name'].toUpperCase() !== 'WARNING'&& alarm['type_name'].toUpperCase() !== 'ERROR') : null;
    let isAlarmed = false;

    if (activeAlarms && activeAlarms.length > 0 && item['group_key'] === 'raw') {
      isAlarmed = true;
      const thresholdsData = this.setThresholds(foundMetric, resultData[0], activeAlarms, item['meta']);
      meta.push(thresholdsData.meta)
      resultData.push(...thresholdsData.data);
    }

    return {data: resultData, meta, isAlarmed: isAlarmed};
  }

  setAlarmsOnChart(alarms, data, minValue, maxValue, meta){
    const maxY = maxValue;
    const minY = minValue * 0.99;
    const itemList = [[],[]];
    const result = [];

    let alarmIndex = 0;
    let isAlarm = false;
    let startedTime = false;
    let endedTime = false;
    let colorIndex = alarms[alarmIndex].color.toLowerCase() == 'red' ? 0 : 1;
    let alarm = null;
    data.map((dataPoint, dataIndex) => {
      // debugger
      const alarmCondition = isAlarm || (alarms[alarmIndex] && data[dataIndex + 1] && (dataPoint['x'] == alarms[alarmIndex].started_at || (data[dataIndex + 1] ? moment(alarms[alarmIndex].started_at).isBetween(dataPoint['x'], data[dataIndex + 1]['x']) : null)));
      if(!alarmCondition) {
        // no alarm
        itemList[colorIndex].push({ x: dataPoint['x'], y: minY });
      } else {
        for(let i = alarmIndex; i < alarms.length; i++) {
          if(alarms[i] && (dataPoint['x'] == alarms[i].started_at || (data[dataIndex + 1] ? moment(alarms[i].started_at).isBetween(dataPoint['x'], data[dataIndex + 1]['x']) : null))) {
            // started
            alarm = alarms[i];
            colorIndex = alarm.color.toLowerCase() == 'red' ? 0 : 1;
            isAlarm = true;
            startedTime = true;
            if(dataPoint['x'] !== alarm.started_at) itemList[colorIndex].push({ x: dataPoint['x'], y: minY });
            itemList[colorIndex].push({ x: alarm.started_at, y: minY });
            itemList[colorIndex].push({ x: alarm.started_at, y: maxY });
          }
          if(alarms[i] && (isAlarm && dataPoint['x'] == alarm.ended_at || (data[dataIndex + 1] ? moment(alarms[i].ended_at).isBetween(dataPoint['x'], data[dataIndex + 1]['x']) : null))) {
            // ended
            colorIndex = alarm.color.toLowerCase() == 'red' ? 0 : 1;
            isAlarm = false;
            endedTime = true;
            alarmIndex++;
            if(dataPoint['x'] !== alarm.ended_at) itemList[colorIndex].push({ x: dataPoint['x'], y: maxY });
            itemList[colorIndex].push({ x: alarm.ended_at, y: maxY });
            itemList[colorIndex].push({ x: alarm.ended_at, y: minY });
          }
          if(isAlarm && startedTime) {
            // going
            itemList[colorIndex].push({ x: dataPoint['x'], y: maxY });
          }
          if(!isAlarm && endedTime) {
            // no alarm
            itemList[colorIndex].push({ x: dataPoint['x'], y: minY });
          }

          if(!data[dataIndex + 1] || new Date(alarms[i].started_at).getTime() >= new Date(data[dataIndex + 1]['x']).getTime() ) {
            break
          }
        }
      }
        
    })

    const { name, color, classed } = this.getComponentChartSettings('peak');
    itemList.map((list, index) => { 
      if(list.length !== 0) {
        result.push({
          values: list,
          key: name + ' ' + index,
          name: name + ' ' + index,
          area: true,
          metricName: alarms[0]['metric']['display_name'],
          metricId: alarms[0]['metric']['id'],
          color: index === 0 ? PRIORITY_COLORS_HEX['red'] : PRIORITY_COLORS_HEX['yellow'],
          classed: classed,
        });
      }
    })
    
    return result;
  }

  // set thresholds for metric charts
  private setThresholds(metricObj, itemList, activeAlarms, meta) {
    const colorMapping = {
      'RED': 'red',
      'YELLOW': 'orange',
    };
    const levelMapping = {
      'red': 'red',
      'orange': 'orange',
      'high_red': null,
      'high_orange': null,
      'low_red': 'red',
      'low_orange': 'orange',
    };
    const colors = activeAlarms.map(item => item.color)
    .filter((value, index, self) => self.indexOf(value) === index)
    const result = [];
    // find thresholds of needed colors
    const thresholds = metricObj['thresholds']
    .filter((threshold) => {
      return colors.find(color => {
        return colorMapping[color] === levelMapping[threshold['level_name']]
      })
    } )
    const tuples = [];
    activeAlarms.map((alarm) => {
      const result = thresholds.reduce((prev, curr) => colorMapping[alarm['color']] === levelMapping[curr['level_name']] && Math.abs(curr.value - alarm.value) < Math.abs(prev.value - alarm.value) ? curr : prev);
      tuples.push(result);
    });
    // find thresholds that closest to the alarmed value
    const closestThresholds = tuples.reduce(function(a,b){
      if (a.indexOf(b) < 0 ) a.push(b);
      return a;
    },[]);
    let thresholdValues = [];
    closestThresholds.map(threshold => {
      thresholdValues = [];
      itemList['values'].forEach((value) => {
        thresholdValues.push({ x: value['x'], y: threshold['value'] });
      });
      const level = threshold['level_name'];
      const { name, color, classed } = this.getComponentChartSettings(levelMapping[level]);
      result.push({
        values: thresholdValues,
        area: false,
        thresholdValue: threshold['value'],
        key: name + (level.includes('high') ? ' High' : (level.includes('low') ? ' Low' : '' )), // all lines in chart need to have different key
        name: name + (level.includes('high') ? ' High' : (level.includes('low') ? ' Low' : '' )),
        color: color,
        level: level,
        classed: classed,
      });
    })

    // max and min values are needed to create data for the alarmed line(alarm needs to be full chart height)
    let minThreshold = null;
    let maxThreshold = null;

    if(closestThresholds && closestThresholds.length > 1) {
      minThreshold = Math.min.apply(
        Math,
        closestThresholds.map(function (o) {
          return o.value;
        })
      );
      maxThreshold = Math.max.apply(
        Math,
        closestThresholds.map(function (o) {
          return o.value;
        })
      );
    } else if(closestThresholds.length == 1){
      minThreshold = closestThresholds[0].value;
      maxThreshold = closestThresholds[0].value;
    }

    const minY = minThreshold && minThreshold < meta['min_value'] ? minThreshold : meta['min_value'];
    const maxY = maxThreshold && maxThreshold > meta['max_value'] ? maxThreshold : meta['max_value'];

    result.push(...this.setAlarmsOnChart(activeAlarms, itemList['values'], minY, maxY, meta))
    return {data: result, meta: {max_value: maxY, min_value: minY},};
  }

  // get Alarms By Date and Alarms By Type
  private getAlarmCounts() {
    const {startD, endD} = this.timezoneService.getDateCountRangeInUTC({startD: this.startD, endD: this.endD})

    const apiParams = {
      site: this.site,
      component_ids: this.componentId,
      startDate: startD,
      endDate: endD,
      timezone: this.timezoneService.getTimezoneOffset()
    };

    this.alarmDateCounts['data'] = [];
    this.alarmDateCounts['loading'] = true;

    const cachedalarmDateCounts = this.cacheService.getAlarmDateCounts();
    let alarmDateCounts = [];
    if (Array.isArray(cachedalarmDateCounts) && cachedalarmDateCounts.length > 0) {
      const alarmDateCountsAll = cachedalarmDateCounts.find((item) => {
        return (
          item['site'] === this.site &&
          item['assetId'] == this.asset &&
          new Date(item['startDate']).getTime() === new Date(apiParams.startDate).getTime()
        );
      });
      alarmDateCounts = alarmDateCountsAll ? [alarmDateCountsAll['data'].find(item => item.component_id == this.componentId)] : []
    }

    const cachedAlarmCounts = this.cacheService.getAlarmCounts();
    let alarmCounts = {};
    if (Array.isArray(cachedAlarmCounts) && cachedAlarmCounts.length > 0) {
      const alarmCountsAll = cachedAlarmCounts.find((item) => {
        return (
          item['site'] === this.site &&
          item['assetId'] == this.asset &&
          new Date(item['startDate']).getTime() === new Date(apiParams.startDate).getTime()
        );
      });
      alarmCounts = alarmCountsAll ? alarmCountsAll['data'].find(item => item.component_id == this.componentId) : {};
    }

    if (
      alarmDateCounts &&
      alarmDateCounts.length > 0 &&
      alarmCounts &&
      Object.entries(alarmCounts).length > 0
    ) {
      this.populateAlarmsCountChart(alarmDateCounts, startD, endD);
      this.populateDonutChart(alarmCounts);
    } else {
      this.complianceService.getAlarmCounts(apiParams).subscribe(
        (result) => {
          const counts = result['counts'] ? result['counts'][0] : {};
          const dateCounts = result['date_counts'] ? result['date_counts'] : [];
          this.populateAlarmsCountChart(dateCounts, startD, endD);
          this.populateDonutChart(counts);
        },
        (error) => {
          this.donutChart.loading = false;
          this.alarmDateCounts.loading = false;
        }
      );
    }
  }

  // Alarms By Date chart
  public populateAlarmsCountChart(alarmsData, startD, endD) {
    const componentData = this.complianceService.getComponentChartSettings(this.componentTypeName); 
    this.alarmDateCounts['data'] = alarmsData.map(item => {
      return {
        color: componentData.color || '#1897F2',
        name: item['component_name'],
        id: item['component_id'],
        key: item['component_name'],
        values: this.getAlarmByDateCount(item['counts'], startD, endD)
      };
    });
    this.alarmDateCounts['options'] = this.assetService.getGroupedChartOptions({});
    this.alarmDateCounts['loading'] = false;
  }

  private getAlarmByDateCount(counts, startD, endD) {
    let start = moment(startD);
    let end = moment(endD)
    const dateList = this.complianceService.getArrayOfDatesInRange(start, end.set({date: end.date() - 1}));

    return dateList.map((date) => {
      const alarmCountItem = counts.find((alarm) => moment(alarm['date']).isSame(date));
      return {
        x: date,
        y: alarmCountItem && alarmCountItem['num_alarms'] ? alarmCountItem['num_alarms'] : 0,
      };
    });
  }

  // Alarms By Type chart
  private initDonutChart() {
    this.donutChart.d3Options = {};
    this.donutChart.data = [];
    this.donutChart.loading = true;
    this.donutChart.normal = '';
  }

  // Alarms By Type chart
  private populateDonutChart(alarmCounts) {
    this.donutChart.loading = false;
    const alarmMetrics = alarmCounts['metrics']

    if (
      !alarmMetrics ||
      !Array.isArray(alarmMetrics) ||
      alarmMetrics.length === 0
    ) {
      this.donutChart.data = [];
      this.donutChart.errorMessage = `No events detected in this time interval`;
      return;
    }

    const resultCount = [];
    alarmMetrics.forEach((metric) => {
          const foundMetric = resultCount.find(metr => metr['metricName'] === metric['metric_display_name'])
          if (foundMetric) {
            metric['counts'].forEach((count) => {
              foundMetric['counts'][count['alarm_type_name']] += count['num_alarms'];
            });
          } else {
            metric['counts'].forEach((count) => {
              const obj = {
                metricName: metric['metric_display_name'],
                counts: {
                  [count['alarm_type_name']]: count['num_alarms'],
                }
              };
              resultCount.push(obj);
            });
          }
        });

    const countSum: any = resultCount.map(item => {
      return Object.values(item['counts']).reduce((a: number, b: number) => a + b, 0);
    }).reduce((a: number, b: number) => a + b, 0);

    if (!countSum || countSum === 0) {
      this.donutChart.errorMessage = `No events detected in this time interval`;
      return;
    }


    const alarmsByTypeData = [];
    resultCount.forEach((metric) => {
      Object.keys(metric['counts']).forEach(alarmType => {
        const percentage = Math.floor((metric['counts'][alarmType] / countSum) * 100);
        alarmsByTypeData.push({
          label: `${ALARM_TYPE[alarmType]} ${metric['metricName']}`,
          value: metric['counts'][alarmType],
          color: this.assetService.getMetricTypeColor(alarmType, metric['metricName']),
          tooltipLabel: `${ALARM_TYPE[alarmType]} ${metric['metricName']} - ${metric['counts'][alarmType]} (${percentage} %)`,
        });
      });
    });

    this.donutChart.data = alarmsByTypeData;
    this.donutChart.d3Options = this.assetService.getDonutChartD3Options();
  }

  timeConverter(date: any) {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];
    const year = date.year;
    const month = months[date.month - 1];
    const day = date.day;
    return day + ' ' + month + ' ' + year;
  }

  public changePageHeader(event) {
    setTimeout(() => {
      this.complianceService.setChangeScreenWidthEvent(event);
    }, 100);
  }

  // if one of the different time durationa on top of the chart(1h, 3h ...) clicked
  public radioChange(event, chart) {
    let dates = this.getDateByType(event.value);
    if(!event.value) { // if the close button clicked, request data for the operational dates
      this.getAlarmList(dates, null, chart.metricId)
      return
    }
    const period = this.periodTypes.find(obj => obj.value === event.value)
    this.getAlarmList(dates, period.type, chart.metricId)
  }

  // get dates for the different time durationa on top of the chart(1h, 3h ...)
  public getDateByType(activePeriodType) {
    switch (activePeriodType) {
      case '1h':
        return {
          startD: moment().subtract('1', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '3h':
        return {
          startD: moment().subtract('3', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '24h':
        return {
          startD: moment().subtract('24', 'hours').toISOString(),
          endD: moment().toISOString(),
        };
      case '1w':
        return {
          startD: moment().subtract('6', 'days').toISOString(),
          endD: moment().toISOString(),
        };
      default:
        return {
          startD: moment().subtract('6', 'days').toISOString(),
          endD: moment().toISOString(),
        };
    }
  }

  public formatDateTwoDigit(date) {
    return date < 10 ? '0' + date : '' + date
  }


  public onDateRangeChange(date) {
    this.startD = date.startD;
    this.endD = date.endD;
    this.selectedToday = moment(this.endD).isSame(moment(), 'day');
    this.getAlarmCounts();
    this.getAlarmList();
  }

  public showChartDialog(type, metricId = null) {
    let dialogData: any;
    switch (type) {
      case 'alarmsByType':
        dialogData = {
          title: 'Events by Type',
          chartData: this.donutChart['data'],
          options: this.assetService.getDonutChartD3Options(350),
        };
        break;
      case 'alarmsByDate':
        dialogData = {
          title: 'Events by date',
          chartData: this.alarmDateCounts['data'],
          options: this.assetService.getGroupedChartOptions({ height: 350 }),
        };
        break;
      case 'generic':
        const chartData = this.genericChartDataList.find((chart) => chart.metricId == metricId);
        const genericDateRange = this.getZoomRange(chartData['data']);
        dialogData = {
          title: chartData.title,
          chartData: chartData['data'],
          options: this.complianceService.getLineChartD3Options({range: genericDateRange}),
        };
        break;
    }

    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: dialogData,
      width: '85%',
      height: '450px',
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: {},
      });
      this.voted.emit(true);
    });
  }

  private getZoomRange(resultData) {
    let startTime = this.startD;
    let endTime = this.endD;

    const activeTimestamp = this.queryParams['timestamp'];
    const minDateTEST = new Date();
    minDateTEST.setHours(0, 0, 0, 0);
    const activeZoomTime = moment(new Date(+activeTimestamp));

    const chartData = resultData[0]['values'];
    let chartStartTime = '';
    let chartEndTime = '';

    if (chartData && chartData.length > 0) {
      const reasultLenght = chartData.length;
      chartStartTime = chartData[0]['x'];
      chartEndTime = chartData[reasultLenght - 1]['x'];
    }

    const isInRange =
      moment(chartStartTime).isBefore(activeZoomTime) &&
      moment(chartEndTime).isAfter(activeZoomTime);

    if (isInRange) {
      // activeTimestamp &&
      // activeZoomTime.isAfter(moment(minDateTEST).subtract(21, 'days')) &&
      // currentAlart['metric'] === this.activeTag.toLocaleLowerCase()) {
      startTime = moment(activeTimestamp, 'YYYY-MM-DD HH:mm:ss.SSSS').add(-30, 'h');
      endTime = moment(activeTimestamp, 'YYYY-MM-DD HH:mm:ss.SSSS').add(0, 'minutes');
    } else {
      startTime = chartStartTime;
      endTime = chartEndTime;
      const percentageOffset = (moment(endTime).diff(moment(startTime), 'minutes') * 5) / 100;
      startTime = moment(startTime).add(percentageOffset, 'minutes');
      endTime = moment(endTime).add(-percentageOffset, 'minutes');
    }

    return {
      startTime,
      endTime,
    };
  }
}
