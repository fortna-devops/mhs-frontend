import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MainMaterialModule } from '../main-material.module';
import { HelperService } from './helper.service';
import { TopNavService } from './top-nav/top-nav.service';
import { TopNavComponent } from './top-nav/top-nav.component';
import { FormsModule } from '@angular/forms';
import { TokenService } from './token.service';
import { ActiveFiltersService } from './active-filters.service';
import { PreviousRouteService } from './previous-route.service';
import { TranslateModule } from '@ngx-translate/core';
import { LineChartComponent } from './line-chart/line-chart.component';
import { NvD3Module } from 'ng2-nvd3';
import { PercentageChartComponent } from './percentage-chart/percentage-chart.component';
import { KeysPipe } from './keys.pipe';
import { ChartDialogComponent } from './chart-dialog/chart-dialog.component';
import { EventLogComponent } from './event-log/event-log.component';
import { EventLogService } from './event-log/event-log.service';
import { SafePipe } from './safe.pipe';
import { DateRangeComponent } from './date-range/date-range.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CacheService } from './cache.service';
import { NoRightsScreenComponent } from './no-rights-screen/no-rights-screen.component';
import { SettingsDialogComponent } from './settings-dialog/settings-dialog.component';
import { MetricLineChartComponent } from './metric-line-chart/metric-line-chart.component';
import { SessionService } from './session.service';
import { PromptDialogComponent } from './prompt-dialog/prompt-dialog.component';
import { TimezoneDirective } from './timezone/timezone.directive';
import { TimezoneService } from './timezone/timezone.service';

@NgModule({
  imports: [
    CommonModule,
    MainMaterialModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    NvD3Module,
    NgbModule,
  ],
  declarations: [
    ConfirmDialogComponent,
    TopNavComponent,
    LineChartComponent,
    PercentageChartComponent,
    KeysPipe,
    SafePipe,
    ChartDialogComponent,
    EventLogComponent,
    DateRangeComponent,
    NoRightsScreenComponent,
    SettingsDialogComponent,
    MetricLineChartComponent,
    PromptDialogComponent,
    TimezoneDirective,
  ],
  providers: [
    HelperService,
    TopNavService,
    TokenService,
    SessionService,
    ActiveFiltersService,
    PreviousRouteService,
    EventLogService,
    CacheService,
    TimezoneService
  ],
  exports: [
    MetricLineChartComponent,
    TopNavComponent,
    LineChartComponent,
    PercentageChartComponent,
    KeysPipe,
    SafePipe,
    EventLogComponent,
    DateRangeComponent,
    TimezoneDirective
  ],
  entryComponents: [ConfirmDialogComponent, PromptDialogComponent, ChartDialogComponent, SettingsDialogComponent],
})
export class SharedModule {}
