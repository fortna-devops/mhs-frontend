import { Component, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { componentAnimation } from '../../animations/component.animation';
import { assetAnimation } from '../../animations/asset.animation';
import { LeftMenuService } from '../../../left-menu/left-menu.service';
import { PRIORITY_COLORS } from '../../../shared/mapping';

export const zoomAsset = trigger('zoomAsset', [
  state(
    'zoomInAsset',
    style({
      transform: 'scale(3)',
    })
  ),
  state(
    'zoomOutAsset',
    style({
      transform: 'scale(1)',
    })
  ),

  transition('* => zoomInAsset', [animate('0.5s')]),
  transition('* => zoomOutAsset', [animate('0.5s')]),
]);

export const fadeAnimation = trigger('fadeAnimation', [
  state(
    'fadeIn',
    style({
      opacity: 0,
    })
  ),
  state(
    'fadeOut',
    style({
      opacity: 1,
    })
  ),

  transition('* => fadeIn', [animate('0.5s')]),
  transition('* => fadeOut', [animate('0.5s')]),
]);

@Component({
  selector: 'app-asset-header',
  templateUrl: './asset-header.component.html',
  styleUrls: ['./asset-header.component.scss'],
  animations: [assetAnimation, fadeAnimation, componentAnimation, zoomAsset],
})
export class AssetHeaderComponent implements OnInit, OnChanges {
  public isOpen = false;
  public isDivIn = false;
  public assetAnimation = false;
  public showAssetImage = false;
  public color: string;

  public assetColorClasses = {
    B: '',
    BELT: '',
    G: '',
    M: '',
  };

  public gearboxAnimation = {
    component: '',
    fade: '',
    class: 'img img--eng img--eng--green',
  };

  public motorAnimation = {
    component: '',
    fade: '',
    class: 'img img--motor img--motor--green',
  };

  public beltAnimation = {
    component: '',
    fade: '',
    class: 'img img--belt img--belt--green',
  };

  public bearingAnimation = {
    component: '',
    fade: '',
    class: 'img img--bearing img--bearing--green',
  };

  private componentStatusList = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService
  ) {}

  ngOnInit() {
    const leftMenuData = this.leftMenuService.getLeftMenuData();

    this.activatedRoute.params.subscribe((params) => {
      this.setComponentStatusList(leftMenuData, params);
      this.setAssetPartsColors();
    });
  }

  ngOnChanges() {
    this.setAssetPartsColors();
  }

  private setAssetPartsColors() {
    // assetColorClasses - for the small asset component illustrations
    if (this.componentStatusList && this.componentStatusList.length > 0) {
      this.componentStatusList.map((item) => {
        const component = item['type']['name'];
        this.assetColorClasses[component] = `--${item['color'].toLowerCase()}`;
      });
    }
  }

  public getComponentImageCssClass(location, color) {
    let cssClass = '';
    location = location.toUpperCase();

    switch (location) {
      case 'MOTOR':
        cssClass = '#0869AE';
        this.motorAnimation['class'] = `img img--motor --${color.toLowerCase()}`;
        break;
      case 'BEARING':
        cssClass = '#DB64FF';
        this.bearingAnimation['class'] = `img img--bearing --${color.toLowerCase()}`;
        break;
      case 'FEED_AIR':
        cssClass = '#FF9359';
        break;
      case 'PHOTO_EYE':
        cssClass = '#673AB7';
        break;
      case 'BN':
        cssClass = '#795548';
        break;
      case 'BF':
        cssClass = '#FF6C27';
        break;
      case 'CHAIN':
        cssClass = '#3B7DFF';
        break;
      case 'GEARBOX':
        cssClass = '#5EFBE1';
        this.gearboxAnimation['class'] = `img img--eng --${color.toLowerCase()}`;
        break;
      case 'P':
        cssClass = '#008400';
        break;
      case 'RF':
        cssClass = '#FF898E';
        break;
      case 'RR':
        cssClass = '#9C27B0';
        break;
      case 'LF':
        cssClass = '#92E9FF';
        break;
      case 'LR':
        cssClass = '#D03997';
        break;
      case 'PIN_DIVERT':
        cssClass = '#8CFFE7';
        break;
      case 'VFD':
        cssClass = '#FF72D8';
        break;
      case 'FRAME':
        cssClass = '#9B96FF';
        break;
      case 'BELT':
        cssClass = '#FFF9F9';
        this.beltAnimation['class'] = `img img--belt --${color.toLowerCase()}`;
        break;
      default:
        cssClass = '#7F8100';
        break;
    }

    return cssClass;
  }

  // TODO: refactor
  public goToItemPage(component) {
    if (
      this.componentStatusList.filter((item) => item['component'] === component.toUpperCase())
        .length === 0
    ) {
      return;
    }

    if (component === 'G') {
      this.gearboxAnimation['component'] = 'zoomin';
      this.gearboxAnimation['fade'] = 'fadeOut';
      this.motorAnimation['fade'] = 'fadeIn';
      this.beltAnimation['fade'] = 'fadeIn';
      this.bearingAnimation['fade'] = 'fadeIn';
    }

    if (component === 'M') {
      this.motorAnimation['component'] = 'zoomin';
      this.motorAnimation['fade'] = 'fadeOut';
      this.gearboxAnimation['fade'] = 'fadeIn';
      this.beltAnimation['fade'] = 'fadeIn';
      this.bearingAnimation['fade'] = 'fadeIn';
    }

    if (component === 'Belt') {
      this.beltAnimation['component'] = 'zoomin';
      this.beltAnimation['fade'] = 'fadeOut';
      this.gearboxAnimation['fade'] = 'fadeIn';
      this.motorAnimation['fade'] = 'fadeIn';
      this.bearingAnimation['fade'] = 'fadeIn';
    }

    if (component === 'B') {
      this.bearingAnimation['component'] = 'zoomin';
      this.bearingAnimation['fade'] = 'fadeOut';
      this.gearboxAnimation['fade'] = 'fadeIn';
      this.motorAnimation['fade'] = 'fadeIn';
      this.beltAnimation['fade'] = 'fadeIn';
    }

    this.isOpen = !this.isOpen;
    let route = '';
    if (this.router.url.includes('more-details')) {
      const url = this.router.url.split('/');
      url.pop();
      url.push(component);
      route = url.join('/');
    } else {
      route = component;
    }
    setTimeout(() => {
      this.router.navigate([route], { relativeTo: this.activatedRoute });
    }, 3000);
  }

  public animationDone(event) {
    if (event['fromState'] !== 'void') {
      this.isDivIn = !this.isDivIn;
    }
  }

  public showSmallAssetImage(event) {
    if (event['fromState'] !== 'void' && event['fromState'] !== 'hideAsset') {
      this.showAssetImage = true;
    }
  }

  private setComponentStatusList(leftMenuData, params) {
    this.componentStatusList = [];
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries']
        .filter((item) => item['name'].replace(/\s/gi, '_') === params['country'])
        .forEach((country) => {
          country['regions']
            .filter((item) => item['name'].replace(/\s/gi, '_') === params['region'])
            .forEach((region) => {
              region['facilities']
                .filter((item) => item['site'].replace(/\s/gi, '_') === params['site'])
                .forEach((facility) => {
                  facility['facility_areas']
                    .filter((item) => item['name'].replace(/\s/gi, '_') === params['facilityArea'])
                    .forEach((facilityArea) => {
                      facilityArea['assets']
                        .filter((item) => item['id'] == params['asset'].replace('asset-', ''))
                        .forEach((asset) => { 
                          this.componentStatusList = asset['components'];
                        });
                    });
                });
            });
        });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities']
        .filter((item) => item['site'].replace(/\s/gi, '_') === params['site'])
        .forEach((facility) => {
          facility['facility_areas']
            .filter((item) => item['name'].replace(/\s/gi, '_') === params['facilityArea'])
            .forEach((facilityArea) => {
              facilityArea['assets']
                .filter((item) => item['id'] == params['asset'].replace('asset-', ''))
                .forEach((asset) => {
                  this.componentStatusList = asset['components'];
                });
            });
        });
    }

    this.componentStatusList.forEach((item) => {
      this.getComponentImageCssClass(item['type']['name'], item['color']);
    });
  }
}
