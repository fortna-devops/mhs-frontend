import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ComplianceComponent } from './compliance/compliance.component';
import { FacilityAreaComponent } from './compliance/facility-area/facility-area.component';
import { CountryComponent } from './compliance/country/country.component';
import { RegionComponent } from './compliance/region/region.component';
import { FacilityComponent } from './compliance/facility/facility.component';
import { ComponentItemComponent } from './compliance/component/component-item.component';
import { AssetComponent } from './compliance/asset/asset.component';
import { UploadMediaComponent } from './compliance/upload-media/upload-media.component';
import { ReportComponent } from './report/report.component';
import { ReportsComponent } from './report/reports/reports.component';
import { TrendsComponent } from './report/trends/trends.component';
import { NoRightsScreenComponent } from './shared/no-rights-screen/no-rights-screen.component';

const routes: Routes = [
  { path: '', redirectTo: '/MHS', pathMatch: 'full' }, 
  {
    path: 'home-screen',
    component: NoRightsScreenComponent,
  },
  {
    path: 'report',
    component: ReportComponent,
    children: [
      { path: ':site/reports', component: ReportsComponent },
      { path: ':site/trends', component: TrendsComponent },
    ],
  },
  {
    path: 'MHS',
    component: ComplianceComponent,
    children: [
      { path: '', component: CountryComponent },
      { path: ':region', component: RegionComponent },
      { path: ':region/:site', component: FacilityComponent },
      {
        path: ':region/:site/:facilityArea',
        component: FacilityAreaComponent,
      },
      {
        path: ':region/:site/:facilityArea/:asset',
        component: AssetComponent,
      },
      {
        path: ':region/:site/:facilityArea/:asset/:component',
        component: ComponentItemComponent,
      },
      {
        path: ':region/:site/:facilityArea/:asset/:component/more-details',
        component: UploadMediaComponent,
      },
      {
        path: ':region/:site/:facilityArea/:asset/more-details',
        component: UploadMediaComponent,
      },
    ],
  },
  { path: '**', redirectTo: '/MHS', pathMatch: 'full' },
  // canActivate: [GuardRoute]
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class RoutesModule {}
