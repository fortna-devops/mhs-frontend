import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GENERAL_ERROR_MESSAGER } from '../../asset/asset.service';

@Component({
  selector: 'app-facility-events',
  templateUrl: './facility-events.component.html',
  styleUrls: ['./facility-events.component.scss'],
})
export class FacilityEventsComponent implements OnInit {
  @Input() allErrorLog: Array<any> = [];
  @Input() assetsWithErrors: Array<any> = [];

  public chartMessage = GENERAL_ERROR_MESSAGER;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {}

  public goToAsset(alarmData) {
    const alarm = alarmData['alarmAllData'];
    const alarmRegion = alarmData['region'];

    this.router.navigate([`${alarmRegion}/${alarm['asset']}`.replace(/\s/gi, '_')], {
      relativeTo: this.activatedRoute,
    });
  }

  public goToComponentOverview(alarmData, component_type) {
    const alarm = alarmData['alarmAllData'];
    const alarmRegion = alarmData['region'];

    this.router.navigate(
      [`${alarmRegion}/${alarm['asset']}/${component_type}`.replace(/\s/gi, '_')],
      { relativeTo: this.activatedRoute }
    );
  }

  public goToComponentAnalize(alarmData, component_type) {
    const alarm = alarmData['alarmAllData'];
    const alarmRegion = alarmData['region'];
    this.router.navigate(
      [`${alarmRegion}/${alarm['asset']}/${component_type}`.replace(/\s/gi, '_')],
      {
        relativeTo: this.activatedRoute,
        queryParams: {
          showChartDialog: true,
          metric: alarm['metric'],
          timestamp: new Date(alarm['data_timestamp']).getTime(),
        },
      }
    );
  }
}
