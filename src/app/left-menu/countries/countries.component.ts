import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { MatExpansionPanel } from '@angular/material';
import { LeftMenuService } from '../left-menu.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss'],
})
export class CountriesComponent implements OnInit {
  @Input() countries;
  @ViewChildren('countriesExpansionPanel') countriesExpansionPanel: QueryList<MatExpansionPanel>;

  public countryData: Array<any>;
  public countryList: Array<any>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private deviceDetectorService: DeviceDetectorService,
    private topNavService: TopNavService
  ) {}

  ngOnInit() {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const paths = ev['url'].substr(5).split('/');

        this.countryList = this.countryList.map((country) => {
          country['isActive'] = false;
          return country;
        });

        if (paths.length === 1) {
          this.countryList = this.countryList.map((country) => {
            if (paths[0] === country['name'].replace(/\s/gi, '_')) {
              country['isActive'] = true;
            }
            return country;
          });
        }

        if (paths.length > 1) {
          this.countryList = this.countryList.map((country) => {
            if (paths[0] === country['name'].replace(/\s/gi, '_')) {
              country['panelOpenState'] = true;
              this.expandParentPanel(country['name'].replace(/\s/gi, '_'));
            }
            return country;
          });
        }
      }
    });

    this.countryList = this.getCountryList(this.countries, this.topNavService.getTheme());

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.countryList = this.getCountryList(this.countries, theme);
    });
  }

  public setOpened(country, isOpened) {
    country.openedByClick = isOpened;
  }

  private expandParentPanel(country) {
    if (this.countriesExpansionPanel && this.countriesExpansionPanel['_results']) {
      const elements = this.countriesExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === country;
      });
      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  // TODO: move to service
  private getCountryList(countries, theme) {
    return countries.map((country) => {
      return {
        name: country['name'],
        id: country['name'].replace(/\s/gi, '_'),
        regions: country['regions'].map((region) => { 
          region['country'] = country['name'];
          return region;
        }),
        panelOpenState: false,
        isActive:
          document.location.pathname.split('/MHS/')[1] === country['name'].replace(/\s/gi, '_')
            ? true
            : false,
        isOpen: this.isOpenedCountry(country['name'].replace(/\s/gi, '_')),
        openedByClick: false,
        icon: `assets/images/compliance/icons/${theme}-globe-${
          country['color'].toLowerCase()
        }.svg`,
        imgSrc: this.getImgSrc(false),
      };
    });
  }

  private isOpenedCountry(countryName) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    return paths[2] && countryName === paths[1];
  }

  private getSite(regions) {
    return regions[0]['facilities'][0]['site'];
  }

  private getImgSrc(openState) {
    return openState
      ? './assets/images/compliance/icons/arrow-expand.svg'
      : './assets/images/compliance/icons/arrow-collapse.svg';
  }

  public goToHomePage(countryNname) {
    // this.topNavService.setSiteId(site);
    this.router.navigate([`./${countryNname.replace(/\s/gi, '_')}`], {
      relativeTo: this.activatedRoute,
    });
  }

  public expandPanel(country: any, matExpansionPanel: MatExpansionPanel, event: Event) {
    this.leftMenuService.expandPanel(matExpansionPanel, event);
    if (this.leftMenuService.isExpansionIndicator(event.target)) {
      country.panelOpenState = !country.panelOpenState;
      return;
    }
    this.goToHomePage(country['name']);
  }
}
