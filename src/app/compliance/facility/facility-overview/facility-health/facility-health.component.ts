import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { PRIORITY_COLORS, PRIORITY_COLOR_NAMES } from '../../../../shared/mapping';

const HEALTH_STATES = [
  {
    name: 'All',
    value: 'all',
    colors: []
  },
  {
    name: 'Severe',
    value: 'severe',
    colors: ['red'],
  },
  {
    name: 'Moderate',
    value: 'moderate',
    colors: ['yellow'],
  },
  {
    name: 'Severe & Moderate',
    value: 'severe_moderate',
    colors: ['red', 'yellow'],
  },
];
const HEALTH_COLORS = {
    'all':  [],
    'severe':  ['red'],
    'moderate':  ['yellow'],
    'severe_moderate':  ['red', 'yellow'],
  };
@Component({
  selector: 'app-facility-health',
  templateUrl: './facility-health.component.html',
  styleUrls: ['./facility-health.component.scss'],
})
export class FacilityHealthComponent implements OnInit {
  @Input() overallFacilityHealth;
  @Input() facilityAreasHealthAll;
  @Input() facilityAreasColors;

  objectKeys = Object.keys;
  public activeHealthState = 'severe_moderate';
  public facilityAreasHealth = [];
  public healthStates = HEALTH_STATES;

  ngOnInit() {
    this.facilityAreasHealth = this.facilityAreasHealthAll;
    this.changeState('severe_moderate')
  }

  getHealthSum(data: any) {
    return data.normal + data.moderate;
  }

  public changeState(state) {
    console.log(state)
    if(state === 'all') {
      this.facilityAreasHealth = this.facilityAreasHealthAll;
      return
    }
    this.facilityAreasHealth = this.facilityAreasHealthAll.filter((area) => {
      return HEALTH_COLORS[state].includes(area.color.toLowerCase());
    });
  }
}
