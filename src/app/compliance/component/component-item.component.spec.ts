import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentItemComponent } from './component.component';

describe('ComplianceComponentItemComponent', () => {
  let component: ComplianceComponentItemComponent;
  let fixture: ComponentFixture<ComplianceComponentItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComplianceComponentItemComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplianceComponentItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
