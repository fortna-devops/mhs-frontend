import { Component, OnInit, ViewChild } from '@angular/core';
import { TokenService } from './shared/token.service';
import { TopNavService } from './shared/top-nav/top-nav.service';
import { environment } from '../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import {
  Router,
  ActivatedRoute,
  NavigationStart,
  NavigationEnd,
  ResolveEnd,
} from '@angular/router';
import { ComplianceComponent } from './compliance/compliance.component';
import { FacilityAreaComponent } from './compliance/facility-area/facility-area.component';
import { LeftMenuService } from './left-menu/left-menu.service';
import { CountryComponent } from './compliance/country/country.component';
import { RegionComponent } from './compliance/region/region.component';
import { ToasterConfig, ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { timer, interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthService } from './core/auth.service';
import { FacilityComponent } from './compliance/facility/facility.component';
import { ComponentItemComponent } from './compliance/component/component-item.component';
import { AssetComponent } from './compliance/asset/asset.component';
import { UploadMediaComponent } from './compliance/upload-media/upload-media.component';
import { ComplianceService } from './compliance/compliance.service';
import { SessionService } from './shared/session.service';

declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @ViewChild('rightMenuSidenav') rightMenuSidenav;

  title = 'app';
  public theme: string;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  private notificationTimeout = 60;
  private showNotificationTime = 1;
  private toaster: any;
  private sessionTimer: Subscription;
  private intervalCount: Subscription;
  public isSessionExpired: boolean;
  public isLogIn = false;

  constructor(
    private tokenService: TokenService,
    private topNavService: TopNavService,
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private toasterService: ToasterService,
    private authService: AuthService,
    private sessionService: SessionService,
    private complianceService: ComplianceService
  ) {
    tokenService.setUserGroups();
    translate.setDefaultLang('en');
    this.sessionService.setCognitoUserPool();

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });
  }

  ngOnInit() {
    this.topNavService.getEventLog().subscribe(() => {
      this.rightMenuSidenav.toggle();
    });

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.theme = theme;
    });

    this.sessionService.refreshSessionAsObservable().subscribe((sessionRefreshed) => {
      this.setExpirationWarning(sessionRefreshed);
    });

    this.router.events.subscribe((routerData) => {
      if (routerData instanceof ResolveEnd) {
        const customers = this.leftMenuService.getCustomersList();

        console.log(routerData.url);
        // if (routerData.url === '/home-screen' && customers.length > 0) {
        //   localStorage.setItem('isLoggedIn', 'false');
        //   this.tokenService.removeCookies();
        //   setTimeout(() => {
        //     location.reload();
        //   }, 200);
        // }
      }
      if (routerData instanceof NavigationStart) {
        const path = routerData['url'].split('/');

        if (this.leftMenuService.getLeftMenuLevels() === 2 && path[1] === '%23') {
          const leftMenuData = this.leftMenuService.getLeftMenuData();
          const activeSite = leftMenuData['facilities'][0]['site'];

          this.router.navigate([`./MHS/${activeSite}`], {
            relativeTo: this.activatedRoute,
          });
        }
      }
    });

    const theme = this.topNavService.getTheme();
    if (!theme) {
      this.topNavService.setTheme(environment.theme || 'theme-dark');
    } else {
      this.theme = theme;
    }

    const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();
    const customers = this.leftMenuService.getCustomersList();
    if (customers.length === 0) {
      this.router.navigate([`./home-screen`]);
      return;
    }

    this.setRoutes(leftMenuLevels);
    this.setSiteRoute(this.leftMenuService.getLeftMenuData(), leftMenuLevels);
    this.setExpirationWarning();
  }

  private loadScripts() {
    const dynamicScripts = ['mhsinsights.com/login/script.js', '../../../assets/js/dummyjs.min.js'];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  public setChangeScreenWidthEvents(event) {
    this.complianceService.setChangeScreenWidthEvent(event);
  }

  public openRightMenuSidenav() {
    this.rightMenuSidenav.toggle();
  }

  public switchLanguage(language: string) {
    this.translate.use(language);
  }

  private setSiteRoute(leftMenuData, leftMenuLevels) {
    const pathname = document.location.pathname;
    if (!pathname.split('/MHS/')[1] && (pathname === '/' || pathname === '/MHS')) {
      if (leftMenuLevels === 2) {
        const activeSite = leftMenuData['facilities'][0]['site'];
        this.router.navigate([`./MHS/${activeSite}`], {
          relativeTo: this.activatedRoute,
        });
      }

      if (leftMenuLevels === 4) {
        const country = leftMenuData['countries'][0]['name'].replace(/\s/gi, '_');
        this.router.navigate([`./MHS/${country}`], {
          relativeTo: this.activatedRoute,
        });
      }
    }
  }

  private setRoutes(leftMenuLevels) {
    const config = this.router.config;
    let route = {};

    if (leftMenuLevels === 2) {
      route = {
        path: 'MHS',
        component: ComplianceComponent,
        children: [
          { path: ':site', component: FacilityComponent },
          { path: ':site/:facilityArea', component: FacilityAreaComponent },
          { path: ':site/:facilityArea/:asset', component: AssetComponent },
          {
            path: ':site/:facilityArea/:asset/more-details',
            component: UploadMediaComponent,
          },
          {
            path: ':site/:facilityArea/:asset/:component',
            component: ComponentItemComponent,
          },
          {
            path: ':site/:facilityArea/:asset/:component/more-details',
            component: UploadMediaComponent,
          },
        ],
      };
    }

    if (leftMenuLevels === 4) {
      route = {
        path: 'MHS',
        component: ComplianceComponent,
        children: [
          { path: ':country', component: CountryComponent },
          {
            path: ':country/:region',
            component: RegionComponent,
          },
          {
            path: ':country/:region/:site',
            component: FacilityComponent,
          },
          {
            path: ':country/:region/:site/:facilityArea',
            component: FacilityAreaComponent,
          },
          {
            path: ':country/:region/:site/:facilityArea/:asset',
            component: AssetComponent,
          },
          {
            path: ':country/:region/:site/:facilityArea/:asset/more-details',
            component: UploadMediaComponent,
          },
          {
            path: ':country/:region/:site/:facilityArea/:asset/:component',
            component: ComponentItemComponent,
          },
          {
            path: ':country/:region/:site/:facilityArea/:asset/:component/more-details',
            component: UploadMediaComponent,
          },
        ],
      };
    }

    this.router.resetConfig(config.map((item) => (item.path === 'MHS' ? route : item)));
  }

  private setExpirationWarning(sessionRefreshed = false) {
    if (sessionRefreshed) {
      console.log('Session is Refreshed!');
      this.toasterService.clear();
      this.sessionTimer.unsubscribe();
      this.intervalCount.unsubscribe();
    }

    const cookieExpirationTime = new Date(this.tokenService.getCookieExpirationTime());
    const cookieExpirationTimeStamp = new Date(cookieExpirationTime);
    cookieExpirationTimeStamp.setMinutes(
      cookieExpirationTime.getMinutes() - this.showNotificationTime
    );
    let countdown = cookieExpirationTimeStamp.getTime() - new Date().getTime();

    // console.log('cookieExpirationTime:');
    // console.log(cookieExpirationTime);

    // console.log('countdown');
    // console.log(countdown);

    if (countdown <= 0) {
      this.sessionExpiration();
      return;
    }

    this.sessionTimer = timer(countdown).subscribe((x) => {
      this.intervalCount = interval(1000)
        .pipe(take(this.notificationTimeout))
        .subscribe(
          (counter) => {
            this.toasterService.clear();

            const toast: Toast = {
              type: 'warning',
              timeout: 1000,
              body: `<div>Your session will expire in ${
                this.notificationTimeout - counter
              } seconds!</div>`,
              bodyOutputType: BodyOutputType.TrustedHtml,
              showCloseButton: false,
              clickHandler: (t, isClosed): boolean => {
                console.log('Refresh Session!');
                setTimeout(() => {
                  this.sessionService.onRefreshSession();
                }, 400);
                return true;
              },
            };
            this.toaster = this.toasterService.pop(toast);
          },
          (err) => {},
          () => {
            this.sessionExpiration();
          }
        );
    });
  }

  private sessionExpiration() {
    // Logout
    // this.authService.removeCookies();
    // setTimeout(() => {
    //   // window.location.href = '.';
    //   window.location.assign('.');
    // }, 200);

    this.isSessionExpired = true;

    if (!this.toasterService) {
      this.toasterService.clear();
    }

    const toast: Toast = {
      type: 'warning',
      timeout: 10000000,
      body: `<i class="material-icons toaster-icon">warning</i>
            <div>Session expired!</div>
            <div>Please login.</div>
            <div class="btn btn-outline-light float-right">Login</div>`,
      bodyOutputType: BodyOutputType.TrustedHtml,
      showCloseButton: false,
      onHideCallback: (toastCallback) => {
        localStorage.setItem('isLoggedIn', 'false');
        this.tokenService.removeCookies();
        setTimeout(() => {
          location.reload();
          // window.location.href = '.';
          // window.location.assign('.');
          // window.location.reload(true);
        }, 200);
      },
    };

    timer(1000).subscribe((x) => {
      this.toaster = this.toasterService.pop(toast);
    });
  }
}
