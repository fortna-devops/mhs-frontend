import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplianceService } from '../compliance/compliance.service';
import { fadeOutAnimation } from '../compliance/fade.animation';
import { ToasterConfig, ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { ResizeEvent } from 'angular-resizable-element';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  animations: [fadeOutAnimation],
})
export class ReportComponent implements OnInit {
  public events: string[] = [];
  public opened = true;
  public opened2 = true;
  public activeNode: any;
  public rightMenuTitle = 'Zone Compliance';
  public showAssetsMenu = false;
  public style: object = {};
  public contentStyle: object = {};

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private complianceService: ComplianceService
  ) {
    iconRegistry.addSvgIcon(
      'open_icon',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/compliance/icons/list-view-charts.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'plus_icon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/plus.svg')
    );
    iconRegistry.addSvgIcon(
      'menu_icon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/menu-btn.svg')
    );
  }

  ngOnInit() {}

  onResizeEnd(event: ResizeEvent): void {
    this.style = {
      width: `${event.rectangle.width}px`,
    };
    this.contentStyle = {
      'margin-left': `${event.rectangle.width}px`,
    };
  }

  public goToPage(page) {
    const mainPath = document.location.pathname.split('/MHS/')[1];
    this.router.navigate([`./${mainPath}/${page}`], { relativeTo: this.activatedRoute });
  }

  public setChangeScreenWidthEvents(event) {
    this.complianceService.setChangeScreenWidthEvent(event);
  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
