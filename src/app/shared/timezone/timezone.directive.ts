import { Directive, ElementRef, Input } from '@angular/core';
import { TimezoneService } from './timezone.service';
import * as moment from 'moment';

@Directive({
  selector: '[appTimezone]'
})
export class TimezoneDirective {

  @Input('appTimezone') date: string;
  
  constructor(private el: ElementRef, private timezoneService: TimezoneService) {}

  ngOnInit() {
    this.setTimeWithOffset()
  }

  setTimeWithOffset(){
    const offsetTime = this.timezoneService.getDateWithOffset(this.date);
    this.el.nativeElement.innerHTML = moment(offsetTime).format('MMM D YYYY  H:mm:ss');
  }
}
