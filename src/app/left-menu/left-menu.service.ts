import { Injectable } from '@angular/core';
import { TokenService } from '../shared/token.service';
import { TopNavService } from '../shared/top-nav/top-nav.service';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { MatExpansionPanel } from '@angular/material';
import { AuthService } from '../core/auth.service';
import { Router } from '@angular/router';
// import compareVersions from 'compare-versions';
import * as semver from 'semver';
import { ToasterConfig, ToasterService, Toast, BodyOutputType } from 'angular2-toaster';
import { timer } from 'rxjs';
import { TimezoneService } from '../shared/timezone/timezone.service';

const VERSIONS = {
  major: false,
  minor: false,
  patch: true,
  null: true,
};

@Injectable({
  providedIn: 'root',
})
export class LeftMenuService {
  private leftMenuLevels: number;
  private facilityName: string;
  private leftMenu = {};
  public customersList: any;
  private leftMenuLevelTypes = { countries: 4, facilities: 2 };
  private toaster: any;

  constructor(
    private http: HttpClient,
    private token: TokenService,
    private timezoneService: TimezoneService,
    // private router: Router,
    private toasterService: ToasterService,
    private topNavService: TopNavService,
    private authService: AuthService
  ) {}

  // TODO: remove redundant left-menu calls
  public getLeftMenu() {
    const params = `customer=${this.topNavService.getCustomer()}`;
    const url = `${environment.apiURL}/left-menu?${params}`;
    this.http
      .get<any>(url, { headers: this.createHttpHeader() })
      .subscribe(
        (res) => {},
        (err) => {
          alert(err);
        }
      );
  }

  private getCustomers() {
    const url = `${environment.apiURL}/customers`;
    return this.http.get(url, { headers: this.createHttpHeader() });
  }

  public isCustomers() {
    if (this.token.getVersion()) {
      const isVersionMatch = VERSIONS[semver.diff(this.token.getVersion(), environment.version)];
      if (!isVersionMatch || this.token.getCookie('version_match') === 'false') {
        this.token.setCookie('version_match', JSON.stringify(false));
        this.onDiffVersion();
      }
    }

    if(!this.token.toString()) {
      localStorage.setItem('isLoggedIn', 'false');
      this.token.removeCookies();
      setTimeout(() => {
        console.log('Location Reload')
        location.reload();
      }, 200);
    }

    const promise = new Promise((resolve, reject) => {
      this.getCustomers()
        .toPromise()
        .then(
          (response) => {
            console.log(response)
            const custList = response['customers'];
            // const activeLeftMenu = menuResult[this.topNavService.getCustomer()];
            this.token.setCookie('customers', JSON.stringify(custList));
            this.customersList = custList;
            
            const cookieCustomer = this.token.getCustomer();

            let customer: any = {};
            if (!cookieCustomer && custList.length > 0) {
              customer = custList[0];
              this.token.setCustomer(customer.id);
            } else {
              customer = custList.find(cust => cust.id == cookieCustomer);
            }

            this.timezoneService.setTimezoneOffsetList(customer);

            // this.token.setCookie('sites', JSON.stringify(customer['sites']));
            // this.token.setCookie('site', customer['sites'][0].id);

            // this.getLeftMenuPromise().then((res) => {
            if (
              custList.length > 0 &&
              Array.isArray(customer['sites']) &&
              customer['sites'].length > 0
            ) {
              this.token.setCookie('sites', JSON.stringify(customer['sites']));
              this.token.setCookie('site', customer['sites'][0].id);
            }

            this.getLeftMenuPromise()
              .then((res) => {
                resolve();
              })
              .catch((err) => {
                resolve();
              });
          },
          (msg) => {
            localStorage.setItem('isLoggedIn', 'false');
            this.token.removeCookies();
            setTimeout(() => {
              console.log('Location Reload')
              location.reload();
            }, 200);
          }
        );
    });
    return promise;
  }

  private onDiffVersion() {
    if (!this.toasterService) {
      this.toasterService.clear();
    }

    const toast: Toast = {
      type: 'info',
      timeout: 10000000,
      body: `<i class="material-icons toaster-icon">warning</i>
            <div>You are currently using an outdated version of the application. </div>
            <div>Please re-login.</div>
            <div class="btn btn-outline-light float-right">Re-Login</div>`,
      bodyOutputType: BodyOutputType.TrustedHtml,
      showCloseButton: true,
      clickHandler: (t, isClosed): boolean => {
        // got clicked and it was NOT the close button!
        if (!isClosed) {
          localStorage.setItem('isLoggedIn', 'false');
          this.token.removeCookies();
          setTimeout(() => {
            location.reload();
          }, 200);
        }
        return true;
      },
    };

    timer(1000).subscribe((x) => {
      this.toaster = this.toasterService.pop(toast);
    });
  }

  public getLeftMenuPromise() {
    const params = `customer=${this.topNavService.getCustomer()}`;
    const url = `${environment.apiURL}/left-menu?${params}`;
    const promise = new Promise((resolve, reject) => {
      this.http
        .get(url, { headers: this.createHttpHeader() })
        .toPromise()
        .then(
          (activeLeftMenu) => {

            const activeLevelType = Object.keys(activeLeftMenu)[0];
            const levels = this.leftMenuLevelTypes[activeLevelType];
            this.setLeftMenuLevels(levels);

            if (
              levels === 2 &&
              activeLeftMenu &&
              activeLeftMenu[this.getMenuLevelKey(levels)] &&
              activeLeftMenu[this.getMenuLevelKey(levels)][0]
            ) {
              const facilityName = activeLeftMenu[this.getMenuLevelKey(levels)][0]['name'];
              this.setFacilityName(facilityName);
            }

            this.setLeftMenuData(activeLeftMenu);

            if(!this.token.getTimezone()) {
              this.timezoneService.getTimezone().subscribe((res: any) => {
                const tz = res && res.timezone ? res.timezone : 'utc';
                this.token.setTimezone(tz);
                this.timezoneService.setTimezoneOffset(tz);
                resolve('');
              });
            } else {
              resolve('');
            }
          },
          (msg) => {
            reject(msg);
          }
        );
    });
    return promise;
  }

  public getAssetName(assetId, paramList) {
    let result = '';

    if (this.leftMenuLevels === 2) {
      this.leftMenu['facilities']
        .filter((item) => item['site'] === paramList['site'])
        .forEach((facility) => {
          facility['facility_areas']
          .filter((facilityArea) => facilityArea['name'] === paramList['facilityArea'])
          .forEach((facility_area) => {
            facility_area['assets'].forEach((asset) => {
              if (assetId == asset['id']) {
                result = asset['name']
              }
            });
          });
        });
    }

    if (this.leftMenuLevels === 4) {
      this.leftMenu['countries']
        .filter(
          (country) =>
            country['name'].replace(/\s/gi, '_') === paramList['country'].replace(/\s/gi, '_')
        )
        .forEach((country) => {
          country['regions']
            .filter(
              (region) =>
              region['name'].replace(/\s/gi, '_') ===
                paramList['region'].replace(/\s/gi, '_')
            )
            .forEach((region) => {
              region['facilities']
                .filter((facility) => facility['site'] === paramList['site'])
                .forEach((facility) => {
                  facility['facility_areas']
                  .filter((facilityArea) => facilityArea['name'] === paramList['facilityArea'])
                  .forEach((facilityArea) => {
                    facilityArea['assets'].forEach((asset) => {
                      if (assetId == asset['id']) {
                        result = asset['name']
                      }
                    });
                  });
                });
            });
        });
    }

    return result;
  }

  public setLeftMenuLevels(levels) {
    this.leftMenuLevels = +levels;
  }

  public getLeftMenuLevels() {
    return this.leftMenuLevels;
  }

  public getCustomersList() {
    return this.customersList;
  }

  public setFacilityName(name) {
    this.facilityName = name;
  }

  public getFacilityName() {
    return this.facilityName;
  }

  public setLeftMenuData(leftMenu) {
    if (leftMenu && leftMenu['countries']) {
      this.leftMenu['countries'] = leftMenu['countries'];
    }

    if (leftMenu && leftMenu['facilities']) {
      this.leftMenu['facilities'] = leftMenu['facilities'];
    }
  }

  public getLeftMenuData() {
    return this.leftMenu;
  }

  public getMenuLevelKey(levels) {
    let menuLevelKey = '';
    switch (levels) {
      case 2:
        menuLevelKey = 'facilities';
        break;
      case 3:
        menuLevelKey = 'regions';
        break;
      case 4:
        menuLevelKey = 'countries';
        break;
    }

    return menuLevelKey;
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString(),
    });
  }

  public expandPanel(matExpansionPanel: MatExpansionPanel, event: Event): void {
    event.stopPropagation();

    if (!this.isExpansionIndicator(event.target) && matExpansionPanel['_expanded']) {
      matExpansionPanel.close();
      return;
    }

    if (!this.isExpansionIndicator(event.target) && !matExpansionPanel['_expanded']) {
      matExpansionPanel.open();
      return;
    }
  }

  public isExpansionIndicator(target: EventTarget): boolean {
    const expansionIndicatorClass = 'expansion-indicator';
    return target['classList'] && target['classList'].contains(expansionIndicatorClass);
  }
}
