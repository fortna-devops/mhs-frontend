import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActiveFiltersService {
  private activeFilters: object = {};

  constructor() { }

  public getActiveFilters() {
    if (Object.keys(this.activeFilters).length > 0) {
      return this.activeFilters;
    } else {
      return JSON.parse(localStorage.getItem('activeFilters')) || {};
    }
  }

  public removeActiveFilters() {
    localStorage.removeItem('activeFilters');
  }

  public doExistActiveFilter(filter) {
    const activeFilters = this.getActiveFilters();
    return activeFilters && Object.keys(activeFilters).length > 0 && activeFilters[filter];
  }

  public doExistActiveDateFilter() {
    return this.doExistActiveFilter('dateRangeSelected') &&
      this.doExistActiveFilter('minDate') &&
      this.doExistActiveFilter('maxDate') &&
      this.doExistActiveFilter('dateRangeMin') &&
      this.doExistActiveFilter('dateRangeMax');
  }

  public setActiveFilter(key, value) {
    const activeFilters = this.getActiveFilters();
    activeFilters[key] = value;
    localStorage.setItem('activeFilters', JSON.stringify(activeFilters));
  }

  public getActiveFilter(key) {
    const activeFilters = this.getActiveFilters();
    return activeFilters[key];
  }
}
