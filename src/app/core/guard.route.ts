import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { TokenService } from '../shared/token.service';

@Injectable()
export class GuardRoute implements CanActivate {
    constructor(
      private tokenService: TokenService,
      private authService: AuthService
      ) { }

    canActivate(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
      if (this.authService.isUserAdmin()) {
        return true;
      } else {
        window.alert('You don\'t have permission to view this page');
        return false;
      }

      // if (this.tokenService.getSiteId() === 'fedex-louisville') {
      //   return true;
      // } else {
      //   window.alert('You don\'t have permission to view this page');
      //   return false;
      // }
    }
}
