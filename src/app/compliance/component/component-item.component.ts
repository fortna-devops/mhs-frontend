import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ComplianceService } from '../compliance.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetService, DEFAULT_PAGE_SIZE_DESKTOP } from '../asset/asset.service';
import { MatTabGroup, MatPaginator } from '@angular/material';
import { Subscription } from 'rxjs';
import { FavoritesService } from '../favorites/favorites.service';
import { CacheService } from '../../shared/cache.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { DeviceDetectorService } from 'ngx-device-detector';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { TimezoneService } from '../../shared/timezone/timezone.service';

@Component({
  selector: 'app-component',
  templateUrl: './component-item.component.html',
  styleUrls: ['./component-item.component.scss'],
})
export class ComponentItemComponent implements OnInit, OnDestroy {
  @ViewChild('tabs') tabGroup: MatTabGroup;
  @ViewChild('paginator') paginator: MatPaginator;

  public selectedIndex: number | null;
  private tabsObj = {
    layout: 0,
    overview: 1,
    analysis: 2,
  };
  public componentId: string;
  public componentName: string;
  public metricsList: Array<any> = [];
  public metricIds: Array<number> = [];
  public componentHealth: number;
  public runtime: string;
  public assetId: string;
  public assetName: string;
  public site: string;
  public activeTab = 0;
  public isLoading = true;

  public href: string;
  public isFavorite = false;
  private favoritesubscription: Subscription;
  private routeSubscription: Subscription;

  public acknowledgements: any;
  public alarms: any;
  public componentLabel: string;
  public details: any;

  private allIssues: Array<any> = [];
  public issues: Array<any> = [];
  public showPaginator = true;
  public pageSize: number;
  public logLength = 0;

  constructor(
    private router: Router,
    private complianceService: ComplianceService,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private assetService: AssetService,
    private leftMenuService: LeftMenuService,
    private timezoneService: TimezoneService,
    private favoritesService: FavoritesService,
    private cacheService: CacheService,
    private deviceDetectorService: DeviceDetectorService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.selectedIndex = this.tabsObj[params['tab']] || 0;
    });
  }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.href = this.router.url;
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;
      this.selectedIndex = favObj.selectedIndex;
      this.componentId = params['component'].replace('component-', '');
      this.componentLabel = this.complianceService.getActiveComponent(this.componentId);
      this.assetId = params['asset'].replace('asset-', '');
      this.assetName = this.leftMenuService.getAssetName(this.assetId, params);
      this.site = params['site'];
      this.timezoneService.setSite(params['site'])
      this.getDetails();
      this.getAlarmsData();
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
        this.selectedIndex = favObj.selectedIndex;
      }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }

  private getDetails() {
    this.details = null;
    const cachedDetailsData = this.cacheService.getComponentDetails();

    let detailsData = {};
    if (cachedDetailsData && Array.isArray(cachedDetailsData) && cachedDetailsData.length > 0) {
      detailsData = cachedDetailsData.find(
        (item) => item['site'] === this.site && item['assetId'] === this.assetId
      );

      if (detailsData && Object.entries(detailsData['data']).length > 0) {
        this.details = detailsData['data'];
        this.componentName = this.details['name'];
      } else {
        this.getDetailsData();
      }
    } else {
      this.getDetailsData();
    }
  }

  private getDetailsData() {
    this.complianceService
      .getComponentDetails({ site: this.site, component_ids: this.componentId })
      .subscribe((response) => {
        this.details = response['component_details'][0];
        this.componentName = this.details['name'];
        this.metricsList = [];

        this.details['data_sources'].map(source => {
          this.metricsList.push(...source['metrics'])
        });

        this.metricIds = this.metricsList.map(metric => metric['id'])
        this.componentHealth = this.details['health']
      });
  }

  private getAlarmsData() {
    const alarmsForAllSites = this.cacheService.getAlarms();
    let alarms = {};
    if (Array.isArray(alarmsForAllSites)) {
      alarms = alarmsForAllSites.find((item) => item['site'] === this.site);
    }

    if (
      alarms &&
      Object.entries(alarms).length > 0
    ) {
      const alarmList = alarms['result'].filter((item) => item['component']['id'] === this.componentId);
      this.alarms = this.assetService.setAlarms(alarmList, this.site);
    } else {
      this.alarms = [];
      this.complianceService
        .getComponentAlarms({ site: this.site, component_ids: this.componentId, count: true })
        .subscribe((res) => {
          let limit = 300;
          let parts = 0;
          let diff = +res['count'] / limit;
          parts = diff > 0 && diff < 1 ? 1 : parts;

          // no need for request if no alarms
          if(parts === 0) { 
            return 
          }

          // sending alarms request using set limit so the request won't fail
          for(let i = 0; i < parts; i++) { 
            this.complianceService
            .getComponentAlarms({ 
              site: this.site, 
              component_ids: this.componentId,
              limit: limit,
              offset: limit * i
            }).subscribe((result) => {
              this.alarms.push(...this.assetService.setAlarms(result['alarms'], this.site));
              this.alarms = this.alarms.slice();
            });
          }
        });
    }
  }
  
  public changePageHeader(tabIndex) {
    this.selectedIndex = tabIndex || 0;
    setTimeout(() => {
      this.complianceService.setChangeScreenWidthEvent(event);
    }, 100);
  }

  private updateCharts() {
    if (typeof(Event) === 'function') {
      // modern browsers
      window.dispatchEvent(new Event('resize'));
    } else {
      // for IE and other old browsers
      // causes deprecation warning on modern browsers
      var resizeEvent = window.document.createEvent('UIEvents'); 
      resizeEvent.initUIEvent('resize', true, false, window, 0); 
      window.dispatchEvent(resizeEvent);
    }
  }
  
  public goBack() {
    this.location.back();
  }

  public onVoted(event) {
    // this.tabGroup.selectedIndex = 1;
    this.activeTab = 1;
  }

  public onLoading(event) {
    // this.tabGroup.selectedIndex = 1;
    this.isLoading = event;
  }
}
