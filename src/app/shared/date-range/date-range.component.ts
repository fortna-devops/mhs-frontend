import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { NgbCalendar, NgbDate, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { TokenService } from '../token.service';

@Component({
  selector: 'app-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.scss'],
})
export class DateRangeComponent implements OnInit, OnChanges {
  @ViewChild('dp') datePicker: NgbInputDatepicker;
  @Output() dateRangeChange = new EventEmitter<any>();
  @Input() isDisabled;

  public startD: any = new Date();
  public endD: any = new Date();
  public minStartDate: any;
  public maxStartDate: any;
  public minEndDate: any;
  public maxEndtDate: any;
  public maxDate: any;
  public minDate: any;
  public fromDate: any;
  public toDate: any;
  public formattedDateRange: string;
  public hoveredDate: NgbDate;

  // TODO: check
  public queryParams: any;

  constructor(
    private calendar: NgbCalendar,
    private router: Router,
    private tokenService: TokenService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    // tslint:disable-next-line:max-line-length
    iconRegistry.addSvgIcon(
      'date-range-calendar',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/compliance/icons/date-range-calendar.svg'
      )
    );
  }

  ngOnInit() {
    this.initOperationTime();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.isDisabled) {
      // this.dataSource = this.assetAlarms;
      // debugger
    }
  }

  // initial set of Operation Dates
  public initOperationTime() {
    const minDate = new Date();
    const localTzOffset = minDate.getTimezoneOffset(); 
    let startDate = moment(minDate).subtract(7, 'days');

    let end = moment(this.endD || new Date());

    if(this.tokenService.getTimezone() !== 'local') {
      startDate = startDate.add(localTzOffset, 'minutes');
      end = end.add(localTzOffset, 'minutes');
    }


    this.maxDate = NgbDate.from({year: end.year(), month: end.month() + 1, day: end.date()});
    this.minDate = this.calendar.getPrev(this.maxDate, 'm', 12); 
    
    this.startD = startDate.toISOString();
    this.fromDate = this.calendar.getPrev(this.maxDate, 'd', 7);

    this.endD = end;
    this.toDate = this.maxDate;

    this.formattedDateRange = this.getFormattedDateRange(this.fromDate, this.toDate);
  }

  private getFormattedDateRange(fromDate, toDate) {
    return `${this.formatDateTwoDigit(fromDate.month)}/${this.formatDateTwoDigit(fromDate.day)}/${fromDate.year
    } - ${this.formatDateTwoDigit(toDate.month)}/${this.formatDateTwoDigit(toDate.day)}/${toDate.year}`;
  }

  public formatDateTwoDigit(date) {
    return date < 10 ? '0' + date : '' + date 
  }

  public changeDateRange(date) {
    console.log('changeDateRange:')
    console.log(date)
    console.log('this.fromDate:')
    console.log(this.fromDate)
    console.log('this.toDate:')
    console.log(this.toDate)
    if (this.fromDate && this.toDate) {
      const minDate = new Date();
      const localTzOffset = minDate.getTimezoneOffset(); 

      let fromDate = moment([
        this.fromDate.year,
        this.fromDate.month - 1,
        this.fromDate.day,
      ]);
      let toDate = moment([
        this.toDate.year,
        this.toDate.month - 1,
        this.toDate.day,
      ]);

      this.startD = fromDate;
      this.endD = toDate;
      this.formattedDateRange = this.getFormattedDateRange(this.fromDate, this.toDate);
      this.dateRangeChange.emit({
        startD: this.startD,
        endD: this.endD,
      });

      this.datePicker.close();
    } else {
      this.closePicker();
    }
  }

  public closePicker() {
    this.datePicker.close();
    this.fromDate = this.formatDatePicketTime(this.startD);
    this.toDate = this.formatDatePicketTime(this.endD);
    this.formattedDateRange = this.getFormattedDateRangeTimestamp(this.startD, this.endD);
  }

  public datePickerToggle() {
    this.datePicker.toggle();
    if (!this.datePicker.isOpen()) {
      this.fromDate = this.formatDatePicketTime(this.startD);
      this.toDate = this.formatDatePicketTime(this.endD);
      this.formattedDateRange = `${moment(this.startD).format('YYYY/M/D')} - ${moment(
        this.endD
      ).format('YYYY/M/D')}`;
    }
  }

  public isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      date.equals(this.toDate) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  public isHovered(date: NgbDate) {
    return (
      this.fromDate &&
      !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.hoveredDate)
    );
  }

  public isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  private formatDatePicketTime(time) {
    return {
      year: +moment(time).format('YYYY'),
      month: +moment(time).format('M'),
      day: +moment(time).format('D'),
    };
  }

  public onDateSelection(date: NgbDate) {
    console.log('onDateSelection:')
    console.log(date)
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.formattedDateRange = this.getFormattedDateRangeTimestamp(this.startD, this.endD);
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      if(this.dateDiffInDays(date, this.fromDate) > 21) {
        this.toDate = this.calendar.getNext(this.fromDate, 'd', 20);
        this.formattedDateRange = this.getFormattedDateRangeTimestamp(this.fromDate, this.toDate);
      } else {
        this.toDate = date;
        this.formattedDateRange = this.getFormattedDateRangeTimestamp(this.startD, this.endD);
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
      // hack
      this.formattedDateRange = this.formattedDateRange + ' ';
    }

    if (this.fromDate && this.toDate) {
      this.formattedDateRange = this.getFormattedDateRange(this.fromDate, this.toDate);
    }
    this.clearQueryParams()
  }

  private dateDiffInDays(last, first) {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    const utc1 = Date.UTC(first.year, first.month - 1, first.day);
    const utc2 = Date.UTC(last.year, last.month - 1, last.day);
  
    return Math.floor((utc2 - utc1) / _MS_PER_DAY) + 1;
  }

  private getFormattedDateRangeTimestamp(fromDate, toDate) {
    return `${moment(fromDate).format('YYYY/M/D')} - ${moment(toDate).format('YYYY/M/D')}`;
  }

  private clearQueryParams() {
    this.router.navigate([], {});
  }
}
