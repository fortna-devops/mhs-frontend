import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { PRIORITY_COLORS } from '../../../../shared/mapping';
import { ComplianceService } from '../../../compliance.service';

@Component({
  selector: 'app-component-event-log',
  templateUrl: './component-event-log.component.html',
  styleUrls: ['./component-event-log.component.scss'],
})
export class ComponentEventLogComponent implements OnInit, OnChanges {
  @Input() alarms;
  @Output() activeTab = new EventEmitter<number>();

  public displayedColumns: string[] = ['severity', 'description', 'date'];
  public dataSource: any;

  constructor(private complianceService: ComplianceService) {}

  ngOnInit() {
    // this.dataSource = this.alarms.map(item => {
    //   item['asset'] = item['component_type'];
    //   item['tag_name'] = item['metric'];
    //   item['dataTimestamp'] = item['data_timestamp'];
    //   item['class'] = `circle circle--${PRIORITY_COLORS[item['severity']]}`;
    //   return item;
    // });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (Array.isArray(changes.alarms.currentValue)) {
      this.dataSource = this.alarms.map((item) => {
        item['asset'] = item['component_type'];
        item['tag_name'] = item['metric'];
        item['dataTimestamp'] = item['data_timestamp'];
        item['class'] = `circle circle--${PRIORITY_COLORS[item['severity']]}`;
        return item;
      });
    }
  }

  public goToAlarm(alarm) {
    this.complianceService.setExpandedChart(alarm['metric']);
  }
}
