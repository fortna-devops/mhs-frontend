import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ComplianceService } from '../../compliance/compliance.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { LocationText } from '../../compliance/sensors.enum';
import { Subscription } from 'rxjs';
import { LeftMenuService } from '../left-menu.service';

@Component({
  selector: 'app-component-items',
  templateUrl: './component-items.component.html',
  styleUrls: ['./component-items.component.scss'],
})
export class ComponentItemsComponent implements OnInit, OnDestroy {
  @Input() components;

  public componentList: Array<any>;
  private subscription: Subscription;

  constructor(
    private complianceService: ComplianceService,
    private topNavService: TopNavService,
    private leftMenuService: LeftMenuService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.componentList = this.getComponentList(this.components);
    this.subscription = this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const paths = ev['url'].substr(5).split('/');
        this.componentList = this.componentList.map((component) => {
          component['isActive'] = false;
          return component;
        });
        this.getIsComponentActive(paths);
      }
    });
  }

  ngOnDestroy() {
    // TODO check why error
    // this.subscription.unsubscribe();
  }

  private getIsComponentActive(paths) {
      this.componentList = this.componentList.map((component) => {

        if (this.leftMenuService.getLeftMenuLevels() === 4) {
          const activeComponent = paths[5];
          component['isActive'] = activeComponent ? component['code'] == activeComponent.replace('component-', '') : false;
          return component;
        }

        if (this.leftMenuService.getLeftMenuLevels() === 2) {
          const activeComponent = paths[3];
          component['isActive'] = activeComponent ? component['code'] == activeComponent.replace('component-', '') : false;
          return component;
        }

        return component;
      });
  }

  private getComponentList(components) {
    
    return components.map((item) => {
      let imageId = item['type']['name'].toLowerCase();
      // tslint:disable-next-line: curly
      if (imageId.includes('VFD')) imageId = 'VFD';

      return {
        code: item['id'],
        asset: item['asset'],
        name: item['name'],
        icon: this.getIconPath(item['color'], this.getLocationText(imageId)) ,
        link: item['link'],
        isActive: this.isActiveComponent(item['id'], item['asset']),
      };
    });
  }

  getLocationText(location) {
    const text = LocationText[location.toUpperCase()];
    return text ? text : 'Belt'
  }
  private getIconPath(color, component) {
    const assetIconFile = `${this.topNavService.getTheme()}-${component
      .toLowerCase()
      .replace(' ', '-')}-${color.toLowerCase()}.svg`;
    return `assets/images/compliance/icons/${assetIconFile}`;
  }

  public goToComponentPage(component) {
    const url = `${component['link'].replace(/\s/gi, '_')}/component-${component['code']}`;
    this.router.navigate([url], { relativeTo: this.activatedRoute });
  }

  private isActiveComponent(component, asset) {
    const paths = document.location.pathname.substr(1).split('/').filter(Boolean);
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      const activeComponent =  paths[6] ? paths[6].replace('component-', '') : '';
      const activeAsset = paths[5] ? paths[5].replace('asset-', '') : '';
      return asset == activeAsset && component == activeComponent;
    }

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const activeComponent =  paths[4] ? paths[4].replace('component-', '') : '';
      const activeAsset = paths[3] ? paths[3].replace('asset-', '') : '';
      return asset == activeAsset && component == activeComponent;
    }
  }
}
