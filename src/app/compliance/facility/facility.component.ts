import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FacilityOverviewService } from './facility-overview/facility-overview.service';
import { Subscription } from 'rxjs';
import { FavoritesService } from '../favorites/favorites.service';
import { ComplianceService } from '../compliance.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { TimezoneService } from '../../shared/timezone/timezone.service';

@Component({
  selector: 'app-facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.scss'],
})
export class FacilityComponent implements OnInit, OnDestroy {
  public selectedIndex: number | null;
  private tabsObj = {
    layout: 0,
    overview: 1,
    analysis: 2,
  };
  public header = 'Layout';
  private params: object;
  public activeTab: number;
  private routeSubscription: Subscription;
  public href: string;
  public isFavorite = false;
  public activeTabDefined = false;

  favoritesubscription: Subscription;

  constructor(
    private router: Router,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private timezoneService: TimezoneService,
    private overviewService: FacilityOverviewService,
    private favoritesService: FavoritesService,
    private complianceService: ComplianceService,
    private httpClient: HttpClient
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.activeTab = params['activeTab'] || 0;
    });

    iconRegistry.addSvgIcon(
      'listview-light',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/list-view-light.svg')
    );
    iconRegistry.addSvgIcon(
      'listview-dark',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/list-view-dark.svg')
    );
  }

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.href = this.getUrlWithoutParams();
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;
      if (params['tab']) {
        this.selectedIndex = params['tab'];
      } else {
        this.selectedIndex = 0;
      }
      this.activeTabDefined = true;
      this.params = params;
      this.timezoneService.setSite(params['site'])
      this.header = this.overviewService.getTitle(this.params);
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
      }
    });
  }

  getUrlWithoutParams() {
    const urlTree = this.router.parseUrl(this.router.url);
    urlTree.queryParams = {};
    return urlTree.toString();
  }

  public changeActiveTab(tabIndex) {
    this.selectedIndex = tabIndex;
    setTimeout(() => {this.updateCharts()}, 500);
  }

  private updateCharts() {
    if (typeof(Event) === 'function') {
      // modern browsers
      window.dispatchEvent(new Event('resize'));
    } else {
      // for IE and other old browsers
      // causes deprecation warning on modern browsers
      var resizeEvent = window.document.createEvent('UIEvents'); 
      resizeEvent.initUIEvent('resize', true, false, window, 0); 
      window.dispatchEvent(resizeEvent);
    }
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }

  private setActiveTab(activeTab) {
    this.router.navigate([], {
      queryParams: {
        activeTab: activeTab,
      },
    });
  }
}
