import { Injectable } from '@angular/core';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { TokenService } from '../../shared/token.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CountryService {
  constructor(private http: HttpClient, private token: TokenService) {}

  public getOverviewData(leftMenuData, country) {
    let overviewData = {};
    const countryData = leftMenuData['countries'].filter(
      (item) => item['name'].replace(/\s/gi, '_') === country
    );

    if (countryData && Array.isArray(countryData) && countryData.length > 0) {
      countryData.forEach((item) => {
        overviewData = item['overview'];
      });
    }
    return {
      overviewData,
      countryData,
    };
  }

  public getRegions(overview, countryData) {
    const paths = {};
    let regions = [];

    countryData.forEach((country) => {
      country['regions'].forEach((region) => {
        const facilities = region.facilities;
        region['facilities'].forEach((facility) => {
          paths[facility['name']] = `./${region['name']}/${facility['site']}`;
        });
        regions.push({
          name: region.name.replace(/\s/gi, '_'),
          facilities: facilities,
          overview: region['overview'],
          num_red_assets: 0,
          num_yellow_assets: 0,
          num_green_assets: 0,
        });
      });
    });

    return regions.map((region) => {
      region['facilities'].map((facility, index) => {
        const found = region['overview']['facilities'].find((item) => item.name === facility.name);
        if (found) {
          region['num_red_assets'] += found['num_red_assets'];
          region['num_yellow_assets'] += found['num_yellow_assets'];
          region['num_green_assets'] += found['num_green_assets'];
        }
        return facility;
      });
      const health = this.getHealthPercent(region);
      region['normal'] = health && !Number.isNaN(health) ? health : 0;
      region['color'] = this.getHealthColor(region);
      region['class'] = `health-${region['color']}`;
      region['path'] = `./${region['name']}`;
      return region;
    });
  }

  getCountryHealthInfo(regionList) {
    const country = {
      num_red_assets: 0,
      num_yellow_assets: 0,
      num_green_assets: 0,
    };
    regionList.map((region) => {
      country['num_red_assets'] += +region['num_red_assets'];
      country['num_yellow_assets'] += +region['num_yellow_assets'];
      country['num_green_assets'] += +region['num_green_assets'];
      return region;
    });
    country['normal'] = this.getHealthPercent(country);
    country['color'] = this.getHealthColor(country);
    country['class'] = `health-${country['color']}`;

    return country;
  }

  getHealthColor(region) {
    if (region['num_red_assets'] > 0) {
      return PRIORITY_COLORS['3'];
    } else if (region['num_yellow_assets'] > 0) {
      return PRIORITY_COLORS['2'];
    }
    return PRIORITY_COLORS['1'];
  }

  getHealthPercent(region) {
    console.log('region')
    console.log(region)
    const assetCount = region['num_red_assets'] + region['num_yellow_assets'] + region['num_green_assets'];         
    return Math.floor((region['num_green_assets'] / assetCount) * 100);
  }

  public getAssetsWithErrors(overview, countryData) {
    const paths = {};

    countryData.forEach((country) => {
      country['regions'].forEach((region) => {
        region['facilities'].forEach((facility) => {
          facility['facility_areas'].forEach((facilityArea) => {
            facilityArea['assets']
              .filter((asset) => asset['color'] > 1)
              .forEach((asset) => {
                paths[
                  asset['name']
                ] = `${region['name']}/${facility['site']}/${facilityArea['name']}`;
              });
          });
        });
      });
    });

    const result = overview['red_assets'].concat(overview['yellow_assets']).map((item) => {
      item['asset'] = item['name'];
      item['severity'] = item['color'];
      item['region'] = paths[item['name']];
      item['icon'] = `./assets/images/compliance/icons/asset-${PRIORITY_COLORS[item['color']]}.svg`;
      return item;
    });

    return result.reduce(function (accumulator, asset) {
      const resultAsset = accumulator.find(
        (item) => item['facilityName'] === asset['facility_name']
      );
      if (!resultAsset) {
        accumulator.push({
          facilityName: asset['facility_name'],
          assets: [asset],
        });
      } else {
        resultAsset['assets'].push(asset);
      }
      return accumulator;
    }, []);
  }

  public getLayoutMap(theme, country) {
    if (theme && theme === 'theme-dark') {
      let countryPath = country.replace(/\s/gi, '_');
      if (country.toLowerCase() === 'italy') {
        countryPath = countryPath.toLowerCase();
      }

      return `./assets/images/compliance/map/dark-map-${countryPath}.svg`;
    } else {
      return `./assets/images/compliance/map/light-map-${country.replace(/\s/gi, '_')}.svg`;
    }
  }

  public getFacilities(menuResult, activeCountry) {
    const facilities = [];
    const activeRegions = [];
    menuResult['countries']
      .filter((item) => item['name'].replace(/\s/gi, '_') === activeCountry)
      .map((country) => {
        country['regions'].map((region) => {
          activeRegions.push(region['name']);
          region['facilities'].map((facility) => {
            const facilityData = country['overview']['facilities'].find(item => item['name'] === facility['name'])
            facilityData['name'] = facility['name'];
            facilityData['site'] = facility['site'];
            facilities.push(facilityData) 
          });
        });
      }); 
    return {facilities, activeRegions,};
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString(),
    });
  }
}
