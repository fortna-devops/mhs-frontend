import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../shared/token.service';
import { Subject, forkJoin, Observable, of } from 'rxjs';
import { TopNavService } from '../shared/top-nav/top-nav.service';
import { TranslateService } from '@ngx-translate/core';
import { PRIORITY_COLORS } from '../shared/mapping';
import { LeftMenuService } from '../left-menu/left-menu.service';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import * as moment from 'moment';

export const ERROR_STATUS = 'error';

export const METRICS_LABEL = {
  Vibration: 'rms_velocity_x',
  Temperature: 'temperature',
  'Acceleration X': 'hf_rms_acceleration_x',
  'Acceleration Z': 'hf_rms_acceleration_z',
};

export const METRICS_TITLES = {
  rms_velocity_x: 'Avg. Vibration',
  rms_velocity_z: 'Avg. Vibration',
  temperature: 'Temperature',
  hf_rms_acceleration_x: 'Acceleration X',
  hf_rms_acceleration_z: 'Acceleration Z',
  speed: 'Speed',
  current: 'Current',
  voltage: 'Voltage',
  mechanical_power: 'Mechanical Power',
};

export const METRICS_KEYS = {
  rms_velocity_x: 'Vibration',
  rms_velocity_z: 'Vibration',
  speed: 'generic',
  current: 'generic',
  voltage: 'generic',
  mechanical_power: 'generic',
};

export const METRICS_LISTS = {
  VFD: ['temperature'],
  VFDMotor: ['speed', 'current', 'voltage', 'mechanical_power'],
  VT: ['rms_velocity_x', 'rms_velocity_z', 'temperature'],
};

export const LINE_CHART_COLORS = {
  measured: '#3B7DFF',
  expected: '#A4DE02',
  red: '#FF5252',
  orange: '#FFA817',
};

export const CHART_COMPONENTS_COLORS = {
  measured: '#3B7DFF',
  expected: '#A4DE02',
  red: '#FF5252',
  orange: '#FFA817',
};

export const CHART_COLORS_BY_COMPONENTS = {
  Motor: '#0869AE',
  Bearing: '#DB64FF',
  Gearbox: '#5EFBE1',
};

export const LINE_CHART_CLASS = {
  measured: 'stroke-width-two',
  expected: 'dashed stroke-width-two inferred-chart',
  red: 'dotted stroke-width-one',
  orange: 'dotted stroke-width-one',
};

export const ACTIVE_CHART_UNITS = {
  rms_velocity_x: 'in/sec',
  temperature: 'Fahrenheit',
};

export const METRICS_TEXT = {
  rms_velocity_x: 'Vibration',
  temperature: 'Temperature',
  hf_rms_acceleration_x: 'Acceleration X',
  hf_rms_acceleration_z: 'Acceleration Z',
};

export const ACTIVE_CHART_UNITS_TAG = {
  Vibration: 'in/sec',
  Temperature: 'Fahrenheit',
  'Belt Speed': '',
  'Motor Current': '',
  'Motor Frequency': '',
};

export const DONUT_CHART = {
  title: 'Activity Time',
  loading: false,
  raw_data: [],
  data: [],
  d3Options: {},
  normal: '',
  errorMessage: ''
};

export const ACTIVE_CHART_RULES = {
  X_RMS_V_IPS: 'Vibration',
  TEMP_F: 'Temperature',
  belt_speed: 'Belt Speed',
  motor_current: 'Motor Current',
  motor_freq: 'Motor Frequency',
};

const CHART_COLORS = [
  '#55A8FF',
  '#FF9359',
  '#673AB7',
  '#795548',
  '#9B96FF',
  '#92E9FF',
  '#FF898E',
  '#7F8100',
  '#FF72D8',
  '#5FFFAD',
  '#8CFFE7',
  '#673A62',
  '#FF72A8',
  '#5EABE1',
];

const BIG_COLOR_ARRAY = [
  '#55A8FF',
  '#FF72D8',
  '#FF9359',
  '#673AB7',
  '#795548',
  '#9B96FF',
  '#92E9FF',
  '#FF898E',
  '#7F8100',
  '#5FFFAD',
  '#d2737d',
  '#c0a43c',
  '#651be6',
  '#8a96c6',
  '#dba2e6',
  '#f812b3',
  '#b17fc9',
  '#8d6c2f',
  '#d3277a',
  '#2ca1ae',
  '#9685eb',
  '#20f6ba',
  '#63b598',
  '#ce7d78',
  '#ea9e70',
  '#a48a9e',
  '#c6e1e8',
  '#648177',
  '#0d5ac1',
  '#f205e6',
  '#14a9ad',
  '#4ca2f9',
  '#d298e2',
  '#6119d0',
  '#608fa4',
  '#436a9e',
  '#0ec0ff',
  '#79806e',
  '#9348af',
  '#c5a4fb',
  '#996635',
  '#b11573',
  '#2f3f94',
  '#2f7b99',
  '#da967d',
  '#7e50a8',
  '#e0eeb8',
  '#11dec1',
  '#566ca0',
  '#ffdbe1',
  '#2f1179',
  '#935b6d',
  '#916988',
  '#513d98',
  '#aead3a',
  '#9e6d71',
  '#4b5bdc',
  '#cb5bea',
  '#228916',
  '#539397',
  '#880977',
  '#f697c1',
  '#ba96ce',
  '#679c9d',
  '#5be4f0',
  '#57c4d8',
  '#be608b',
  '#088baf',
  '#f158bf',
  '#e145ba',
  '#ee91e3',
  '#05d371',
  '#5426e0',
  '#802234',
  '#6749e8',
  '#0971f0',
  '#8fb413',
  '#b2b4f0',
  '#c3c89d',
  '#c9a941',
  '#fb21a3',
  '#51aed9',
  '#21538e',
  '#d36647',
  '#0023b8',
  '#986b53',
  '#983f7a',
  '#ea24a3',
  '#79352c',
  '#c79ed2',
  '#d6dd92',
  '#b2be57',
  '#fa06ec',
  '#1bb699',
  '#6b2e5f',
  '#64820f',
  '#21538e',
  '#d36647',
  '#0023b8',
  '#986b53',
  '#983f7a',
  '#ea24a3',
  '#79352c',
  '#521250',
  '#c79ed2',
  '#d6dd92',
  '#fa06ec',
  '#1bb699',
  '#6b2e5f',
  '#996c48',
  '#9ab9b7',
  '#e3a481',
  '#fc458e',
  '#aa226d',
  '#792ed8',
  '#73872a',
  '#cefcb8',
  '#a5b3d9',
  '#7d1d85',
  '#243eeb',
  '#dd93fd',
  '#3f8473',
  '#e7dbce',
  '#421f79',
  '#7a3d93',
  '#635f6d',
  '#93f2d7',
  '#9b5c2a',
  '#15b9ee',
  '#0f5997',
  '#409188',
  '#1350ce',
  '#10e5b1',
  '#fff4d7',
  '#cb2582',
  '#ce00be',
  '#32d5d6',
  '#608572',
  '#c79bc2',
  '#77772a',
  '#6995ba',
  '#b47162',
  '#947002',
  '#e08c56',
  '#28fcfd',
  '#a84a8f',
  '#911e7e',
  '#3f16d9',
  '#0f525f',
  '#ac7c0a',
  '#b4c086',
  '#640fc1',
  '#406df9',
  '#615af0',
  '#4a543f',
  '#79bca0',
  '#a8b8d4',
  '#00efd4',
  '#7260d8',
  '#1deaa7',
  '#823c59',
  '#b46238',
  '#2dfff6',
  '#a82b89',
  '#436a9f',
  '#1a806a',
  '#c188a2',
  '#b308d3',
  '#71b1f4',
  '#e23dd0',
  '#00f7f9',
  '#474893',
  '#1c65cb',
  '#a259a4',
  '#1bede6',
  '#8798a4',
  '#de73c2',
  '#d70a9c',
  '#88e9b8',
  '#c2b0e2',
  '#ae90e2',
  '#1a806b',
  '#07d7f6',
  '#77ecca',
];

export const LocationCode = {
  Motor: 'M',
  Gearbox: 'G',
  Bearing: 'B',
  'Bearing motor near': 'BMN',
  'Bearing motor far': 'BMF',
  'Bearing near': 'BN',
  'Bearing far': 'BF',
  Structure: 'S',
  'Bearing Right Front': 'RF',
  'Bearing Right Rear': 'RR',
  'Bearing Left Front': 'LF',
  'Bearing Left Rear': 'LR',
  Frame: 'FRAME',
  VFD: 'VFD',
  'Pin Divert': 'PIN_DIVERT',
};

@Injectable({
  providedIn: 'root',
})
export class ComplianceService {
  private rightMenuData = new Subject<any>();
  private rightMenuActiveSensor = new Subject<any>();
  private activeAssetList = new Subject<any>();
  private sideBarsEvent = new Subject<any>();
  private expandedChart = new Subject<string>();

  private alarmsResult: Array<any> = [];
  private acknowledgements: Array<any>;
  private alarms: Array<any>;

  constructor(
    private http: HttpClient,
    private token: TokenService,
    private topNavService: TopNavService,
    private translate: TranslateService,
    private leftMenuService: LeftMenuService
  ) {}

  public getMenu() {
    const params = `customer=${this.topNavService.getCustomer()}`;
    const url = `${environment.apiURL}/left-menu?${params}`;
    return this.http.get<any>(url, { headers: this.createHttpHeader() });
  }

  public getfacilityData() {
    const url = `${environment.apiURL}/facility-page?site=${this.topNavService.getSiteId()}`;
    return this.http.get<any>(url, { headers: this.createHttpHeader() });
  }

  public getComponentHealth(params) {
    // tslint:disable-next-line:max-line-length
    const componentHealthUrl = `${environment.apiURL}/asset-health?site=${encodeURIComponent(
      params['site']
    )}&asset=${encodeURIComponent(params['asset'])}&current_time=${params[
      'currentTime'
    ].toISOString()}`;
    return this.http.get<any>(componentHealthUrl, { headers: this.createHttpHeader() });
  }

  public getAlarmCounts(params) {
    // tslint:disable-next-line:max-line-length
    const componentIds = params['component_ids'] ? `&component_ids=${encodeURIComponent(params['component_ids'] || '')}` : '';
    const alarmCountUrl = `${environment.apiURL}/alarm-counts?site=${encodeURIComponent(
      params['site'])}&start=${params['startDate'].toISOString()}&end=${params['endDate'].toISOString()
    }${componentIds}&timezone=${params['timezone']}`;
    return this.http.get<any>(alarmCountUrl, { headers: this.createHttpHeader() });
  }

  public getComponentDetails(params) {
    // tslint:disable-next-line:max-line-length
    const rightMenuUrl = `${environment.apiURL}/component-details?site=${encodeURIComponent(
      params['site']
    )}&component_ids=${encodeURIComponent(params['component_ids'])}`;
    return this.http.get<any>(rightMenuUrl, {
      headers: this.createHttpHeader(),
    });
  }

  public getAssetDetails(params) {
    // tslint:disable-next-line:max-line-length
    const rightMenuUrl = `${environment.apiURL}/asset-details?site=${encodeURIComponent(
      params['site']
    )}&asset_ids=${encodeURIComponent(params['asset_ids'])}`;
    return this.http.get<any>(rightMenuUrl, {
      headers: this.createHttpHeader(),
    });
  }

  public getAssetStatusAndDetails(params) {
    // tslint:disable-next-line:max-line-length
    const assetStatusUrl =
      environment.apiURL +
      '/asset-status-percentage?' +
      'site=' +
      encodeURIComponent(params['site']) +
      '&asset=' +
      encodeURIComponent(params['asset']);
    const assetStatusResponse = this.http.get<any>(assetStatusUrl, {
      headers: this.createHttpHeader(),
    });

    // tslint:disable-next-line:max-line-length
    const detailsUrl = `${environment.apiURL}/details?site=${encodeURIComponent(
      params['site']
    )}&asset=${encodeURIComponent(params['asset'])}`;
    const detailsResponse = this.http.get<any>(detailsUrl, {
      headers: this.createHttpHeader(),
    });

    return forkJoin([assetStatusResponse, detailsResponse]);
  }

  public getAssetStatus(params) {
    // tslint:disable-next-line:max-line-length
    const currentDayUrl =
      environment.apiURL +
      '/asset-status-percentage?' +
      'site=' +
      encodeURIComponent(params['site']) +
      '&asset=' +
      encodeURIComponent(params['asset']);
    return this.http.get<any>(currentDayUrl, { headers: this.createHttpHeader() });
  }

  public getAlarms(params) {
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }
  
  public getAssetAlarms(params) {
    const countUrl = params.count ? `&count=true` : '';
    const limitOffset = params.limit ? `&offset=${params['offset']}&limit=${params['limit']}` : '';
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])
    }&asset_ids=${encodeURIComponent(params['asset_ids'])}${limitOffset}${countUrl}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }

  public getComponentAlarms(params) {
    const countUrl = params.count ? `&count=true` : '';
    const limitOffset = params.limit ? `&offset=${params['offset']}&limit=${params['limit']}` : '';
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])
    }&component_ids=${encodeURIComponent(params['component_ids'])}${limitOffset}${countUrl}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }

  public getComponentAlarmsCount(params) {
    const countUrl = params.count ? `&count=true` : '';
    const metricIds = params['metric_ids'] ? `&metric_ids=${encodeURIComponent(params['metric_ids'] || '')}` : '';
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])
    }&component_ids=${encodeURIComponent(params['component_ids'])}&start=${params['startDate'].toISOString()}&end=${params[
      'endDate'].toISOString()}${metricIds}${countUrl}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }

  public getComponentRangeAlarms(params, parts) {
    const metricIds = params['metric_ids'] ? `&metric_ids=${encodeURIComponent(params['metric_ids'] || '')}` : '';
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])
    }&component_ids=${encodeURIComponent(params['component_ids'])}&start=${params['startDate'].toISOString()}&end=${params[
    'endDate'].toISOString()}${metricIds}`;

    const alarms = [];
    for(let i = 0; i < parts; i++) {
      const limitOffset = params.limit ? `&offset=${+params['offset'] * i}&limit=${params['limit']}` : '';
      alarms.push(this.http.get<any>(urlAlarms + limitOffset, { headers: this.createHttpHeader() }));
    }
    return forkJoin(alarms); 
  }

  public getAlarmsAndAcknowledgements(params) {
    // tslint:disable-next-line:max-line-length
    const urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(
      params['site']
    )}&asset_ids=${encodeURIComponent(params['asset_ids'])}`;
    const alarms = this.http.get<any>(urlAlarms, {
      headers: this.createHttpHeader(),
    });

    const urlAcknowledgements = `${environment.apiURL}/acknowledgements?site=${encodeURIComponent(
      params['site']
    )}&cache=update`;
    const acknowledgements = this.http.get<any>(urlAcknowledgements, {
      headers: this.createHttpHeader(),
    });

    return forkJoin([alarms, acknowledgements]);
  }

  public getAcknowledgements(params) {
    const urlAcknowledgements = `${environment.apiURL}/acknowledgements?site=${encodeURIComponent(
      params['site']
    )}&cache=update`;
    return this.http.get<any>(urlAcknowledgements, { headers: this.createHttpHeader() });
  }

  public setRightMenuList(rightMenu) {
    this.rightMenuData.next(rightMenu);
  }

  public setRightMenuActiveSensor(item) {
    this.rightMenuActiveSensor.next(item);
  }

  public getRightMenuList() {
    return this.rightMenuData.asObservable();
  }

  public setExpandedChart(chart) {
    // debugger
    this.expandedChart.next(chart);
  }

  public getExpandedChart() {
    return this.expandedChart.asObservable();
  }

  public getRightMenuActiveItem() {
    return this.rightMenuActiveSensor.asObservable();
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString(),
    });
  }

  public setActiveAssetList(item) {
    this.activeAssetList.next(item);
  }

  public getActiveAssetList() {
    return this.activeAssetList.asObservable();
  }

  public getAssetsData(assets, leftMenuData) {
    const assetsFacilities = {};

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities'].forEach((facility) => {
        facility['facility_areas'].forEach((facilityArea) => {
          facilityArea['assets'].forEach((asset) => {
            assetsFacilities[asset['name']] = facilityArea['name'];
          });
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((facility) => {
            facility['facility_areas'].forEach((facilityArea) => {
              facilityArea['assets'].forEach((asset) => {
                assetsFacilities[asset['name']] = facilityArea['name'];
              });
            });
          });
        });
      });
    }

    return assets
      .sort((a, b) => b.severity - a.severity)
      .map((asset) => {
        return {
          asset: asset['asset'],
          region: assetsFacilities[asset['asset']],
          icon: `./assets/images/compliance/icons/asset-${PRIORITY_COLORS[asset['severity']]}.svg`,
          color: PRIORITY_COLORS[asset['severity']],
        };
      });
  }

  public getMetrics(detailList, site = '') {
    const metrics = [];
    let hasVFD = false,
      hasVT = false,
      hasVFDMotor = false;

    detailList.map((item) => {
      if (item['component'] === 'VFD') {
        hasVFD = true;
      } else if (site === 'dhl-brescia' && item['component'] === 'M') {
        hasVFDMotor = true;
      } else {
        hasVT = true;
      }
    });

    // tslint:disable-next-line: curly
    if (hasVFD) metrics.push(...METRICS_LISTS['VFD']);
    // tslint:disable-next-line: curly
    if (hasVFDMotor) metrics.push(...METRICS_LISTS['VFDMotor']);
    // tslint:disable-next-line: curly
    if (hasVT) metrics.push(...METRICS_LISTS['VT']);

    return metrics;
  }

  public getFacilityOverview(leftMenuData, site) {
    let overview = {};

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const activeFacilities = leftMenuData['facilities'].filter(
        (facility) => facility['site'] === site
      );

      if (activeFacilities && Array.isArray(activeFacilities)) {
        overview = activeFacilities[0]['overview'];
      }
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          const activeFacilities = region['facilities'].filter(
            (facility) => facility['site'] === site
          );
          if (activeFacilities && Array.isArray(activeFacilities) && activeFacilities.length > 0) {
            overview = activeFacilities[0]['overview'];
          }
        });
      });
    }

    return overview;
  }

  public getFacilityAreas(leftMenuData, site) {
    let facilityAreas = [];

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const activeFacility = leftMenuData['facilities'].find(
        (facility) => facility['site'] === site
      );

      if (activeFacility) {
        facilityAreas = activeFacility['facility_areas'];
      }
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          const activeFacility = region['facilities'].find(
            (facility) => facility['site'] === site
          );
          if (activeFacility) {
            facilityAreas = activeFacility['facility_areas'];
          }
        });
      });
    }

    return facilityAreas;
  }

  public setChangeScreenWidthEvent(item) {
    this.sideBarsEvent.next(item);
  }

  public getChangeScreenWidthEvent() {
    return this.sideBarsEvent.asObservable();
  }

  public acknowledge(params) {
    params['alarm_timestamp'] = moment().format('YYYY-MM-DD HH:mm:ss').concat('.000').toString();
    params['alarm_description'] = 'ALARM_DESCRIPTION';

    const urlAcknowledgements = `${environment.apiURL}/acknowledgements?site=${encodeURIComponent(
      params['site']
    )}`;
    const requestBody = {
      site: params['site'],
      acknowledgement: {
        alarm_description: params['alarm_description'],
        alarm_timestamp: params['alarm_timestamp'],
        asset: params['asset'],
        criteria: params['criteria'],
        downtime_schedule: params['downtime_schedule'] || '',
        component: params['component'].replace('component-', ''),
        key: params['key'],
        sensor: params['sensor'],
        severity: params['severity'].toString(),
        timestamp: params['timestamp'],
        type: params['type'],
        value: params['value'],
        work_order: params['work_order'] || '',
        work_order_lead: params['work_order_lead'] || '',
      },
    };

    return this.http.post<any>(urlAcknowledgements, JSON.stringify(requestBody), {
      headers: this.createHttpHeader(),
    });
  }

  public getActiveComponent(location) {
    let activeComponentText = '';

    // if (location.indexOf('PLC') !== -1) {
    //   location = 'P';
    // }

    switch (location) {
      case 'motor':
        activeComponentText = 'Motor';
        break;
      case 'bearing':
        activeComponentText = 'Bearing';
        break;
      case 'BMN':
        activeComponentText = 'Bearing motor near';
        break;
      case 'BMF':
        activeComponentText = 'Bearing motor far';
        break;
      case 'BN':
        activeComponentText = 'Bearing near';
        break;
      case 'BF':
        activeComponentText = 'Bearing far';
        break;
      case 'BU':
        activeComponentText = 'Bearing upper';
        break;
      case 'gearbox':
        activeComponentText = 'Gearbox';
        break;
      case 'P':
        activeComponentText = 'PLC';
        break;
      case 'RF':
        activeComponentText = 'Bearing Right Front';
        break;
      case 'RR':
        activeComponentText = 'Bearing Right Rear';
        break;
      case 'LF':
        activeComponentText = 'Bearing Left Front';
        break;
      case 'LR':
        activeComponentText = 'Bearing Left Rear';
        break;
      case 'FRAME':
        activeComponentText = 'Frame';
        break;
      case 'PIN_DIVERT':
        activeComponentText = 'Pin Divert';
        break;
      case 'BELT':
        activeComponentText = 'Belt';
        break;
      case 'VFD':
        activeComponentText = 'VFD';
        break;
      default:
        activeComponentText = location;
        break;
    }

    return `${activeComponentText}`;
  }

  public getChartLineColors(index) {
    return CHART_COLORS[index];
  }

  public getFacilityColors(index) {
    return BIG_COLOR_ARRAY[index];
  }

  public getComponentChartSettings(location) {
    let color = '';
    let classed = '';
    location = location.toUpperCase();
    classed = 'stroke-width-two';

    switch (location) {
      case 'MOTOR':
        color = '#0869AE';
        break;
      case 'BEARING':
        color = '#DB64FF';
        break;
      case 'FEED_AIR':
        color = '#FF9359';
        break;
      case 'PHOTO_EYE':
        color = '#673AB7';
        break;
      case 'BN':
        color = '#795548';
        break;
      case 'BF':
        color = '#FF6C27';
        break;
      case 'CHAIN':
        color = '#3B7DFF';
        break;
      case 'GEARBOX':
        color = '#5EFBE1';
        break;
      case 'P':
        color = '#008400';
        break;
      case 'RF':
        color = '#FF898E';
        break;
      case 'LF':
        color = '#92E9FF';
        break;
      case 'LR':
        color = '#D03997';
        break;
      case 'VFD':
        color = '#FF72D8';
        break;
      case 'FRAME':
        color = '#9B96FF';
        break;
      case 'RR':
        color = '#9C27B0';
        break;
      case 'PIN_DIVERT':
        color = '#8CFFE7';
        break;
      case 'BELT':
        color = '#FFF9F9';
        break;
      default:
        color = '#7F8100';
        break;
    }

    return {
      color,
      classed,
    };
  }

  public getAlarmChartSettings(chartType) {
    let name = '';
    let color = '';
    let classed = '';

    switch (chartType) {
      case 'measured':
        name = 'Measured';
        color = '#1897F2';
        classed = 'stroke-width-two';
        break;
      case 'predicted':
        name = 'Expected';
        color = '#9AD5FF';
        classed = 'dashed stroke-width-two';
        break;
      case 'red':
        name = 'Threshold Red';
        color = '#FF5252';
        classed = 'dashed stroke-width-one';
        break;
      case 'orange':
        name = 'Threshold Yellow';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-one';
        break;
      case 'peak':
        name = 'Alarm peak';
        color = '#D7292E';
        classed = 'stroke-width-three';
        break;
      case 'lower':
        name = 'Lower';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-two';
        break;
      case 'upper':
        name = 'Upper';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-two';
        break;
    }

    return {
      name,
      color,
      classed,
    };
  }

  public cacheAlarmsAndRemedies(alarmsResult) {
    const newAlarmsResult = alarmsResult
      .sort((firstIssue, secondIssue) => secondIssue.severity - firstIssue.severity)
      .map((item, index) => {

        return {
          asset: item.asset,
          sensor: item.sensor,
          metric: item.metric,
          component_type: item.component_type,
          severity: item.severity,
          data: {
            class: `alarm-${PRIORITY_COLORS[item.severity]} main-text-color alarm-link`,
            issueMessage: item,
            remedyMessage: item['remedy'],
          },
        };
      });

    newAlarmsResult.forEach((newAlarm) => {
      if (
        !this.alarmsResult.some((cachedAlarm) => {
          return (
            cachedAlarm.asset === newAlarm.asset &&
            cachedAlarm.sensor === newAlarm.sensor &&
            cachedAlarm.severity === newAlarm.severity
          );
        })
      ) {
        this.alarmsResult.push(newAlarm);
      }
    });
  }

  // public getTagName(metric) {
  //   return metric.toLowerCase().indexOf('velocity') !== -1 ? 'vibrations' : 'temperature';
  // }

  public getActiveRegionColor(theme) {
    if (theme && theme === 'theme-dark') {
      return '#976CCC';
    } else {
      return '#A6E4F5';
    }
  }

  public getAlarmsAndRemedies() {
    return this.alarmsResult;
  }

  public cacheAcknowledgements(acknowledgements) {
    this.acknowledgements = acknowledgements || [];
  }

  public getCachedAcknowledgements() {
    return this.acknowledgements;
  }

  public cacheAlarms(alarms) {
    this.alarms = alarms || [];
  }

  public getCachedAlarms() {
    return this.alarms;
  }

  public getChartData(params) {
    // tslint:disable-next-line:max-line-length
    const url = `${environment.apiURL}/data?site=${params.site}&metric_ids=${encodeURIComponent(
      params.metric_ids)}&group_by=${params.group_by || 'kind'}&sample_rate=${params.sample_rate}&start_datetime=${params.startDate.toISOString()}&end_datetime=${params.endDate.toISOString()}`;
    return this.http.get<Array<any>>(url, { headers: this.createHttpHeader() });
  }

  // public getChartData(params) {
  //   // tslint:disable-next-line:max-line-length
  //   const url = `${environment.apiURL}/data?site=${params.site}&sample_rate=${
  //     params.sample_rate
  //   }&metric_ids=${encodeURIComponent(
  //     params.metric_ids
  //   )}&group_by=component_id&start_datetime=${params.timestamp_start.toISOString()}&end_datetime=${params.timestamp_end.toISOString()}`;
  //   return this.http.get<Array<any>>(url, { headers: this.createHttpHeader() });
  // }

  public getDetailChartData(params) {
    // tslint:disable-next-line:max-line-length
    const url = `${environment.apiURL}/data?site=${params.site}&metric_ids=${encodeURIComponent(
      params.metric_ids)}&start_datetime=${params.timestamp_start.toISOString()}&end_datetime=${params.timestamp_end.toISOString()}&sample_rate=${
      params.data_type
    }`;
    return this.http.get<Array<any>>(url, { headers: this.createHttpHeader() });
  }

  public dateCompare(firstItem, secondItem) {
    if (firstItem['x'] === secondItem['x']) {
      return 0;
    }
    if (moment(firstItem['x']).isAfter(secondItem['x'])) {
      return 1;
    } else {
      return -1;
    }
  }

  public getLevelHealthInfo(childrenList) {
    const level = {
      num_red_assets: 0,
      num_yellow_assets: 0,
      num_green_assets: 0,
    };
    childrenList.map((child) => {
      level['num_red_assets'] += +child['num_red_assets'];
      level['num_yellow_assets'] += +child['num_yellow_assets'];
      level['num_green_assets'] += +child['num_green_assets'];
      return child;
    });
    level['normal'] = this.getHealthPercent(level);
    level['color'] = this.getHealthColor(level);
    level['class'] = `health-${level['color']}`;

    return level;
  }

  public getHealthPercent(item) {
    const assetCount = item['num_red_assets'] + item['num_yellow_assets'] + item['num_green_assets'];
    return Math.floor((item['num_green_assets'] / assetCount) * 100);
  }

  public getHealthColor(item) {
    if (item['num_red_assets'] > 0) {
      return PRIORITY_COLORS['3'];
    } else if (item['num_yellow_assets'] > 0) {
      return PRIORITY_COLORS['2'];
    }
    return PRIORITY_COLORS['1'];
  }

  public getLineChartD3Options(params) {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineWithFocusChart',
        height: 350,
        margin: {
          top: 50,
          right: 40,
          bottom: 60,
          left: 60,
        },
        x: (d) => (d ? moment(d.x).toDate() : ''),
        y: (d) => (d ? d.y : ''),
        useInteractiveGuideline: true,
        interactiveUpdateDelay: 100,
        brushExtent: [new Date(params['range']['startTime']), new Date(params['range']['endTime'])],
        duration: 100,
        xAxis: {
          // axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d %H:%M')(moment(d).toDate()),
          rotateLabels: '15',
        },
        yAxis: {
          axisLabel: '',
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10,
        },
        // forceY: [0, params['maxValue'] + (params['maxValue'] * 20 / 100)],
        xScale: d3.time.scale.utc(),
        pointSize: 30,
        tooltip: {
          position: function () {
            const brect = this.chart.getBoundingClientRect();
            return {
              top: brect.top + brect.height / 2,
              left: brect.left + brect.width / 2,
            };
          },
        },
        x2Scale: d3.time.scale(),
        interactiveLayer: {
          tooltip: {
            contentGenerator: (e) => {
              const seriesCount = e.series.length;
              const header = `
                      <strong style="padding-left: 5px;">${d3.time.format('%b %d %H:%M')(
                        moment(e.value).toDate()
                      )}</strong>
                    `;

              let rows = '';
              for (let i = 0; i < seriesCount; i++) {
                const d = e.series[i];
                let label = d.value;
                if (d.data.x !== moment(e.value).format('YYYY-MM-DD HH:mm:ss.000')) {
                  label = null;
                }

                if (!d.key.includes('Alarm peak')) {
                  rows += `<tr>
                  <td class="legend-color-guide">
                     <div style="background-color: ${d.color};"></div>
                   </td>
                   <td class="key">${d.key}</td>
                   <td class="value" style="text-align: center;">${
                     label || label === 0 ? d.value : '\u2013'
                   }</td>
                 </tr>`;
                }
              }
              return header + '<table>' + '<tbody>' + rows + '</tbody>' + '</table>';
            },
          },
        },
        x2Axis: {
          tickValues: function (d) {
            const between = [];
            const secondDate = d[0].values[d[0].values.length - 1].x;
            const firstDate = d[0].values[0].x;

            let day = new Date(new Date(firstDate).setHours(0, 0, 0, 0));
            const lastDay = new Date(new Date(secondDate).setHours(0, 0, 0, 0));
            between.push(day);
            // get all dates between max and min date
            while (day.getTime() !== lastDay.getTime()) {
              const date = day;
              date.setDate(date.getDate() + 1);
              between.push(new Date(+date));
              day = date;
            }
            return between;
          },
          axisLabel: '',
          tickFormat: function (d) {
            return d3.time.format('%b %d')(new Date(d));
          },
          showMaxMin: false,
        },
      },
    };

    // if (params['maxValue'] && params['maxValue'] < 0.05) {
    //   lineChartD3Options.chart.yAxis['ticks'] = 0;
    // } else {
    //   lineChartD3Options.chart.yAxis['ticks'] = 5;
    // }

    return lineChartD3Options;
  }

  public getArrayOfDatesInRange(startD, endD) {
    const dateArray = [];
    let startDate = moment(startD);
    const endDate = moment(endD);

    while (startDate <= endDate) {
      dateArray.push(moment(startDate).format('YYYY-MM-DD'));
      startDate = moment(startDate).add(1, 'days');
    }

    return dateArray;
  }
}
