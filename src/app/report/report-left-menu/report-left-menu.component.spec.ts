import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLeftMenuComponent } from './report-left-menu.component';

describe('ReportLeftMenuComponent', () => {
  let component: ReportLeftMenuComponent;
  let fixture: ComponentFixture<ReportLeftMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportLeftMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLeftMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
