import { Component, OnInit, Input, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { MatExpansionPanel } from '@angular/material';
import { LeftMenuService } from '../left-menu.service';
import { Subscription } from 'rxjs';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.scss'],
})
export class RegionsComponent implements OnInit, OnDestroy {
  @Input() regions;
  @ViewChildren('regionExpansionPanel') regionExpansionPanel: QueryList<MatExpansionPanel>;

  public regionList: Array<any>;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService
  ) {}

  ngOnInit() {
    this.subscription = this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const paths = ev['url'].substr(5).split('/');
        this.regionList = this.regionList.map((region) => {
          region['isActive'] = false;
          return region;
        });

        if (paths.length === 2) {
          this.regionList = this.regionList.map((region) => {
            if (paths[1] === region['name'].replace(/\s/gi, '_')) {
              region['isActive'] = true;
            }
            return region;
          });
        }

        if (paths.length > 2) {
          this.regionList = this.regionList.map((region) => {
            if (paths[1] === region['name'].replace(/\s/gi, '_')) {
              region['panelOpenState'] = true;
              this.expandParentPanel(region['name'].replace(/\s/gi, '_'));
            }
            return region;
          });
        }
      }
    });

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.regionList = this.getRegionList(this.regions, theme);
    });

    this.regionList = this.getRegionList(this.regions, this.topNavService.getTheme());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private expandParentPanel(region) {
    if (this.regionExpansionPanel && this.regionExpansionPanel['_results']) {
      const elements = this.regionExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === region;
      });

      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  // TODO: move to service
  private getRegionList(regions, theme) {
    return regions.map((region) => {
      return {
        name: region['name'],
        id: region['name'].replace(/\s/gi, '_'),
        country: region['country'],
        facilities: region['facilities'].map((facility) => {
          facility['country'] = region['country'];
          facility['regionName'] = region['name'];
          return facility;
        }),
        panelOpenState: false,
        isActive: this.isActiveRegion(region['name']),
        isOpen: this.isOpenedRegion(region['name']),
        openedByClick: false,
        icon: `assets/images/compliance/icons/${theme}-flag-${
          region['color'].toLowerCase()
        }.svg`,
      };
    });
  }

  setOpened(region, isOpened) {
    region.openedByClick = isOpened;
  }

  private isActiveRegion(regionName) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    const pathsLenght = paths.length;

    if (pathsLenght === 3) {
      const currentRegion = paths[2];
      return regionName === currentRegion;
    } else {
      return false;
    }
  }

  private isOpenedRegion(regionName) {
    const paths = document.location.pathname.split('/').filter(Boolean);
    return paths[3] && regionName === paths[2];
  }

  public goToPage(region) {
    this.router.navigate([`./${region['country'].replace(/\s/gi, '_')}/${region['name']}`], {
      relativeTo: this.activatedRoute,
    });
  }

  public expandPanel(region: any, matExpansionPanel: MatExpansionPanel, event: Event) {
    this.leftMenuService.expandPanel(matExpansionPanel, event);
    if (this.leftMenuService.isExpansionIndicator(event.target)) {
      region.panelOpenState = !region.panelOpenState;
      return;
    }
    this.goToPage(region);
  }
}
