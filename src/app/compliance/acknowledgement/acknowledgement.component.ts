import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
// tslint:disable-next-line:max-line-length
import { AcknowledgementsDialogComponent } from './acknowledgements-dialog/acknowledgements-dialog.component';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { AcknowledgeDialogComponent } from './acknowledge-dialog/acknowledge-dialog.component';
import { ComplianceService } from '../compliance.service';
import { GoogleAnalyticsService } from '../../shared/google-analytics.service';
import { ToasterService } from 'angular2-toaster';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-acknowledgement',
  templateUrl: './acknowledgement.component.html',
  styleUrls: ['./acknowledgement.component.scss'],
})
export class AcknowledgementComponent implements OnInit {
  @Input() alarms;
  @Input() acknowledgements;

  private listAcknowledgementDialogRef: any;

  constructor(
    private dialog: MatDialog,
    private complianceService: ComplianceService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private topNavService: TopNavService,
    private toasterService: ToasterService
  ) {}

  ngOnInit() {}

  public showAcknowledgements() {
    const alarmErrorLog = this.buildAlarmErrorLog();
    this.listAcknowledgementDialogRef = this.dialog.open(
      AcknowledgementsDialogComponent,
      {
        data: {
          alarmsLog: alarmErrorLog,
          showAcknowledgement: alarmErrorLog.some((alarm) => alarm.isAcknowledged === false),
          showHistory: alarmErrorLog.some((alarm) => alarm.isAcknowledged === true),
        },
        width: '85%',
        height: '85%',
        panelClass: this.topNavService.getTheme(),
      }
    );

    this.listAcknowledgementDialogRef.componentInstance['params'] = {
      doAcknowledge: this.doAcknowledge.bind(this),
      dialog: this.dialog,
    };
  }

  private buildAlarmErrorLog() {
    return this.alarms
      .map((item) => {
        const alreadyAcknowledged = this.acknowledgements.find((alarm) => {
          return (
            alarm['sensor'] === item['alarmAllData']['sensor'] &&
            alarm['asset'] === item['alarmAllData']['asset'] &&
            alarm['severity'] === item['alarmAllData']['severity'].toString() &&
            alarm['key'] === item['alarmAllData']['key']
          );
        });
        if (alreadyAcknowledged) {
          item['isAcknowledged'] = true;
          item['work_order'] = alreadyAcknowledged['work_order'];
          item['downtime_schedule'] = alreadyAcknowledged['downtime_schedule'];
          item['alarm_timestamp'] = alreadyAcknowledged['alarm_timestamp'];
        } else {
          item['isAcknowledged'] = false;
        }

        item['class'] = `alarm-${PRIORITY_COLORS[item['severity']]}`;

        return item;
      })
      .sort((a, b) => (a.isAcknowledged === b.isAcknowledged ? 0 : a.isAcknowledged ? -1 : 1));
  }

  private doAcknowledge(alarm) {
    const doAcknowledgeDialogRef = this.dialog.open(AcknowledgeDialogComponent, {
      data: alarm,
      width: '40%',
      height: '85%',
      panelClass: this.topNavService.getTheme(),
    });

    doAcknowledgeDialogRef.afterClosed().subscribe((acknowledgement) => {
      if (acknowledgement) {
        this.complianceService.acknowledge(acknowledgement).subscribe((result) => {
          this.googleAnalyticsService.eventEmitter('Acknowledge', 'DoAcknowledge');
          this.complianceService
            .getAlarmsAndAcknowledgements({ site: acknowledgement['site'] })
            .subscribe((results) => {
              this.acknowledgements = results[1] ? results[1] : [];
              this.alarms = results[0]['alarms'].map((item) => {
                item['site'] = acknowledgement['site'];
                return {
                  key_label: item.key_label,
                  description: item.type === 'CAMERA' ? item.key_label : item.description,
                  asset: item.asset,
                  component_type: item.component_type,
                  sensor: item.sensor,
                  tag_name: item.tag_name,
                  tag_limit: item.tag_limit,
                  actual_value: item.actual_value,
                  class: `alarm-${PRIORITY_COLORS[item.severity]} main-text-color`,
                  alarmAllData: item,
                };
              });

              const alarmErrorLog = this.buildAlarmErrorLog();
              this.listAcknowledgementDialogRef.componentInstance.data = {
                alarmsLog: alarmErrorLog,
                showAcknowledgement: alarmErrorLog.some((item) => item.isAcknowledged === false),
                showHistory: alarmErrorLog.some((item) => item.isAcknowledged === true),
              };

              this.toasterService.pop(
                'success',
                '',
                `Acknowledgement was recorded successfully.  `
              );
            });
        });
      }
    });
  }
}
