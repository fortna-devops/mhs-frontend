import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftMenuComponent } from './left-menu.component';
import { CountriesComponent } from './countries/countries.component';
import { RegionsComponent } from './regions/regions.component';
import { FacilitiesComponent } from './facilities/facilities.component';
import { AssetsComponent } from './assets/assets.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { ToasterModule } from 'angular2-toaster';
import { SharedModule } from '../shared/shared.module';
import { MainMaterialModule } from '../main-material.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentItemsComponent } from './component-items/component-items.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    // FormsModule,
    // ReactiveFormsModule,
    ToasterModule,
    // NvD3Module,
    SharedModule,
    MainMaterialModule,
    RouterModule,
    TranslateModule,
    // FleetModule,
    // AlertPageModule,
    // BreadcrumbModule,
    // NouisliderModule
  ],
  declarations: [
    LeftMenuComponent,
    CountriesComponent,
    RegionsComponent,
    FacilitiesComponent,
    AssetsComponent,
    ComponentItemsComponent,
  ],
  exports: [
    LeftMenuComponent,
    CountriesComponent,
    RegionsComponent,
    FacilitiesComponent,
    AssetsComponent,
    ComponentItemsComponent,
  ],
})
export class LeftMenuModule {}
