import { Component, OnInit } from '@angular/core';
import { EventLogService } from './event-log.service';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { ComplianceService } from '../../compliance/compliance.service';
import { PRIORITY_COLORS } from '../mapping';
import { Router, ActivatedRoute } from '@angular/router';
import { TopNavService } from '../top-nav/top-nav.service';
import { Observable, Subscription } from 'rxjs';
import { TokenService } from '../token.service';
import { CacheService } from '../cache.service';

@Component({
  selector: 'app-event-log',
  templateUrl: './event-log.component.html',
  styleUrls: ['./event-log.component.scss'],
})
export class EventLogComponent implements OnInit {
  public allErrorLog: Array<any> = [];
  public optimizationLog: Array<any> = [];
  subscription: Subscription;
  public customer: string;
  public theme: string;
  private leftMenuLevelTypes = { countries: 4, facilities: 2 };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private eventLogService: EventLogService,
    private leftMenuService: LeftMenuService,
    private complianceService: ComplianceService,
    private topNavService: TopNavService,
    private tokenService: TokenService,
    private cacheService: CacheService
  ) {
    this.optimizationLog = JSON.parse(tokenService.getOptimizations()) || [];
    this.customer = this.tokenService.getCustomer();
  }

  ngOnInit() {
    this.theme = this.topNavService.getTheme();
    this.getAlarms();
  }

  private getAlarms() {
    this.eventLogService.getAlarms(this.customer).subscribe((result) => {
      this.setAlarms(result);
      this.cacheService.setAlarms(result['alarms']);
      this.eventLogService.setAlarmsFromAllSites(result);
    });
  }

  private setAlarms(allSitesAlarms) {
    const result = [];
    this.allErrorLog = [];

    let alarmsCount = allSitesAlarms['num_alarms'];

    allSitesAlarms['sites'].forEach((site) => {
      site['assets'].forEach((asset) => {
        const classProp = `alarm-${asset['color'].toLowerCase()} main-text-color alarm-link`;
        const { activeCountry, activeRegion, activeFacilityArea } = this.getRegions(asset['id']);
        const preparedAlarms = {
          assetName: asset['name'], 
          assetId: asset['id'],
          issues: [],
          class: classProp,
          site: site['site'],
          color: asset['color'].toLowerCase(),
          country: activeCountry ? activeCountry.replace(/\s/gi, '_') : activeCountry,
          region: activeRegion ? activeRegion.replace(/\s/gi, '_') : activeRegion,
          facilityArea: activeFacilityArea
            ? activeFacilityArea.replace(/\s/gi, '_')
            : activeFacilityArea,
        };
        asset['components'].forEach((component) => {
          const iconProp = `./assets/images/compliance/icons/${this.theme}-${component['type_name'].toLowerCase()}-${component['color'].toLowerCase()}.svg`;
          component['metrics'].forEach((metric) => {
            const issue = {
              asset: asset['name'],
              componentName: component['name'],
              componentId: component['id'],
              color: component['color'],
              icon: iconProp,
              description: metric['display_name'],
              metricId: metric['id'],
              counter: metric['num_alarms'],
            };
  
            preparedAlarms.issues.push(issue);
            
          });
        });
        result.push(preparedAlarms);
      });
    });

    this.eventLogService.setAlarmsCount(alarmsCount) 
    this.topNavService.announceEventLogChanged();

    this.allErrorLog = result.reduce((acc, val) => acc.concat(val), []);
    this.allErrorLog.sort((firstItem, secondItem) => firstItem['color'].localeCompare(secondItem['color']));
  }

  public onImgError(event, alarm){
    event.target.src = `./assets/images/compliance/icons/${this.theme}-default-component-${alarm.color.toLowerCase()}.svg`;
  }

  private getRegions(assetId) {
    let activeFacilityArea: string;
    let activeRegion: string;
    let activeCountry: string;
    const leftMenuData = this.leftMenuService.getLeftMenuData();

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities'].forEach((facility) => {
        facility['facility_areas'].forEach((facilityArea) => {
          facilityArea['assets'].forEach((asset) => {
            if (asset['id'] === assetId) {
              activeFacilityArea = facilityArea['name'];
            }
          });
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((facility) => {
            facility['facility_areas'].forEach((facilityArea) => {
              facilityArea['assets'].forEach((asset) => {
                if (asset['id'] === assetId) {
                  activeFacilityArea = facilityArea['name'];
                  activeRegion = region['name'];
                  activeCountry = country['name'];
                }
              });
            });
          });
        });
      });
    }

    return {
      activeCountry,
      activeRegion,
      activeFacilityArea,
    };
  }

  public goToAsset(alarm) {
    let url = '';

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      url = `./MHS/${alarm['site']}/${alarm['facilityArea']}/asset-${alarm['assetId']}`.replace(/\s/gi, '_');
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      url = `./MHS/${alarm['country']}/${alarm['region']}/${alarm['site']}/${alarm['facilityArea']}/asset-${alarm['assetId']}`.replace(/\s/gi, '_')
    }

    this.router.navigate([url], { relativeTo: this.activatedRoute });
  }

  public goToComponentOverview(asset, issue) {
    let url = '';

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      url = `./MHS/${asset['site']}/${asset['facilityArea']}/asset-${asset['assetId']}/component-${issue['componentId']}`.replace(/\s/gi, '_')
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      url = `./MHS/${asset['country']}/${asset['region']}/${asset['site']}/${asset['facilityArea']}/asset-${asset['assetId']}/component-${issue['componentId']}`.replace(/\s/gi, '_')
    }

    this.router.navigate([url], { relativeTo: this.activatedRoute });
  }

  public goToComponentAnalize(asset, issue) {
    this.clearQueryParams()
    let url = '';

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      url = `./MHS/${asset['site']}/${asset['facilityArea']}/asset-${asset['assetId']}/component-${issue['componentId']}`.replace(/\s/gi, '_')
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      url = `./MHS/${asset['country']}/${asset['region']}/${asset['site']}/${asset['facilityArea']}/asset-${asset['assetId']}/component-${issue['componentId']}`.replace(/\s/gi, '_')
    }

    this.router.navigate([url], {
      relativeTo: this.activatedRoute,
      queryParams: {
        showChartDialog: true,
        metric: issue['metricId'],
      },
    });
  }

  private clearQueryParams() {
    this.router.navigate([], {});
  }
}
