import { Injectable, OnInit } from '@angular/core';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserSession
} from 'amazon-cognito-identity-js';
import { CognitoUtil } from './cognito.service';
import { Observable } from 'rxjs';
import { TokenService } from '../shared/token.service';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { LeftMenuService } from '../left-menu/left-menu.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService implements OnInit {
  currentUserValue: any;
  isLoggedIn: boolean;

  constructor(
    private http: HttpClient,
    public cognitoUtil: CognitoUtil,
    private router: Router,
    public tokenService: TokenService,
    private leftMenuService: LeftMenuService
  ) {
    this.getCustomers();
  }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenService.toString();
    if (this.isLoggedIn) {
      console.log(this.isLoggedIn);
      this.router.navigate(['./MHS']);
    }
  }

  private onLoginSuccess = res => {
    console.log('In authenticateUser onSuccess callback');
    console.log(res);

    this.tokenService.setCookie('refreshToken', res['refreshToken'].token);
    const oldToken = this.tokenService.toString();
    this.tokenService.setCookie('token', res['idToken'].jwtToken);
    const date = new Date(res['idToken'].payload.exp * 1000);

    this.tokenService.setCookie('expires_at', date.toDateString());

    this.tokenService.setCookie('accessToken', res['accessToken'].jwtToken);

    this.getCustomers().subscribe(cust => {
      this.tokenService.setCookie(
        'customers',
        JSON.stringify(cust['customers'])
      );
      this.tokenService.setCookie('customer', cust['customers'][0].id);
      this.tokenService.setCookie('sites', JSON.stringify(cust['sites']));
      this.tokenService.setCookie('site', cust['sites'][0].id);
      localStorage.setItem('isLoggedIn', 'true');
      console.log('logged in');
      // alert('getLeftMenu')
      // this.leftMenuService.getLeftMenu();
      // this.router.navigate(['/MHS']);
    });
  };

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.tokenService.toString()
    });
  }

  private getCustomers() {
    const url = `${environment.apiURL}/customers`;
    return this.http.get(url, { headers: this.createHttpHeader() });
  }

  authenticate(
    username: string,
    password: string
  ): Observable<{ type: string; result: any }> {
    console.log('UserLoginService: starting the authentication');

    const authData = {
      Username: username,
      Password: password
    };
    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };
    const cognitoUser = new CognitoUser(userData);
    // this.tokenService.setCookie('token', '65');
    // this.onLoginSuccess({refreshToken: {token: ''}, idToken: {jwtToken: '2'}, accessToken: {jwtToken: ''}});
    return new Observable<any>(sub => {
      cognitoUser.authenticateUser(authDetails, {
        onSuccess: (result: any) => {
          sub.next(this.onLoginSuccess(result));
          sub.complete();
        },
        onFailure: (error: any) => sub.error(error),
        newPasswordRequired: (userAttributes, requiredAttributes) => {
          sub.next({
            type: 'newPasswordRequired',
            result: [userAttributes, requiredAttributes]
          });
          sub.complete();
        }
      });
    });
  }
}
