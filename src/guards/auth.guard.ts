import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { UserLoginService } from '../app/login/user-login-service.service';
import {TokenService} from '../app/shared/token.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: UserLoginService,
    private tokenService: TokenService
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log('canActivate');
    const isLoggedIn = !!this.tokenService.toString();

      if (isLoggedIn) {
      // authorised so return true
      console.log('canActivate ' + true);
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    console.log('canActivate ' + false);

    return false;
  }
}
