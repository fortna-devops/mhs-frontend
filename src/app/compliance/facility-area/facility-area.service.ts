import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FacilityAreaService {
  private leftMenuData: any;

  constructor() { }

  public getConveyours() {
    return this.leftMenuData;
  }

  public setConveyours(data) {
    this.leftMenuData = data;
  }

  public setLeftMenuData(data) {
    this.leftMenuData = data;
  }

  public getOverviewData(leftMenuData, params) {
    let overviewData = {};
    let facilityAreaData = {};

    leftMenuData['countries']
      .filter((item) => item['name'].replace(/\s/gi, '_') === params['country'])
      .forEach((country) => {
        country['regions'].filter(
          (item) => item['name'].replace(/\s/gi, '_') === params['region']
        ).forEach((region) => {
          region['facilities'].filter(
            (item) => item['name'].replace(/\s/gi, '_') === params['facility']
          ).forEach((facility) => {
            facilityAreaData = facility['facility_areas'].find(
              (item) => item['name'].replace(/\s/gi, '_') === params['facilityArea']
            );
            if (facilityAreaData) {
              overviewData = facilityAreaData['overview'];
            }
          });
        });
      });

    return {
      overviewData,
      facilityAreaData,
    };
  }

}
