function initViz() {
  var data = null;
  var xhr = new XMLHttpRequest();
  xhr.open(
    'GET',
    'https://kzhr78e7p8.execute-api.us-east-1.amazonaws.com/mhspredict-backend-rdb-demo/demo-looker/?first_name=Tosin&laste_name=Sonuyi&site=dhl-milano&dashboard_name=3&tableau=true&username=mhspredictiot'
  );
  xhr.send(data);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log(xhr.responseText);
      var response = JSON.parse(xhr.response);
      var trustedTicket = response.trusted_ticket;

      var containerDiv = document.getElementById('vizContainer'),
        url = `http://35.174.120.241/trusted/${trustedTicket}/views/tosin-test-extracts-amazonsdf4/AmazonSDF4-Asset-Temperature`,
        options = {
          hideTabs: true,
          onFirstInteractive: function () {
            console.log('Run this code when the viz has finished loading.');
          },
        };

      new tableau.Viz(containerDiv, url, options);
    }
  };
}
