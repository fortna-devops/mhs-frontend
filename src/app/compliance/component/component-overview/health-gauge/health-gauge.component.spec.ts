import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthGaugeComponent } from './health-gauge.component';

describe('HealthGaugeComponent', () => {
  let component: HealthGaugeComponent;
  let fixture: ComponentFixture<HealthGaugeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthGaugeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthGaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
