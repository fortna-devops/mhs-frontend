import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TokenService } from '../token.service';
import { MatDialogRef } from '@angular/material';
import { TopNavService } from '../top-nav/top-nav.service';
import { TimezoneService } from '../timezone/timezone.service';

const LANGUAGES = {
  en: 'English',
  ge: 'Deutsch',
  it: 'Italiano',
  sp: 'Español',
  du: 'Nederlands',
};

const TIMEZONES = {
  utc: 'UTC with no time zone',
  site: 'Site time zone',
  local: 'User time zone',
};

@Component({
  selector: 'app-settings-dialog',
  templateUrl: './settings-dialog.component.html',
  styleUrls: ['./settings-dialog.component.scss'],
})
export class SettingsDialogComponent implements OnInit {
  public currentLanguage: string;
  public currentTimezone: string;
  public timezones = TIMEZONES;
  public timezoneKeys = Object.keys(TIMEZONES);
  public languages = LANGUAGES;
  public languageKeys = Object.keys(LANGUAGES);
  public theme = 'theme-dark';

  constructor(
    private translate: TranslateService,
    private tokenService: TokenService,
    public topNavService: TopNavService,
    public timezoneService: TimezoneService,
    public dialogRef: MatDialogRef<SettingsDialogComponent>
  ) {}

  ngOnInit() {
    this.getLanguage();
    this.getTimezone();

    if (this.topNavService.getTheme()) {
      this.theme = this.topNavService.getTheme();
    }
  }

  switchLanguage(language: string) {
    this.translate.use(language);
    this.tokenService.setLanguage(language);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  changeTheme(theme) {
    console.log(theme);
    this.topNavService.setTheme(theme);
    this.theme = theme;
    console.log(theme);
  }

  getLanguage() {
    const lang = this.tokenService.getLanguage();
    if (lang && lang.length > 0) {
      this.translate.use(lang);
      this.currentLanguage = lang;
    } else {
      this.currentLanguage = 'en';
    }
  }

  switchTimezone(timezone: string) {
    this.tokenService.setTimezone(timezone);
    this.timezoneService.setTimezoneOffset(timezone); 
    this.timezoneService.updateTimezone(timezone).subscribe((res) => {
      window.location.reload();
    })
  }

  getTimezone() {
    const tz = this.tokenService.getTimezone();
    if (tz && tz.length > 0) {
      this.currentTimezone = tz;
    } else {
      this.currentTimezone = 'utc';
    }
    this.timezoneService.setTimezoneOffset(this.currentTimezone);
  }
}
