import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-acknowledgements-dialog',
  templateUrl: './acknowledgements-dialog.component.html',
  styleUrls: ['./acknowledgements-dialog.component.scss']
})
export class AcknowledgementsDialogComponent implements OnInit {
  public params: any;
  // public showAcknowledgement: boolean;
  // public showHistory: boolean;

  constructor(
    public dialogRef: MatDialogRef<AcknowledgementsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    // this.showAcknowledgement = this.data.some(alarm => alarm.isAcknowledged === false);
    // this.showHistory = this.data.some(alarm => alarm.isAcknowledged === true);
  }

  public acknowledge(data) {
    this.data.acknowledgement = data;
    this.params.doAcknowledge(data);
  }

  public back() {
    this.dialogRef.close();
  }
}
