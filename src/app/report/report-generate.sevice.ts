import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TokenService } from '../shared/token.service';
import { jsPDF } from 'jspdf';
import { LOGOBASE64 } from './logo';

@Injectable({
  providedIn: 'root',
})
export class ReportGenerateService {
  private logoBase64 = LOGOBASE64;
  constructor() {}

  public getDocumentDefinition(rows, doc, date) {
    // <><>><><>><>><><><><><>>><><<><><><><>
    // SETTINGS
    // <><>><><>><>><><><><><>>><><<><><><><>

    const fontSizes = {
      TitleFontSize: 15,
      SubTitleFontSize: 13,
      NormalFontSize: 12,
      SmallFontSize: 10,
    };

    doc.addFont('../../assets/fonts/URW/FranklinGothicURWLig.ttf', 'FranklinGothic', 'normal');
    doc.addFont('../../assets/fonts/URW/FranklinGothicURWMed.ttf', 'FranklinGothic', 'bold');

    const lineSpacing = 4 * 0.75;

    let startY = 10; // bit more then 45mm

    const pageHeight = doc.internal.pageSize.height;
    const pageWidth = doc.internal.pageSize.width;
    const pageCenterX = pageWidth / 2;

    // <><>><><>><>><><><><><>>><><<><><><><>
    // Logo

    const imgProps = doc.getImageProperties(this.logoBase64);
    const imageWidth = doc.internal.pageSize.getWidth() - 150;
    const imageHeight = (imgProps.height * imageWidth) / imgProps.width;
    startY = this.newPage(doc, startY, imageHeight);
    doc.addImage(this.logoBase64, 'PNG', 10, startY, imageWidth, imageHeight);

    doc.setFont('FranklinGothic', 'normal');
    doc.setFontSize(12 * 0.75);
    doc.setTextColor(120, 120, 120);
    startY = startY + imageHeight / 2;
    doc.text(date, doc.internal.pageSize.getWidth() - 10, startY, 'right');
    startY = startY + imageHeight;

    startY = this.addReportInfo(doc, rows, startY, lineSpacing);

    // <><>><><>><>><><><><><>>><><<><><><><>
    // REPEATED PAGE COMPONENTS
    // <><>><><>><>><><><><><>>><><<><><><><>

    const pageNr = doc.internal.getNumberOfPages();

    // <><>><><>><>><><><><><>>><><<><><><><>
    // Fold Marks

    const foldX = 12;
    const foldMarksY = [288, 411, 585];
    let n = 0;

    while (n < pageNr) {
      n++;

      doc.setPage(n);

      doc.setDrawColor(215, 40, 47);
      doc.setLineWidth(0.5);

      foldMarksY.map((valueY) => {
        doc.line(foldX, valueY, foldX + 23, valueY);
      });
    }

    // <><>><><>><>><><><><><>>><><<><><><><>
    // Page Numbers

    if (pageNr > 1) {
      n = 0;
      doc.setFontSize(9);

      while (n < pageNr) {
        n++;

        doc.setPage(n);

        doc.text(n + ' / ' + pageNr, pageCenterX, pageHeight - 5, 'center');
      }
    }

    return doc;
  }

  addReportInfo(doc, info, startY, lineSpacing) {
    const startX = 10;
    const imageMargin = 10;
    const spaceBetweenWords = 8;
    const lineHeight = 5;
    let section = 1;
    let subSection = 1;

    const endX = doc.internal.pageSize.width - startX;
    doc.setLineWidth(0.5);
    doc.setDrawColor(120, 120, 120);
    doc.line(startX, startY + lineSpacing / 2, endX, startY + lineSpacing / 2);
    startY = startY + 20;

    info.map((obj) => {
      if (obj.section) {
        section = obj.section;
        subSection = 1;
      }
      if (obj.text) {
        doc.setFont('FranklinGothic', obj.fontType);
        doc.setFontSize(obj.fontSize);
        doc.setLineHeightFactor(obj.lineHeight || 1.15);
        doc.setTextColor(0, 0, 0);

        const pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
        const splitText = doc.splitTextToSize(obj.text, 180);
        const text = obj.subtitle ? `${section}.${subSection} ${splitText}` : splitText;
        const textHeight = doc.getTextDimensions(splitText).h;
        const textX = obj.align === 'center' ? pageWidth / 2 : !obj.section ? startX + 5 : startX;

        startY = this.newPage(doc, startY, textHeight);
        doc.text(text, textX, startY, obj.align);
        const textWidth = doc.getTextWidth(text);
        if (obj.subtitle) {
          doc.setFillColor(obj.color); 
          doc.circle(3 + textX + textWidth, startY - 1.5, 2, 'F');
        }
        startY = startY + textHeight + (obj.marginBottom || lineSpacing);
        // tslint:disable-next-line: curly
        if (obj.subtitle) subSection++;
      }

      if (obj.html) {
        doc.setFont('FranklinGothic', obj.fontType);
        doc.setFontSize(obj.fontSize);
        const pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
        const textHeight = doc.getTextDimensions(obj.html).h;
        const textX = obj.align === 'center' ? pageWidth / 2 : startX;

        startY = this.newPage(doc, startY, textHeight);
        doc.html(obj.html);
        startY = startY + textHeight + (obj.marginBottom || lineSpacing);
      }

      if (obj.image) {
        const imgProps = doc.getImageProperties(obj.image);
        const imageWidth = doc.internal.pageSize.getWidth() - 20;
        const imageHeight = (imgProps.height * imageWidth) / imgProps.width;
        startY = this.newPage(doc, startY, imageHeight);
        doc.addImage(obj.image, 'PNG', startX, startY, imageWidth, imageHeight);
        startY = startY + imageHeight + imageMargin;
      }
    });
    return startY;
  }

  newPage(doc, startY, neededHeight) {
    const pageHeight = doc.internal.pageSize.height;
    const endY = pageHeight - 10; // minus footerHeight
    const newPageY = 10;

    if (endY - startY - neededHeight < 0) {
      doc.addPage();
      return newPageY;
    }
    return startY;
  }
}
