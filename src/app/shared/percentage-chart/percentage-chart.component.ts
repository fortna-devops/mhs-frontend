import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-percentage-chart',
  templateUrl: './percentage-chart.component.html',
  styleUrls: ['./percentage-chart.component.scss']
})
export class PercentageChartComponent implements OnInit {
  @Input() chartData;
  @Input() bigChart = false;
  @Input() runtimeChart = false;
  @Input() calculated = true;

  constructor() {}

  ngOnInit() {}

  isTooltipNeeded() {
    const percent = this.bigChart ? 10 : 15;
    const modPercent = this.bigChart ? 10 : 21;
    const criticalValue =
      this.chartData.red_components <= percent && this.chartData.red_components > 0;
    const normalValue =
      this.chartData.green_components <= percent && this.chartData.green_components > 0;

    if (
      (criticalValue || normalValue) &&
      this.chartData.moderate <= modPercent
    ) {
      return true;
    }
    return false;
  }
}
