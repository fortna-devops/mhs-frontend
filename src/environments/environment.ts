// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  region: 'us-east-1',
  // apiURL: 'https://0ufwq9pjfj.execute-api.us-east-1.amazonaws.com/dev',
  apiURL: 'https://haesqzseya.execute-api.us-east-1.amazonaws.com/preprod',
  // apiURL: 'https://i9rcayhs9b.execute-api.us-east-1.amazonaws.com/dev-new',
  // apiURL: 'https://fvflqmj6ab.execute-api.us-east-1.amazonaws.com/prod',

  identityPoolId: '', // 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
  userPoolId: 'us-east-1_TEkwyJ3Ix',
  clientId: '7qgpad371bgjrcao0h272kp03e',
  rekognitionBucket: 'rekognition-pics',
  albumName: 'usercontent',
  bucketRegion: 'us-east-1',
  ddbTableName: 'LoginTrail',
  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: '',
  theme: 'theme-dark',
  version: require('../../package.json').version,
};
