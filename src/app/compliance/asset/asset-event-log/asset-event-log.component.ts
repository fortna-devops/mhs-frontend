import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ComplianceService } from '../../compliance.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PRIORITY_COLORS } from '../../../shared/mapping';
import { Subscription } from 'rxjs';
import { MatDialog, Sort } from '@angular/material';
import * as moment from 'moment';
import { AssetService } from '../asset.service';
import { ChartDialogComponent } from '../../../shared/chart-dialog/chart-dialog.component';
import { TopNavService } from '../../../shared/top-nav/top-nav.service';

const TIME_DEVIATION = 12;

@Component({
  selector: 'app-asset-event-log',
  templateUrl: './asset-event-log.component.html',
  styleUrls: ['./asset-event-log.component.scss'],
})
export class AssetEventLogComponent implements OnInit, OnChanges {
  @Input() assetAlarms;

  public displayedColumns: string[] = ['severity', 'component_type', 'message', 'dataTimestamp'];
  public dataSource: any;
  public sortedData: any;
  public asset: string;
  private routeSubscription: Subscription;

  private vibrationChartAllData: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  public vibrationChartData: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  private site: string;
  public isLoading = false;

  constructor(
    private complianceService: ComplianceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private topNavService: TopNavService,
    private assetService: AssetService
  ) {}

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.asset = `Asset ${params['asset']}`;
      this.site = params['site'];
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.assetAlarms && Array.isArray(changes.assetAlarms.currentValue)) {
      this.dataSource = this.assetAlarms;
      this.sortedData = this.dataSource.slice();
    }
  }

  public goToAlarm(alarm) {
    if (alarm.type == 'WARNING' || alarm.type == 'ERROR') {
      this.showChartDialog(alarm, true);
      return;
    }
    this.isLoading = true;
    this.getChartData(alarm);
  }

  public showChartDialog(alarm, error = false) {
    const errorData = {
      title: 'Detailed Data',
      error: 'This event was calculated by the VFD. There are presently no data to display.',
    };
    const dialogData = {
      title: 'Detailed Data',
      chartData: this.vibrationChartData['data'],
      options: this.vibrationChartData['options'],
      description: alarm,
    };

    const dialogRef = this.dialog.open(ChartDialogComponent, {
      data: error ? errorData : dialogData,
      width: '85%',
      height: '350px',
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['.'], {
        relativeTo: this.activatedRoute,
        queryParams: {},
      });
    });
  }

  private getChartData(alarm) {
    const chartData: Array<any> = [];
    this.vibrationChartData['data'] = [];
    this.vibrationChartData['options'] = {};
    this.vibrationChartData['loading'] = true;
    this.vibrationChartAllData['data'] = [];

    const timestampStart = moment
      .utc(alarm['started_at'], 'YYYY-MM-DD HH:mm:ss.SSS')
      .subtract(TIME_DEVIATION, 'hours')
      .toISOString();

    const timestampEnd = moment
      .utc(alarm['ended_at'], 'YYYY-MM-DD HH:mm:ss.SSS')
      .add(TIME_DEVIATION, 'hours')
      .toISOString();

    const apiParams = {
      site: this.site,
      sensor: alarm['sensor_id'],
      metric_ids: alarm['metric']['id'], 
      timestamp_start: new Date(timestampStart),
      timestamp_end: new Date(timestampEnd),
      data_type: 'minute',
    };

    this.complianceService.getDetailChartData(apiParams).subscribe(
      (response) => {
        this.isLoading = false;

        const metricData = response['data'];

        const resultData = this.setChartData(metricData, alarm['metric'], alarm);
        const result = {
          id: metricData['id'],
          metric: alarm['metric'],
          data: resultData,
        };
        chartData.push(result);

        chartData.forEach((item) => {
          item['data'].forEach((data) => {
            data['metric'] = item['metric'];
            this.vibrationChartAllData['data'].push(data);
          });
        });

        this.vibrationChartData['data'] = this.vibrationChartAllData['data'];
        this.vibrationChartData['options'] = this.assetService.getAlarmChartOptions({
          xValue1: alarm['started_at'],
          xValue2: alarm['ended_at'],
          chartId: 'chart-dialog-peak',
          criteria: alarm['criteria'],
          color: alarm['color'],
        });

        this.showChartDialog(alarm);
      },
      (error) => {}
    );
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'status': return compare(a.class, b.class, isAsc);
        case 'component_type': return compare(a.component.name, b.component.name, isAsc);
        case 'dataTimestamp': return compare(a.started_at, b.started_at, isAsc);
        default: return 0;
      }
    });
  }

  private setChartData(items, metric, alarm) {
    if (!items || items.length === 0) {
      return [];
    }

    const resultData = items.map((chartData) => {
      const { name, color, classed } = this.getComponentChartSettings(chartData['group_key']);
      return {
        values: chartData['data'].sort(this.complianceService.dateCompare),
        key: name || chartData['group_key'],
        name: name || chartData['group_key'],
        color: color,
        classed: classed,
      };
    });

    const thresholdsData = this.setThresholds(resultData[0], alarm['color'], alarm['criteria']);
    resultData.push(...thresholdsData);

    return resultData;
  }

  private setThresholds(itemList, alarmColor, criteria) {
    const result = [];

    const thresholdValues = [];
    itemList['values'].forEach((value) => {
      thresholdValues.push({ x: value['x'], y: criteria });
    });

    const { name, color, classed } = this.getComponentChartSettings(
      alarmColor.toLowerCase()
    );

    result.push({
      values: thresholdValues,
      key: name,
      name: name,
      color: color,
      classed: classed,
    });

    return result;
  }

  private getComponentChartSettings(chartType) {
    let name = '';
    let color = '';
    let classed = '';

    switch (chartType) {
      case 'raw':
        name = 'Measured';
        color = '#1897F2';
        classed = 'stroke-width-two';
        break;
      case 'calculated':
        name = 'Expected';
        color = '#9AD5FF';
        classed = 'dashed stroke-width-two';
        break;
      case 'red':
        name = 'Threshold Red';
        color = '#FF5252';
        classed = 'dashed stroke-width-one';
        break;
      case 'yellow':
        name = 'Threshold Yellow';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-one';
        break;
      case 'peak':
        name = 'Alarm peak';
        color = '#D7292E';
        classed = 'stroke-width-three';
        break;
    }

    return {
      name,
      color,
      classed,
    };
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}