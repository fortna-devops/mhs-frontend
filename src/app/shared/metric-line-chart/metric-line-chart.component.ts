import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-metric-line-chart',
  templateUrl: './metric-line-chart.component.html',
  styleUrls: ['./metric-line-chart.component.scss'],
})
export class MetricLineChartComponent implements OnInit {
  @Input() chart;
  @ViewChild('nvd3') nvd3;

  constructor() {}

  ngOnInit() {}
}
