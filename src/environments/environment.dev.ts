export const environment = {
  production: true,
  region: 'us-east-1',
  apiURL: 'https://0ufwq9pjfj.execute-api.us-east-1.amazonaws.com/dev',
  identityPoolId: '', // 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
  userPoolId: 'us-east-1_TEkwyJ3Ix',
  clientId: '7qgpad371bgjrcao0h272kp03e',
  rekognitionBucket: 'rekognition-pics',
  albumName: 'usercontent',
  bucketRegion: 'us-east-1',
  ddbTableName: 'LoginTrail',
  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: '',
  theme: 'theme-dark',
  version: require('../../package.json').version
};