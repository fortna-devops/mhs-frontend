import { Component, OnInit, Input } from '@angular/core';
import { FavoritesService } from '../favorites.service';
import { ToasterService } from 'angular2-toaster';
import { ActivatedRoute, Router } from '@angular/router';
import { TokenService } from '../../../shared/token.service';

@Component({
  selector: 'app-fav-button',
  templateUrl: './fav-button.component.html',
  styleUrls: ['./fav-button.component.scss'],
})
export class FavButtonComponent implements OnInit {
  @Input() status = false;
  @Input() url = '';
  @Input() title = '';
  @Input() selectedIndex = 0;
  private favList: any = [];
  private params: any;

  constructor(
    public router: Router,
    public tokenService: TokenService,
    private activatedRoute: ActivatedRoute,
    public favoritesService: FavoritesService,
    private toasterService: ToasterService
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.params = params;
      if (params['tab']) {
        this.selectedIndex = params['tab'];
      }
    });
  }

  toggleFavorite() {
    this.status = this.status ? false : true;
    this.getFavorites();
    if (this.status) {
      if (this.favList.length < 5) {
        this.toasterService.pop('success', '', `Waiting for server response...`);
        this.favoritesService
          .addNewFavorite(this.url, this.title, this.selectedIndex)
          .subscribe((res) => {
            this.favoritesService.announceFavorite(true);
            this.toasterService.pop('success', '', `${this.title} page was added to favorites`);
          });
      } else {
        this.favoritesService.announceMaxStack({
          url: this.url,
          title: this.title,
        });
      }
    } else {
      this.toasterService.pop('success', '', `Waiting for server response...`);

      this.favoritesService.deleteFavorite(this.url).subscribe((res) => {
        this.favoritesService.announceFavorite(true);
        this.toasterService.pop('success', '', `${this.title} page was deleted from favorites`);
      });
    }
  }

  getFavorites() {
      const favs = JSON.parse(this.tokenService.getFavorites());
      this.favList = favs ? favs : [];
      this.status = this.favList.find(fav => this.url.includes(fav.url))
  }

  ngOnInit() {
    this.getFavorites()
  }
}
