import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-health-dashboard',
  templateUrl: './health-dashboard.component.html',
  styleUrls: ['./health-dashboard.component.scss'],
})
export class HealthDashboardComponent implements OnInit {
  @Input() items;
  @Input() title = '';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      if (!this.title) {
        if (params['country'].toLowerCase() === 'europe' && !params['region']) {
          if (this.items.length > 1) {
            this.title = 'Health By Country';
          } else {
            this.title = `Country health`;
          }
        } else {
          if (this.items.length > 1) {
            this.title = params['region'] ? `${params['region']} region` : 'Health By Region';
          } else {
            this.title = `Region health`;
          }
        }
      }
    });
  }

  public goToPage(page) {
    this.router.navigate([`./${page}`], { relativeTo: this.activatedRoute });
  }
}
