import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComplianceComponent } from './compliance.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { NvD3Module } from 'ng2-nvd3';
import { SharedModule } from '../shared/shared.module';
import { MainMaterialModule } from '../main-material.module';
import { RouterModule } from '@angular/router';
import { FacilityAreaComponent } from './facility-area/facility-area.component';
import { CountryComponent } from './country/country.component';
import { RegionComponent } from './region/region.component';
import { HealthDashboardComponent } from '../shared/health-dashboard/health-dashboard.component';
import { HealthDetailsComponent } from '../shared/health-details/health-details.component';
import { AssetCardComponent } from './asset-card/asset-card.component';
import { ComplianceService } from './compliance.service';
import { TranslateModule } from '@ngx-translate/core';
import { LeftMenuModule } from '../left-menu/left-menu.module';
// tslint:disable-next-line:max-line-length
import { AcknowledgementsDialogComponent } from './acknowledgement/acknowledgements-dialog/acknowledgements-dialog.component';
import { AcknowledgeDialogComponent } from './acknowledgement/acknowledge-dialog/acknowledge-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AcknowledgementComponent } from './acknowledgement/acknowledgement.component';
import { FacilityComponent } from './facility/facility.component';
import { FacilityLayoutComponent } from './facility/facility-layout/facility-layout.component';
import { FacilityOverviewComponent } from './facility/facility-overview/facility-overview.component';
import { FacilityAnalysisComponent } from './facility/facility-analysis/facility-analysis.component';
import { FacilityOverviewService } from './facility/facility-overview/facility-overview.service';
import { FacilityHealthComponent } from './facility/facility-overview/facility-health/facility-health.component';
import { ComponentItemComponent } from './component/component-item.component';
import { ComponentHeaderComponent } from './component/component-header/component-header.component';
import { AssetComponent } from './asset/asset.component';
import { AssetHeaderComponent } from './asset/asset-header/asset-header.component';
import { AssetRuntimeComponent } from './asset/asset-runtime/asset-runtime.component';
import { FacilityEventsComponent } from './facility/facility-events/facility-events.component';
import { ComponentOverviewComponent } from './component/component-overview/component-overview.component';
import { ComponentAnalyzeComponent } from './component/component-analyze/component-analyze.component';
import { HealthGaugeComponent } from './component/component-overview/health-gauge/health-gauge.component';
import { ComponentEventLogComponent } from './component/component-overview/component-event-log/component-event-log.component';
import { FavButtonComponent } from './favorites/fav-button/fav-button.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { UploadMediaComponent, FilePopupComponent } from './upload-media/upload-media.component';
import { AssetEventLogComponent } from './asset/asset-event-log/asset-event-log.component';
import { FavLimitPopupComponent } from './favorites/fav-limit-popup/fav-limit-popup.component';
import { DragDropDirective } from './upload-media/drag-drop.directive';
import { ResizableModule } from 'angular-resizable-element';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ToasterModule,
    NvD3Module,
    SharedModule,
    MainMaterialModule,
    RouterModule,
    TranslateModule,
    LeftMenuModule,
    NgbModule,
    ResizableModule
  ],
  declarations: [
    AcknowledgementComponent,
    AcknowledgementsDialogComponent,
    AcknowledgeDialogComponent,
    AssetComponent,
    AssetHeaderComponent,
    AssetEventLogComponent,
    AssetRuntimeComponent,
    AssetCardComponent,
    ComplianceComponent,
    CountryComponent,
    ComponentItemComponent,
    ComponentHeaderComponent,
    ComponentOverviewComponent,
    ComponentAnalyzeComponent,
    ComponentEventLogComponent,
    DragDropDirective,
    FacilityComponent,
    FacilityLayoutComponent,
    FacilityOverviewComponent,
    FacilityAnalysisComponent,
    FacilityHealthComponent,
    FacilityAreaComponent,
    FacilityEventsComponent,
    FilePopupComponent,
    FavoritesComponent,
    FavButtonComponent,
    FavLimitPopupComponent,
    HealthDashboardComponent,
    HealthDetailsComponent,
    HealthGaugeComponent,
    RegionComponent,
    UploadMediaComponent,
  ],
  providers: [ComplianceService, FacilityOverviewService],
  entryComponents: [
    AcknowledgementsDialogComponent,
    AcknowledgeDialogComponent,
    FilePopupComponent,
    FavLimitPopupComponent,
  ],
})
export class ComplianceModule {}
