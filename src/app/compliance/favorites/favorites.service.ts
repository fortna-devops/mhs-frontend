import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../shared/token.service';
import { environment } from '../../../environments/environment';
import { Subject } from 'rxjs';

export class FavoritesModel {
  constructor(
    public url: string,
    public title: string,
    public selectedIndex: number,
    public status: boolean
  ) {}
}

// export let favoritesList = [];

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  // public favoritesList: any[] = null;
  private favoritesAnnounceSource = new Subject<Object>();
  private maxStackAnnounceSource = new Subject<Object>();
  private favoritesLoadedSource = new Subject<Object>();
  favoritesAnnounce$ = this.favoritesAnnounceSource.asObservable();
  maxStackAnnounce$ = this.maxStackAnnounceSource.asObservable();
  favoritesLoaded$ = this.favoritesLoadedSource.asObservable();

  constructor(private http: HttpClient, private token: TokenService) {}

  public addNewFavorite(url, title, selectedIndex = 0) {
    const favs = this.token.getFavorites();
    const favoritesList = favs && favs.length > 0 ? JSON.parse(this.token.getFavorites()) : [];
    const favorite = new FavoritesModel(url, title, selectedIndex, true);
    favoritesList.push(favorite);
    const timezone = this.token.getTimezone();
    const json = { fav_list: favoritesList, timezone: timezone ? timezone : 'utc' };
    return this.http.post(`${environment.apiURL}/user-pref`, JSON.stringify(json), {
      headers: this.createHttpHeader(),
    });
  }

  public checkIsFavorite(url) {
    const favs = this.token.getFavorites();
    const favoritesList = favs && favs.length > 0 ? JSON.parse(this.token.getFavorites()) : [];
    const isFav = favoritesList.find((item) => {
      return item.url === url;
    });
    const selectedTab = (isFav && isFav.selectedIndex) || 0;
    if (isFav !== undefined) {
      return { favorite: true, selectedIndex: selectedTab };
    }
    return { favorite: false, selectedIndex: selectedTab };
  }

  public deleteFavorite(url) {
    const favs = this.token.getFavorites();
    const favoritesList = favs && favs.length > 0 ? JSON.parse(this.token.getFavorites()) : [];
    const obj = favoritesList.find((item) => {
      return item.url === url;
    });
    const index = favoritesList.indexOf(obj);
    favoritesList.splice(index, 1);
    const timezone = this.token.getTimezone();
    const json = { fav_list: favoritesList, timezone: timezone ? timezone : 'utc' };
    return this.http.post(`${environment.apiURL}/user-pref`, JSON.stringify(json), {
      headers: this.createHttpHeader(),
    });
  }

  deleteFavoritesCookies() {
    this.token.deleteCookie('favorites');
  }

  checkFavoritesCookies() {
    return this.token.getFavorites();
  }

  announceFavorite(status) {
    this.favoritesAnnounceSource.next(status);
  }

  announceMaxStack(obj) {
    this.maxStackAnnounceSource.next(obj);
  }

  loadFavorite(status) {
    this.favoritesLoadedSource.next(status);
  }

  public getFavoriteList() {
    const url = `${environment.apiURL}/user-pref`;
    return this.http.get(url, {
      headers: this.createHttpHeader(),
    });
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString(),
    });
  }
}
