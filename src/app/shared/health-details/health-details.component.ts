import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-health-details',
  templateUrl: './health-details.component.html',
  styleUrls: ['./health-details.component.scss']
})
export class HealthDetailsComponent implements OnInit {
  @Input() healthInfo: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('red-circle', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/red-circle.svg'));
    iconRegistry.addSvgIcon('yellow-circle', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/yellow-circle.svg'));
    iconRegistry.addSvgIcon('green-circle', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/green-circle.svg'));
  }

  ngOnInit() {
    console.log(this.healthInfo)
  }

  public goToPage(info) {
    if(info['path']) {
      this.router.navigate([`${info['path']}`], { relativeTo: this.activatedRoute });
    }
  }
}
