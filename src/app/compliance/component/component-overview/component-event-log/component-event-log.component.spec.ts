import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentEventLogComponent } from './component-event-log.component';

describe('ComponentEventLogComponent', () => {
  let component: ComponentEventLogComponent;
  let fixture: ComponentFixture<ComponentEventLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ComponentEventLogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentEventLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
