import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TokenService } from '../token.service';

@Injectable()
export class TopNavService {
  private title = new Subject<any>();
  public visible = new Subject<any>();
  public themeChanged = new Subject<any>();
  public eventLog = new Subject<any>();
  public themeAsPrimitive: string;
  private siteId: string;

  private optimizationListChangedAnnounceSource = new Subject<any>();
  private eventLogChangedAnnounceSource = new Subject<any>();
  optimizationListChangedAnnounce$ = this.optimizationListChangedAnnounceSource.asObservable();
  eventLogChangedAnnounce$ = this.eventLogChangedAnnounceSource.asObservable();

  constructor(private tokenService: TokenService) {}

  public announceOptimizationListChanged() {
    this.optimizationListChangedAnnounceSource.next(true);
  }
  public announceEventLogChanged() {
    this.eventLogChangedAnnounceSource.next(true);
  }

  public hide(): void {
    this.visible.next(false);
  }

  public show(): void {
    this.visible.next(true);
  }

  public getVisible(): Observable<any> {
    return this.visible.asObservable();
  }

  public setTitle(title: string): void {
    this.title.next(title);
  }

  public getTitle(): Observable<any> {
    return this.title.asObservable();
  }

  public setCustomerId(customerId): void {
    this.tokenService.setCustomerId(customerId);
  }

  public deleteCookie(cookie): void {
    this.tokenService.deleteCookie(cookie);
  }

  public getSiteId(): string {
    return this.siteId; // this.tokenService.getSiteId();
  }

  public setSiteId(siteId): void {
    // this.tokenService.setSiteId(siteId);
    this.siteId = siteId;
  }

  public getLeftMenuLevels() {
    // TODO: customer map - move to config
    const customerLevelsMap = {
      dhl: 4,
      fedex: 2,
      mhs: 4,
    };

    return customerLevelsMap[this.getCustomer().toLowerCase()];
  }

  public getCustomer(): string {
    return this.tokenService.getCustomer();
  }

  public setCustomer(customer): void {
    this.tokenService.setCustomer(customer);
  }

  public getUser(): string {
    return this.tokenService.getUser();
  }

  public getSiteListId(): Array<string> {
    let siteListId: Array<string> = [];
    const cookiesSiteList = this.tokenService.getSiteList();

    if (cookiesSiteList) {
      siteListId = JSON.parse(cookiesSiteList).map((item) => item.site_id);
    }

    return siteListId;
  }

  public getSiteGroup(): string {
    let siteGroup: string;
    const siteCookies = this.getAllSites();
    const siteId = this.tokenService.getSiteId();

    siteGroup = siteCookies
      .filter((item) => item['site_id'] === siteId)
      .map((site) => site['group'])
      .find((site) => site);

    return siteGroup;
  }

  public getSiteLocation(): string {
    const siteCookies = this.getAllSites();
    const siteId = this.tokenService.getSiteId();

    return siteCookies
      .filter((item) => item['site_id'] === siteId)
      .map((site) => site['location'])
      .find((site) => site);
  }

  public getActiveSiteName(): string {
    let siteName = '';
    let siteListId: Array<string> = [];
    const cookiesSiteList = this.tokenService.getSiteList();
    const siteId = this.tokenService.getSiteId();

    if (cookiesSiteList && siteId) {
      siteListId = JSON.parse(cookiesSiteList);
      siteName = siteListId
        .filter((item) => item['site_id'] === siteId)
        .map((site) => site['name'])
        .find((site) => site);
    }

    return siteName;
  }

  public getAllSites(): Array<string> {
    let siteListId: Array<string> = [];
    const cookiesSiteList = this.tokenService.getSiteList();
    if (cookiesSiteList) {
      siteListId = JSON.parse(cookiesSiteList);
    }
    return siteListId;
  }

  public getCustomerList(): Array<string> {
    let customerList: Array<string> = [];
    const customerListJson = this.tokenService.getCustomerList();
    if (customerListJson) {
      customerList = JSON.parse(customerListJson);
    }
    return customerList;
  }

  public getActiveCustomerName(): string {
    let customerName = '';
    const customerList = this.getCustomerList();
    const customerId = this.tokenService.getCustomer();

    if (customerList && customerId) {
      customerName = customerList
        .filter((item) => item['id'] === customerId)
        .map((customer) => customer['name'])
        .find((site) => site);
    }

    return customerName;
  }

  // TODO: fix names
  public getThemeAsObservable(): Observable<any> {
    return this.themeChanged.asObservable();
  }

  public getTheme() {
    return this.themeAsPrimitive;
    // return localStorage.getItem('theme') || this.themeAsPrimitive;
  }

  public setTheme(theme: string): void {
    this.themeChanged.next(theme);
    this.themeAsPrimitive = theme;
    // localStorage.setItem('theme', theme);
  }

  public getInit() {
    return this.tokenService.getInit();
  }

  public setInit() {
    this.tokenService.setInit();
  }

  public setEventLog() {
    this.eventLog.next();
  }

  public getAlarmsCount() {
    return this.tokenService.getAlarmsCount();
  }

  public getEventLog(): Observable<any> {
    return this.eventLog.asObservable();
  }
}
