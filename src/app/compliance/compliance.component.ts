import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { LeftMenuService } from '../left-menu/left-menu.service';
import { ComplianceService } from './compliance.service';
import { fadeOutAnimation } from './fade.animation';
import { TopNavService } from '../shared/top-nav/top-nav.service';
import { ResizeEvent } from 'angular-resizable-element';
import { TokenService } from '../shared/token.service';
import { TimezoneService } from '../shared/timezone/timezone.service';

@Component({
  selector: 'app-compliance',
  templateUrl: './compliance.component.html',
  styleUrls: ['./compliance.component.scss'],
  animations: [fadeOutAnimation]
})
export class ComplianceComponent implements OnInit, OnDestroy{
  // @ViewChild('rightMenuSidenav') rightMenuSidenav;

  public events: string[] = [];
  public opened = true;
  public opened2 = true;
  public activeNode: any;
  public rightMenuTitle = 'Zone Compliance';
  public showAssetsMenu = false;
  public style: object = {};
  public contentStyle: object = {};
  public activatedRouteObserver: any;
  public site: any;

  constructor(
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tokenService: TokenService,
    private timezoneService: TimezoneService,
    private leftMenuService: LeftMenuService,
    private complianceService: ComplianceService, 
    private topNavService: TopNavService
  ) {
    iconRegistry.addSvgIcon('open_icon', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/list-view-charts.svg'));
    iconRegistry.addSvgIcon('plus_icon', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/plus.svg'));
    iconRegistry.addSvgIcon('menu_icon', sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/menu-btn.svg'));

    this.activatedRouteObserver =  this.activatedRoute.params.subscribe((params) => {
      if(this.tokenService.getTimezone() === 'site' && this.site !== params['site']) {
        this.site = params['site'];
        this.switchTimezone('site');
      }
    });
  }

  ngOnInit() {
    // this.topNavService.getEventLog()
    //   .subscribe(() => {
    //     this.rightMenuSidenav.toggle();
    //   });
  }

  ngOnDestroy() {
    this.activatedRouteObserver.unsubscribe();
  }

  switchTimezone(timezone: string) {
    this.timezoneService.setTimezoneOffset(timezone); 
    window.location.reload();
  }

  public goToPage(page) {
    const mainPath = (document.location.pathname).split('/MHS/')[1];
    this.router.navigate([`./${mainPath}/${page}`], { relativeTo: this.activatedRoute });
  }

  public setChangeScreenWidthEvents(event) {
    this.complianceService.setChangeScreenWidthEvent(event);
  }

  onResizeEnd(event: ResizeEvent): void {
    if(event.rectangle.width <= 352 && event.rectangle.width >= 150) {
      this.style = {
        width: `${event.rectangle.width}px`,
      };
      this.contentStyle = {
        'margin-left': `${event.rectangle.width}px`,
      };
    } else {
      this.style = {
        width: `${event.rectangle.width < 150 ? 150 : 352}px`,
      };
      this.contentStyle = {
        'margin-left': `${event.rectangle.width < 150 ? 150 : 352}px`,
      };
    }
  }

  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  // public openRightMenuSidenav() {
  //   this.rightMenuSidenav.toggle();
  // }
}

