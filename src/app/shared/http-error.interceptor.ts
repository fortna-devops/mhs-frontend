import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable, throwError, timer } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AuthService } from '../core/auth.service';
import { Injectable } from '@angular/core';
import { TokenService } from './token.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    public authService: AuthService,
    private tokenService: TokenService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {

          const cookieExpirationTime = this.tokenService.getCookieExpirationTime();
          // const cookieExpirationTime = new Date();
          const cookieExpirationTimeStamp = new Date(cookieExpirationTime);
          const countdown = cookieExpirationTimeStamp.getTime() - new Date().getTime();

          if (countdown <= 10000 && cookieExpirationTime) {
              this.authService.removeCookies();
              setTimeout(() => {
                // window.location.href = '.';
                window.location.assign('.');
              }, 200);
          }

          return throwError(error);
        })
      );
  }
}
