import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { TopNavService } from '../shared/top-nav/top-nav.service';
import { LeftMenuService } from './left-menu.service';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss'],
})
export class LeftMenuComponent implements OnInit {
  public activeSiteName: string;
  public levels: number;
  public menuData: Array<any>;
  public activeMenuData: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService
  ) {}

  ngOnInit() {
    const leftMenuData = this.leftMenuService.getLeftMenuData();
    this.activeSiteName = this.topNavService.getSiteGroup();
    this.levels = this.leftMenuService.getLeftMenuLevels();
    this.menuData = leftMenuData[this.leftMenuService.getMenuLevelKey(this.levels)];

    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationStart) {
        const leftMenuLevels = this.leftMenuService.getLeftMenuLevels();

        const path = ev['url'].split('/');
        if (leftMenuLevels === 2 && path.length >= 3) {
          this.topNavService.setSiteId(path[2]);
        }
      }
    });

    this.activatedRoute.params.subscribe((params) => {
      this.activeMenuData = params;
    });

    if (!this.topNavService.getSiteId()) {
      const path = document.location.pathname.split('/');
      this.topNavService.setSiteId(path[2]);
    }
  }

  public goToHomePage() {
    this.router.navigate([`./`], { relativeTo: this.activatedRoute });
  }
}
