import { Component, OnInit, Input, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { FacilityOverviewService } from '../../compliance/facility/facility-overview/facility-overview.service';
import { TokenService } from '../../shared/token.service';

@Component({
  selector: 'app-report-left-menu',
  templateUrl: './report-left-menu.component.html',
  styleUrls: ['./report-left-menu.component.scss'],
})
export class ReportLeftMenuComponent implements OnInit, OnDestroy {
  @Input() sites;
  @ViewChildren('sitesExpansionPanel') sitesExpansionPanel: QueryList<MatExpansionPanel>;

  public siteList = [];
  public site = '';
  private subscription: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private facilityOverviewService: FacilityOverviewService,
    private tokenService: TokenService
  ) {
    this.getSiteList();
    this.subscription = this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        const paths = ev['url'].substr(8).split('/');

        this.siteList = this.siteList.map((site) => {
          site['isReportsActive'] = false;
          site['isTrendsActive'] = false;
          return site;
        });

        if (paths.length === 2) {
          this.siteList = this.siteList.map((site) => {
            if (paths[0] === site['name']) {
              this.site = site['name'];
              site['panelOpenState'] = true;
              if (paths[1] === 'reports') {
                site['isReportsActive'] = true;
              }
              if (paths[1] === 'trends') {
                site['isTrendsActive'] = true;
              }
            }
            return site;
          });
        }
      }
    });
  }

  ngOnInit() {
    setTimeout(() => {
      if (this.site) {
        this.expandParentPanel(this.site.replace(/\s/gi, '_'));
      } else {
        this.expandParentPanel(this.siteList[0]['name']);
      }
    }, 100);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getSiteList() {
    const sites = this.facilityOverviewService.getSites();
    this.siteList = sites.map((item, index) => {
      return {
        name: item.site,
        title: item.name.replace(/_/g, ' '),
        id: item.site,
        panelOpenState: index === 0 ? true : false,
        icon: `assets/images/compliance/icons/report-facility.svg`,
        isReportsActive: index === 0 ? true : false,
        isTrendsActive: false,
      };
    });
  }

  private expandParentPanel(site) {
    if (this.sitesExpansionPanel && this.sitesExpansionPanel['_results']) {
      const elements = this.sitesExpansionPanel['_results'].filter((item) => {
        return item['_viewContainerRef'].element.nativeElement.getAttribute('id') === site;
      });
      if (Array.isArray(elements) && elements.length > 0) {
        elements[0].open();
      }
    }
  }

  public expandPanel(site: any, matExpansionPanel: MatExpansionPanel, event: Event) {
    this.leftMenuService.expandPanel(matExpansionPanel, event);
    if (this.leftMenuService.isExpansionIndicator(event.target)) {
      site.panelOpenState = !site.panelOpenState;
      return;
    }
  }

  public goToReports(site) {
    this.router.navigate([`./${site}/reports`], { relativeTo: this.activatedRoute });
  }

  public goToTrends(site) {
    this.router.navigate([`./${site}/trends`], { relativeTo: this.activatedRoute });
  }
}
