import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcknowledgementsDialogComponent } from './acknowledgements-dialog.component';

describe('AcknowledgementsDialogComponent', () => {
  let component: AcknowledgementsDialogComponent;
  let fixture: ComponentFixture<AcknowledgementsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcknowledgementsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcknowledgementsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
