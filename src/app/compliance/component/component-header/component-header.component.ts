import { LeftMenuService } from '../../../left-menu/left-menu.service';
import { Component, OnInit, Input, AfterViewInit, SimpleChanges, OnChanges } from '@angular/core';
import { trigger, state, style, transition, animate, query } from '@angular/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplianceService } from '../../compliance.service';
import { PRIORITY_COLORS } from '../../../shared/mapping';

export const childAnimation = trigger('childAnimation', [
  transition('* => *', [
    query(':enter', [style({ opacity: 0, transform: 'translateX(0%)' })], { optional: true }),

    query(':leave', [style({ opacity: 1 }), animate('5.5s', style({ opacity: 0 }))], {
      optional: true,
    }),

    query(
      ':enter',
      [
        style({ opacity: 0 }),
        animate('5.5s', style({ opacity: 1, transform: 'translateX(-100%)' })),
      ],
      { optional: true }
    ),
  ]),
]);

// work on that when Manish adds more details
const FEED_AIR = 'FEED_AIR';
const MOTOR = 'MOTOR';
const VFD_WITH_MOTOR = 'VFD';
const FRAME = 'FRAME';

@Component({
  selector: 'app-component-header',
  templateUrl: './component-header.component.html',
  styleUrls: ['./component-header.component.scss'],
  animations: [childAnimation],
})
export class ComponentHeaderComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() details;
  @Input() needAsset = true;
  @Input() needAnimation = true;
  @Input() site;

  public gearboxAnimation = {
    component: '',
    fade: '',
    class: 'img img--eng img--eng--green',
  };

  public motorAnimation = {
    component: '',
    fade: '',
    class: 'img img--motor img--motor--green',
  };

  public beltAnimation = {
    component: '',
    fade: '',
    class: 'img img--belt img--belt--green',
  };

  public bearingAnimation = {
    component: '',
    fade: '',
    class: 'img img--bearing img--bearing--green',
  };

  public vfdWithMotorInfo = {
    vfd: {
      path: `./assets/images/compliance/asset/asset_rb/vfd_hq.png`,
      class: '',
    },
    vfd_motor: {
      path: `./assets/images/compliance/asset/asset_rb/vfd_motor_hq.png`,
      class: ``,
    },
  };

  public feedAirInfo = {
    nozzle: {
      path: `./assets/images/compliance/asset/asset_rb/feed_air_nozzle_hq.png`,
      class: `component-img feed-air-nozzle-img`,
    },
    air: {
      path: `./assets/images/compliance/asset/asset_rb/feed_air_air_hq.png`,
      class: '',
    },
  };

  public frameInfo = {
    balks: {
      path: `./assets/images/compliance/asset/asset_rb/frame_balks_hq.png`,
      class: '',
    },
    planks: {
      path: `./assets/images/compliance/asset/asset_rb/frame_planks_hq.png`,
      class: '',
    },
    legs_left: {
      path: `./assets/images/compliance/asset/asset_rb/frame_legs_left_hq.png`,
      class: '',
    },
    legs_right: {
      path: `./assets/images/compliance/asset/asset_rb/frame_legs_right_hq.png`,
      class: '',
    },
  };

  public assetColorClasses = {
    B: '',
    BELT: '',
    G: '',
    M: '',
  };

  public assetAnimation = true;
  public isDivIn = true;
  public component: string;
  public componentId: string;
  public imageId: string;
  public imagePath: string;
  public vfdImagePath: string;
  public frameLeftLegsImagePath: string;
  public frameRightLegsImagePath: string;
  public subImagePath: string;
  public imgClass: string;
  public subImgClass: string;
  public color: string;
  public showAnimation: boolean;
  public isVFDWithMotor: boolean;
  public isFrame: boolean;
  public isFeedAir: boolean;
  public isGeneric: boolean;

  private urlPrams: any;
  private components = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private leftMenuService: LeftMenuService,
    private complianceService: ComplianceService
  ) {}

  ngOnInit() {
    const leftMenuData = this.leftMenuService.getLeftMenuData();
    const levels = this.leftMenuService.getLeftMenuLevels();
    this.showAnimation = true;
    this.activatedRoute.params.subscribe((params) => {
      this.urlPrams = params;
      this.componentId = params['component'].replace('component-', '');
      this.imageId = this.details['type']['name'];

      this.components = this.getComponents(leftMenuData, levels, params);
      this.setComponentColor();
      this.setComponentClassAndPath();
    });
  }

  private setComponentClassAndPath() {
    this.isVFDWithMotor = false;
    this.isFrame = false;
    this.isFeedAir = false;
    this.isGeneric = false;
    this.vfdWithMotorInfo['vfd']['class'] = '';
    Object.keys(this.frameInfo).map((item) => {
      this.frameInfo[item]['class'] = '';
    });
    this.imagePath = '';
    this.imgClass = '';
    const IMAGE_ID = this.imageId ? this.imageId.toUpperCase() : '';

    // tslint:disable-next-line: curly
    if (!this.imageId) return;

    if (IMAGE_ID === VFD_WITH_MOTOR) {
      this.vfdWithMotorInfo['vfd']['class'] = `component-img vfd-img --${this.color}`;
      this.vfdWithMotorInfo['vfd_motor']['class'] = `component-img vfd-motor-img`;
      this.isVFDWithMotor = true;
    }

    if (IMAGE_ID === MOTOR && this.urlPrams['site'] === 'dhl-brescia') {
      this.vfdWithMotorInfo['vfd']['class'] = `component-img vfd-img`;
      this.vfdWithMotorInfo['vfd_motor']['class'] = `component-img vfd-motor-img --${this.color}`;
      this.isVFDWithMotor = true;
    }

    if (IMAGE_ID === FRAME) {
      const frameParts = Object.keys(this.frameInfo);
      frameParts.map((item) => {
        this.frameInfo[item]['class'] = `component-img frame-${item}-img --${this.color}`;
      });
      this.isFrame = true;
    }

    if (IMAGE_ID === FEED_AIR) {
      this.feedAirInfo['air']['class'] = `component-img feed-air-air-img --${this.color}`;
      this.isFeedAir = true;
    }

    if (IMAGE_ID !== FRAME && IMAGE_ID !== VFD_WITH_MOTOR && IMAGE_ID !== FEED_AIR) {
      this.imagePath = `./assets/images/compliance/asset/asset_rb/${this.imageId
        .split(' ')
        .join('_')
        .toLocaleLowerCase()}_hq.png`;
      this.imgClass = `equpment-big component-img ${this.imageId
        .split(' ')
        .join('_')
        .toLocaleLowerCase()}-img --${this.color}`;
      this.isGeneric = true;
      if (IMAGE_ID === MOTOR && this.urlPrams['site'] === 'dhl-brescia') {
        this.isGeneric = false;
      }
    }
  }

  private setComponentColor() {
    const component = this.components.find((item) => {
      return item['id'] == this.componentId;
    });
    this.color = component ? component['color'].toLowerCase() : 'gray';
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setComponentColor();
    this.setComponentClassAndPath();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.showAnimation = false;
    }, 100);
  }

  private getComponents(menuResult, leves, paramList) {
    const result = [];

    if (leves === 2) {
      menuResult['facilities']
        .filter((item) => item['site'].replace('-', '_') === paramList['site'].replace('-', '_')) 
        .forEach((facility) => {
          facility['facility_areas']
          .filter((facilityArea) => facilityArea['name'].replace(/\s/gi, '_') === paramList['facilityArea'].replace(/\s/gi, '_'))
          .forEach((facilityArea) => {
            facilityArea['assets']
            .filter((asset) => asset['id'] == paramList['asset'].replace('asset-', ''))
            .forEach((asset) => {
              result.push(...asset['components']);
            });
          });
        });
    }

    if (leves === 4) {
      menuResult['countries']
        .filter(
          (country) =>
            country['name'].replace(/\s/gi, '_') === paramList['country'].replace(/\s/gi, '_')
        )
        .forEach((country) => {
          country['regions']
            .filter(
              (region) =>
              region['name'].replace(/\s/gi, '_') ===
                paramList['region'].replace(/\s/gi, '_')
            )
            .forEach((mainRegion) => {
              mainRegion['facilities']
                .filter((facility) => facility['site'].replace('-', '_') === paramList['site'].replace('-', '_'))
                .forEach((facility) => {
                  facility['facility_areas']
                  .filter((facilityArea) => facilityArea['name'].replace(/\s/gi, '_') === paramList['facilityArea'].replace(/\s/gi, '_'))
                  .forEach((facilityArea) => {
                    facilityArea['assets']
                    .filter((asset) => asset['id'] == paramList['asset'].replace('asset-', ''))
                    .forEach((asset) => {
                      result.push(...asset['components']);
                    });
                  });
                });
            });
        });
    }

    return result;
  }

  public animationItemDone() {
    this.assetAnimation = !this.assetAnimation;
  }

  public goToItemPage(component) {
    // tslint:disable-next-line:max-line-length
    if (
      this.components.filter((item) => item['id'] === component.toUpperCase())
        .length === 0
    ) {
      return;
    }
    // tslint:disable-next-line: max-line-length
    const url = `/MHS/${this.urlPrams['country']}/${this.urlPrams['region']}/${this.urlPrams['site']}/${this.urlPrams['facilityArea']}/asset-${this.urlPrams['asset']}/component-${component}`;
    this.router.navigate([`${url}`], { relativeTo: this.activatedRoute });
  }
}
