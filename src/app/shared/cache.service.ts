import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CacheService {
  private alarms: any;
  private componentDetails = [];
  private assetDetails = [];
  private assetStatus = [];
  private acknowledgements = [];
  private alarmDateCounts = [];
  private alarmCounts = [];

  constructor() {}

  public getAlarms() {
    return this.alarms;
  }

  public setAlarms(alarms) {
    this.alarms = alarms;
  }

  public getComponentDetails() {
    return this.componentDetails;
  }

  public getAssetDetails() {
    return this.assetDetails;
  }

  public setAssetDetails(details, site, assetId) {
    const alreadyCachedItem = this.assetDetails.filter(
      (item) => item.site === site && item.assetId === assetId
    );

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.assetDetails.push({
      site: site,
      assetId: assetId,
      data: details,
    });
  }

  public setComponentDetails(details, site, componentId) {
    const alreadyCachedItem = this.componentDetails.filter(
      (item) => item.site === site && item.componentId === componentId
    );

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.componentDetails.push({
      site: site,
      componentId: componentId,
      data: details,
    });
  }



  public getAssetStatus() {
    return this.assetStatus;
  }

  public setAssetStatus(assetStatus, site, assetName) {
    const alreadyCachedItem = this.assetStatus.filter(
      (item) => item.site === site && item.assetName === assetName
    );

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.assetStatus.push({
      site: site,
      assetName: assetName,
      data: assetStatus,
    });
  }

  public getAcknowledgements() {
    return this.acknowledgements;
  }

  public setAcknowledgements(acknowledgements, site) {
    const alreadyCachedItem = this.acknowledgements.filter((item) => item.site === site);

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.acknowledgements.push({
      site: site,
      data: acknowledgements,
    });
  }

  public getAlarmDateCounts() {
    return this.alarmDateCounts;
  }

  public setAlarmDateCounts(params) {
    const alreadyCachedItem = this.alarmDateCounts
      .filter((item) => item.site === params.site)
      .filter((item) => item.assetId === params.assetId)
      .filter((item) => item.startDate === params.startDate)
      .filter((item) => item.endDate === params.endDate);

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.alarmDateCounts.push({
      site: params.site,
      startDate: params.startDate,
      endDate: params.endDate,
      assetId: params.assetId,
      data: params.data,
    });
  }

  public getAlarmCounts() {
    return this.alarmCounts;
  }

  public setAlarmCounts(params) {
    const alreadyCachedItem = this.alarmCounts
      .filter((item) => item.site === params.site)
      .filter((item) => item.assetId === params.assetId)
      .filter((item) => item.startDate === params.startDate)
      .filter((item) => item.endDate === params.endDate);

    if (alreadyCachedItem.length > 0) {
      return;
    }

    this.alarmCounts.push({
      site: params.site,
      startDate: params.startDate,
      endDate: params.endDate,
      assetId: params.assetId,
      data: params.data,
    });
  }
}
