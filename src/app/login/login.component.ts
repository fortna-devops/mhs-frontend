import { Component, OnInit } from '@angular/core';
import { UserLoginService } from './user-login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public username: string;
  public password: string;

  constructor(public userService: UserLoginService) {}

  ngOnInit() {}

  signIn() {
    console.log('SignIn start');
    this.userService.authenticate(this.username, this.password).subscribe({
      next(res) {
        console.log(res);
        const accessToken = res.result.getAccessToken().getJwtToken();
      },
      error(err) {
        console.error('something wrong occurred: ' + err);
      },
      complete() {
        console.log('done');
      }
    });
  }
}
