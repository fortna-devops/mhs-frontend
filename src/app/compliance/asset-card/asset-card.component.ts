import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { MANUFACTURE_FEDEX } from '../manufacture-data';

@Component({
  selector: 'app-asset-card',
  templateUrl: './asset-card.component.html',
  styleUrls: ['./asset-card.component.scss'],
})
export class AssetCardComponent implements OnInit {
  @Input() item: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private topNav: TopNavService
  ) {}

  ngOnInit() {
    // let manufacturer;
    // switch (this.topNav.getSiteId()) {
    //   case '1':
    //     manufacturer = MANUFACTURE_DHL;
    //     break;
    //   case '2':
    //     manufacturer = MANUFACTURE_FEDEX;
    //     break;
    // }
    // this.item['manufacturer'] = MANUFACTURE_FEDEX[this.item['sensor']] || '';
  }

  public actionsClick(alert: any) {
    // alert.actions = true;
    // this.alertService.setAlert(alert);
  }

  public escalateClick(alert: any) {
    // this.alertService.setModalMessage('Escalated Alert');
  }

  public goToAlertAsset(alert) {
    const asset = 'asset-' + alert['assetId'];
    let url = `${asset}`;

    if (alert['region']) {
      url = `${alert['region']}/${asset}`;
    }

    this.router.navigate([`./${url.replace(/\s/gi, '_')}`], { relativeTo: this.route });
  }
}
