import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatIconRegistry, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ComplianceService, DONUT_CHART } from '../../compliance.service';
import { LeftMenuService } from '../../../left-menu/left-menu.service';
import { TopNavService } from '../../../shared/top-nav/top-nav.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ToasterService } from 'angular2-toaster';
import { PRIORITY_COLORS } from '../../../shared/mapping';
import { FacilityOverviewService, PIE_CHART_COLORS } from './facility-overview.service';

declare var initViz: any;

@Component({
  selector: 'app-facility-overview',
  templateUrl: './facility-overview.component.html',
  styleUrls: ['./facility-overview.component.scss'],
})
export class FacilityOverviewComponent implements OnInit {
  @ViewChild('pieChart') pieChart;

  public mainPieChartData: any;
  public inboundPieChartData: any;
  public pickmodsPieChartData: any;
  public ptsPieChartData: any;
  public shippingPieChartData: any;
  public pieChartList: Array<any> = [];
  public title: string;
  public donutChart = DONUT_CHART;
  public allErrorLog: Array<any> = [];
  public assetsWithErrors: Array<any> = [];
  public length = 0;
  public pageSize: number;
  public pageSizeOptions: number[] = [];
  public overallFacilityHealth: any;
  public facilityAreasHealth: any;
  public facilityAreasColors: any;
  public pageEvent: PageEvent;
  public theme: string;
  public showFacilityLayoutMap: boolean;
  public acknowledgements: Array<any>;
  private site: string;
  public isLoading: boolean;
  public loader: string;
  public leftMenuData: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private complianceService: ComplianceService,
    private leftMenuService: LeftMenuService,
    private topNavService: TopNavService,
    private deviceService: DeviceDetectorService,
    private overviewService: FacilityOverviewService,
    private toasterService: ToasterService,
    private dialog: MatDialog
  ) {
    iconRegistry.addSvgIcon(
      'map-dark',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/compliance/icons/baseline-map-dark.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'map-light',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/images/compliance/icons/baseline-map-light.svg'
      )
    );
    this.loader = 'assets/images/compliance/icons/loader.svg';
  }

  ngOnInit() {
    this.theme = this.topNavService.getTheme();
    this.leftMenuData = this.leftMenuService.getLeftMenuData();

    this.complianceService.getChangeScreenWidthEvent().subscribe((event) => {
      if (this.pieChart && this.pieChart.chart) {
        this.pieChart.chart.update();
      }
    });

    this.route.params.subscribe((params) => {
      this.isLoading = true;
      this.setFacilityHealth(params['site'])
      this.title = this.overviewService.getTitle(params);
      this.topNavService.setSiteId(params['site']);
      this.showFacilityLayoutMap =
        this.topNavService.getSiteId() === 'dhl-miami' ||
        this.topNavService.getSiteId() === 'amazon-sdf4';
      this.initDonutChart();
    });
  }

  setFacilityHealth(site) {
    const facilities = this.complianceService.getFacilityOverview(this.leftMenuData, site);
    const alarmsWithError = facilities['red_assets'].concat(facilities['yellow_assets']);

    this.assetsWithErrors = alarmsWithError.map((asset) => {
      return {
        asset: asset['name'],
        region: this.getRegion(this.leftMenuData, asset['name']),
        icon: `./assets/images/compliance/icons/asset-${PRIORITY_COLORS[asset['color']]}.svg`,
      };
    });

    this.facilityAreasColors = this.getfacilityAreasColors(this.leftMenuData);

    this.facilityAreasHealth = facilities['facility_area_percentages'].map(item => { 
      item['color'] = this.facilityAreasColors[item.name]; 
      return item;
    });
    const areaPercentages = facilities['facility_area_percentages'];
    this.setPagination(areaPercentages);
    this.donutChart.loading = false;
    this.overallFacilityHealth = facilities['percentages'];
    // TODO
    // this.populateDonutChart(facilities['total_percentage']);

    this.pieChartList = [];

    areaPercentages.forEach((item) => {
      const data = {
        critical: item['percentages']['red_components'],
        moderate: item['percentages']['yellow_components'],
        normal: item['percentages']['green_components'],
      };
      this.pieChartList.push({
        title: item['name'],
        data: this.overviewService.getPieChartData(data, 80, 120, item),
      });
    });
  }

  getfacilityAreasColors(leftMenuData) {
    const colors = {};

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities'].forEach((facility) => {
        facility['facility_areas'].forEach((facilityArea) => {
          colors[facilityArea['name']] = facilityArea['color'].toLowerCase();
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((facility) => {
            facility['facility_areas'].forEach((facilityArea) => {
              colors[facilityArea['name']] = facilityArea['color'].toLowerCase();
            });
          });
        });
      });
    }

    return colors;
  }

  private getRegion(leftMenuData, assetName) {
    let activeRegion: string;

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities'].forEach((facility) => {
        facility['facility_areas'].forEach((facilityArea) => {
          facilityArea['assets'].forEach((asset) => {
            if (asset['name'] === assetName) {
              activeRegion = facilityArea['name'];
            }
          });
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((facility) => {
            facility['facility_areas'].forEach((facilityArea) => {
              facilityArea['assets'].forEach((asset) => {
                if (asset['name'] === assetName) {
                  activeRegion = facilityArea['name'];
                }
              });
            });
          });
        });
      });
    }

    return activeRegion;
  }

  public goToAlert(routerLink) {
    this.router.navigate([`./${routerLink.replace(/\s/gi, '_')}`], {
      relativeTo: this.route,
    });
  }

  private initDonutChart() {
    this.donutChart.d3Options = this.overviewService.getDonutChartD3Options();
    this.donutChart.data = [];
    this.donutChart.loading = true;
    this.donutChart.normal = '';
  }

  private populateDonutChart(data) {
    this.donutChart.normal = `${data['normal']} %`;
    this.donutChart.data = [
      {
        label: `${parseFloat(data['normal']).toFixed(2)} % - Normal`,
        tooltipLabel: 'Normal',
        type: 'normal',
        value: parseFloat(data['normal']).toFixed(2),
        color: PIE_CHART_COLORS['normal'],
      },
      {
        label: `${parseFloat(data['critical']).toFixed(2)} % - Critical`,
        tooltipLabel: 'Critical',
        type: 'critical',
        value: parseFloat(data['critical']).toFixed(2),
        color: PIE_CHART_COLORS['critical'],
      },
      {
        label: `${parseFloat(data['moderate']).toFixed(2)} % - Moderate`,
        tooltipLabel: 'Moderate',
        type: 'moderate',
        value: parseFloat(data['moderate']).toFixed(2),
        color: PIE_CHART_COLORS['moderate'],
      },
    ];
  }

  public showDonutChart() {
    return this.donutChart.data.some((item) => item.value !== 0);
  }

  private setPagination(areaPercentages) {
    this.pageSize = this.overviewService.getPageSize(
      areaPercentages,
      this.deviceService.isTablet()
    );
    this.length = this.allErrorLog.length;
  }

  public changePageEvent(event) {
    const startIndex = event['pageIndex'] * this.pageSize;
  }

  public goToDetailsPage(alarm) {
    const url = `${alarm['region']}/asset-${alarm['asset']}/all`;

    this.router.navigate([`./${url.replace(/\s/gi, '_')}`], {
      queryParams: {
        sensor: alarm['alarmAllData']['sensor'],
        timestamp: new Date(alarm['alarmAllData']['data_timestamp']).getTime(),
        componentType: alarm['alarmAllData']['component_type'],
        tagName: this.mapTagName(alarm['tag_name']),
      },
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  private mapTagName(tagName) {
    return tagName === 'vibrations' ? 'Vibration' : tagName[0].toUpperCase() + tagName.slice(1);
  }

  // tslint:disable-next-line:use-life-cycle-interface
  // ngAfterViewInit() {
  //   initViz();
  // }
}
