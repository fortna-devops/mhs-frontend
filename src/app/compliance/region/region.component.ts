import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToasterService } from 'angular2-toaster';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { ComplianceService, DONUT_CHART } from '../compliance.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { RegionService } from './region.service';
import { FavoritesService } from '../favorites/favorites.service';

@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss'],
})
export class RegionComponent implements OnInit, OnDestroy {
  @ViewChild('regionMapContainer') regionMapContainer: ElementRef;
  @ViewChild('regionHealthNvd3') regionHealthNvd3;

  public alertAssetList: {};
  private subscription: Subscription;
  public pieChartTitle = '';
  public donutChart = DONUT_CHART;
  public pieChartData: any;

  public facilityList = [];
  public assetsWithErrors = [];
  private theme: string;
  private region: string;
  public regionLabel: string;
  public regionName: string;
  public regionHealthInfo = {};
  private country: string;
  public facilityListTitle = 'Health By Facility';

  // favorites part
  public href: string;
  public isFavorite = false;
  public showRegionHealth = false;
  private favoritesubscription: Subscription;
  private routeSubscription: Subscription;
  layoutMap: any;

  constructor(
    private topNavService: TopNavService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toasterService: ToasterService,
    private leftMenuService: LeftMenuService,
    private complianceService: ComplianceService,
    private regionService: RegionService,
    private favoritesService: FavoritesService
  ) {}

  ngOnInit() {
    this.complianceService.getChangeScreenWidthEvent().subscribe((event) => {
      if (this.regionHealthNvd3 && this.regionHealthNvd3.chart) {
        this.regionHealthNvd3.chart.update();
      }
    });

    this.subscription = this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.layoutMap = this.regionService.getLayoutMap(theme, this.region);
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      this.setMapLayout(leftMenuData, this.facilityList);
    });

    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      this.href = this.router.url;
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;

      this.country = params['country'];
      this.region = params['region'];
      this.regionLabel = this.country.toLowerCase() === 'europe' ? this.region : `${this.region} Region`;
      this.regionName = this.region.replace('_', ' ');
      this.theme = this.topNavService.getTheme();
      this.layoutMap = this.regionService.getLayoutMap(this.theme, this.country);

      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const { overviewData, regionData } = this.regionService.getOverviewData(
        leftMenuData,
        params
      );
      this.facilityList = this.regionService.getFacilities(overviewData, regionData);
      this.setMapLayout(leftMenuData, this.facilityList);
      this.regionHealthInfo = this.complianceService.getLevelHealthInfo(this.facilityList);
      if (this.facilityList.length > 1) {
        this.facilityListTitle = 'Health By Facility';
        this.showRegionHealth = true;
      } else {
        this.facilityListTitle = `Facility Health`;
        this.showRegionHealth = false;
      }
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
      }
    });

  }

  private setMapLayout(menuData, facilityList) {
    const that = this;
    const getActiveRegionColor = this.complianceService.getActiveRegionColor(this.theme);
    const { activeRegions, activeRegionColors, facilitiesFullName } = this.getMapInfo(menuData);

    d3.xml(this.layoutMap)
      .mimeType('image/svg+xml')
      .get(function (error, xml) {
        if (error) {
          that.toasterService.pop('error', '', `An error occurred. Please try again later`);
          return;
        }

        const tooltip = d3
          .select('body')
          .append('div')
          .attr('class', 'assettooltip')
          .style('opacity', 0);

        if (that.regionMapContainer.nativeElement['hasChildNodes']()) {
          that.regionMapContainer.nativeElement['removeChild'](
            that.regionMapContainer.nativeElement['lastChild']
          );
        }
        that.regionMapContainer.nativeElement['appendChild'](xml.documentElement);
        const svg = d3.select(that.regionMapContainer.nativeElement.children[0]);

        svg.selectAll('path, polygon').each(function (d, i) {
          if (d3.select(this).attr('class')) {
            d3.select(this).style('fill', () => {
              if (activeRegions.indexOf(d3.select(this).attr('class')) > -1) {
                return getActiveRegionColor;
              }
            });
          }
        });

        svg.selectAll('circle').each(function (d, i) {
          if (activeRegions.indexOf(this['id'].split('@@')[0]) === -1) {
            return;
          }

         d3.select(this)
            .style('fill', (item) => {
              const status = activeRegionColors[d3.select(this).attr('id').split('@@')[1]];
              return status ? status.toLowerCase() : null;
            })
            .attr('r', '6')
            // .attr('stroke', 'gray')
            .on('click', function () {
              tooltip.transition().style('opacity', 0);

              const regionAndFacility = d3.select(this).attr('id').split('@@');
              const region = regionAndFacility[0];
              const facility = regionAndFacility[1];

              that.router.navigate([`./${facility}`], {
                relativeTo: that.activatedRoute,
              });
            })
            .on('mouseover', function () {
              d3.select(this).attr('stroke', 'gray');
              d3.select(this).attr('r', '7');
              d3.select(this).style('cursor', 'hand');

              const site = d3.select(this).attr('id').split('@@')[1];
              const facilityTotalCount = facilityList.find(item => item['site'] === site);
              tooltip.transition().duration(300).style('opacity', 0.9).style('z-index', 400);
              tooltip
                .html(
                  `${facilitiesFullName[site]} </br>
                    Assets In Normal Condition: ${facilityTotalCount['num_green_assets']} </br>
                    Assets In Moderate Condition: ${facilityTotalCount['num_yellow_assets']} </br>
                    Assets In Severe Condition: ${facilityTotalCount['num_red_assets']}`
                )
                .style('left', d3.event['pageX'] + 10 + 'px')
                .style('top', d3.event['pageY'] - 60 + 'px');
            })
            .on('mouseout', function () {
              d3.select(this).attr('stroke', 'none');
              d3.select(this).attr('r', '6');
              tooltip.transition().duration(500).style('opacity', 0).style('z-index', 0);
            });
        });
      });
  }

  public getFacilitiesAssetsCount() {
    let severity: string;

    const menuResult = this.leftMenuService.getLeftMenuData();

    menuResult['countries']
      .filter((item) => item['name'].replace(/\s/gi, '_') === this.country.replace(/\s/gi, '_'))
      .forEach((country) => {
        country['regions']
          .filter(
            (item) => item['name'].replace(/\s/gi, '_') === this.regionName.replace(/\s/gi, '_')
          )
          .forEach((item) => {
            severity = item['color'];
          });
      });

    return PRIORITY_COLORS[severity];
  }

  private getMapInfo(menuResult) {
    const activeRegions = [];
    const activeRegionColors = {};
    const facilitiesFullName = {};

    menuResult['countries']
      .filter((item) => item['name'].replace(/\s/gi, '_') === this.country.replace(/\s/gi, '_'))
      .forEach((country) => {
        country['regions']
          .filter(
            (item) => item['name'].replace(/\s/gi, '_') === this.regionName.replace(/\s/gi, '_')
          )
          .forEach((region) => {
            activeRegions.push(region['name']);
            region.facilities.forEach((facility) => {
              const facilityName = facility['name'].replace(/_/g, ' ');
              facilitiesFullName[facility.site] = facilityName;
              activeRegionColors[facility.site] = facility.color;
            });
          });
      });

    return {
      activeRegions,
      activeRegionColors,
      facilitiesFullName,
    };
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }
}
