import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  public chart = {
    options: {},
    data: []
  };

  constructor() { }

  ngOnInit() {
    this.chart['options'] = this.getLineChartD3Options();
    this.chart['data'] = [{
      key: 'Measured',
      name: 'measured',
      color: '#3B7DFF',
      classed: 'stroke-width-two',
      values: [
        {x: '2019-10-29 08:00:00.000', y: 0.034, series: 0},
        {x: '2019-10-29 09:00:00.000', y: 0.044, series: 0},
        {x: '2019-10-29 10:00:00.000', y: 0.044, series: 0},
        {x: '2019-10-29 11:00:00.000', y: 0.054, series: 0},
        {x: '2019-10-29 12:00:00.000', y: 0.035, series: 0},
        {x: '2019-10-30 00:00:00.000', y: 0.055, series: 0},
        {x: '2019-10-30 01:00:00.000', y: 0.044, series: 0},
        {x: '2019-10-30 02:00:00.000', y: 0.034, series: 0}
      ]
    }];
  }

  public getLineChartD3Options() {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineChart',
        height: 80,
        width: 150,
        margin: {
          top: 20,
          right: 20,
          bottom: 5,
          left: 3
        },
        x: (d) => moment(d.x).toDate(),
        y: (d) => d.y,
        useInteractiveGuideline: false,
        interactiveUpdateDelay: 100,
        duration: 100,
        xAxis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d %H:%M')(moment(d).toDate()),
          rotateLabels: '15'
        },
        yAxis: {
          // axisLabel: params['title'],
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10
        },
        // forceY: [0, params['maxValue'] + (params['maxValue'] * 20 / 100)],
        xScale: d3.time.scale.utc(),
        pointSize: 30,
        tooltip: {
          // position: function () {
          //   const brect = this.chart.getBoundingClientRect();
          //   return {
          //     top: brect.top + brect.height / 2,
          //     left: brect.left + brect.width / 2
          //   };
          // },
          enabled: false
        },
        x2Axis: {
          axisLabel: 'Time',
          tickFormat: (d) => d3.time.format('%b %d')(moment(d).toDate()),
        },
      }
    };

    // if (params['maxValue'] && params['maxValue'] < 0.05) {
    //   lineChartD3Options.chart.yAxis['ticks'] = 0;
    // } else {
    //   lineChartD3Options.chart.yAxis['ticks'] = 5;
    // }

    return lineChartD3Options;
  }

}
