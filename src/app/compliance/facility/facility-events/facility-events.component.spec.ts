import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityEventsComponent } from './facility-events.component';

describe('FacilityEventsComponent', () => {
  let component: FacilityEventsComponent;
  let fixture: ComponentFixture<FacilityEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
