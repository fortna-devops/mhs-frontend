import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

const GAUGE_COLORS = {
  // red: '#a9aaab',
  // yellow: '#a9aaab',
  // green: '#a9aaab',
  red: '#FF5252',
  yellow: '#FFBB38',
  green: '#00E893',
};

@Component({
  selector: 'app-health-gauge',
  templateUrl: './health-gauge.component.html',
  styleUrls: ['./health-gauge.component.scss'],
})
export class HealthGaugeComponent implements OnInit, OnChanges {
  @Input() health;
  public gray = false;
  
  constructor() {}

  ngOnInit() {
    this.getHealthDeg()
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getHealthDeg()
    if(this.health == 'ASSET OFF') {
      this.gray = true;
    }
  }

  getHealthDeg() {
    document.getElementById("needle").style.transform = 'rotate(' + (this.health * 2) + 'deg)';
  }
  
}
