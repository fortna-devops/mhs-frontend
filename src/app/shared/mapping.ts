export const PRIORITY_COLORS = {
  '1': 'green',
  '2': 'yellow',
  '3': 'red',
};

export const PRIORITY_COLORS_HEX = {
  'green': '#00e893',
  'yellow': '#ffcd0b',
  'red': '#ff5252',
};

export const PRIORITY_COLOR_NAMES = {
  green: '1',
  yellow: '2',
  red: '3',
};
