import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TopNavService } from './top-nav.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { PreviousRouteService } from '../previous-route.service';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { TokenService } from '../token.service';
import { TranslateService } from '@ngx-translate/core';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { Subscription } from 'rxjs';
import { SettingsDialogComponent } from '../settings-dialog/settings-dialog.component';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss'],
})
export class TopNavComponent implements OnInit {
  public customerList: Array<any> = [];
  public customerListAll: Array<any> = [];
  public siteId: string;
  public activeCustomerName: string;
  public visible = true;
  public theme = 'theme-dark';
  public logo: string;
  public currentFlagUrl: string;
  public user: string;
  public title: string;
  public notificationCount = null;
  public isAdmin = false;
  private routeSubscription: Subscription;
  private optimizationSubscription: Subscription;
  private eventLogSubscription: Subscription;
  public isLogIn = false;

  constructor(
    public topNavService: TopNavService,
    private tokenService: TokenService,
    private location: Location,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private previousRouteService: PreviousRouteService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private translate: TranslateService,
    private leftMenuService: LeftMenuService
  ) {
    iconRegistry.addSvgIcon(
      'user-icon',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/user.svg')
    );
  }

  ngOnInit() {
    this.isAdmin = this.authService.isUserAdmin();
    this.user = this.topNavService.getUser();
    this.activeCustomerName = this.topNavService.getActiveCustomerName();
    this.customerListAll = this.topNavService.getCustomerList();
    this.customerList = this.topNavService
      .getCustomerList()
      .filter((item) => item['name'] !== this.activeCustomerName);

    this.topNavService.getTitle().subscribe((title) => {
      this.title = title;
    });

    this.topNavService.getVisible().subscribe((visible) => {
      this.visible = visible;
    });

    this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.theme = theme === 'theme-dark' ? 'theme-light' : 'theme-dark';
      this.logo = `./assets/images/${theme}-logo.svg`;
      console.log(this.logo);
    });

    if (this.topNavService.getTheme()) {
      this.theme = this.topNavService.getTheme() === 'theme-dark' ? 'theme-light' : 'theme-dark';
      this.logo = `./assets/images/${this.topNavService.getTheme()}-logo.svg`;
    }

    this.setNotficationsCount();
    this.eventLogSubscription = this.topNavService.eventLogChangedAnnounce$.subscribe(
      (status) => {
        if (status) {
          this.setNotficationsCount();
        }
      }
    );
    // this.optimizationSubscription = this.topNavService.optimizationListChangedAnnounce$.subscribe(
    //   (status) => {
    //     if (status) {
    //       this.setNotficationsCount();
    //     }
    //   }
    // );
    this.currentFlagUrl = `assets/images/compliance/icons/${this.translate.getDefaultLang()}.svg`;
  }

  setNotficationsCount() {
    const count = this.topNavService.getAlarmsCount();

    this.notificationCount = count ? count : null;
  }

  showSettingsDialog() {
    const dialogRef = this.dialog.open(SettingsDialogComponent, {
      width: '40%',
      panelClass: this.topNavService.getTheme(),
    });
  }

  logout() {
    this.tokenService.removeCookies();
    setTimeout(() => {
      // window.location.assign('.');
      location.reload();
    }, 200);
  }

  isLoggedIn(isLoggedIn: boolean) {
    if (!isLoggedIn) {
      this.visible = false;
      this.router.navigate(['/login']);
    } else {
      this.visible = true;
      this.router.navigate(['/MHS']);
    }
  }

  performSearch(value) {
    this.router.navigateByUrl('/search');
  }

  getTitle() {
    this.topNavService.getTitle();
  }

  setCustomer(customerId) {
    const newActiveCustomerName = this.customerList
      .filter((item) => item['id'] === customerId)
      .map((customer) => customer['name'])
      .find((customer) => customer);

    if (newActiveCustomerName === this.activeCustomerName) {
      return;
    }

    this.topNavService.setCustomerId(customerId);
    // window.location.href = '.';
    window.location.assign('.');
  }

  goBack() {
    this.location.back();
  }

  goBackPage() {
    this.location.back();
  }

  goToSignIn() {
    this.router.navigate([`/login`]);
  }

  goPrevPage() {
    const prevUrl = this.previousRouteService.getPreviousUrl();
    this.router.navigate([`.${prevUrl.split('/compliance')[1]}`], {
      relativeTo: this.activatedRoute,
    });
  }

  reloadPage() {
    window.location.reload();
  }

  changeTheme() {
    this.topNavService.setTheme(this.theme);
  }

  goToAdminPanel() {
    this.router.navigate([`/admin/sensors`]);
  }

  goToSparePartsPage() {
    this.router.navigate([`spare-parts-optimization`]);
  }

  goToHome() {
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const country = leftMenuData['countries'][0]['name'].replace(/\s/gi, '_');
      this.router.navigate([`./MHS/${country}`], {
        relativeTo: this.activatedRoute,
      });
    }
    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const site = leftMenuData['facilities'][0]['site'];
      this.router.navigate([`./MHS/${site}`], {
        relativeTo: this.activatedRoute,
      });
    }
  }

  public toggleEventLog() {
    this.topNavService.setEventLog();
  }

  public goToReporting() {
    this.router.navigate([`report`]);
  }
}
