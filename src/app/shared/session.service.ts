import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { CognitoUserPool, CognitoUser, CognitoRefreshToken } from 'amazon-cognito-identity-js';
import { TopNavService } from './top-nav/top-nav.service';
import { TokenService } from './token.service';
import { AuthService } from '../core/auth.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private userPool;
  private sessionUserAttributes;
  private username;
  private cognitoUser;
  private refreshToken;
  public sessionRefreshed = new Subject<any>();

  constructor(
    private topNavService: TopNavService,
    private authService: AuthService,
    private tokenService: TokenService
  ) {}

  public refreshSessionAsObservable(): Observable<any> {
    return this.sessionRefreshed.asObservable();
  }

  public setCognitoUserPool() {
    const poolData = {
      UserPoolId: environment.userPoolId,
      ClientId: environment.clientId,
    };
    return new CognitoUserPool(poolData);
  }

  public getCognitoUser() {
    const userData = {
      Username: this.topNavService.getUser(),
      Pool: this.setCognitoUserPool(),
    };
    return new CognitoUser(userData);
  }

  public onRefreshSession() {
    this.refreshToken = this.tokenService.getRefreshToken();
    const refreshTokenObj = new CognitoRefreshToken({
      RefreshToken: this.tokenService.getRefreshToken(),
    });
    this.refreshSession(refreshTokenObj);
  }

  private refreshSession(refreshTokenObj) {
    const cognitoUser = this.getCognitoUser();
    cognitoUser.refreshSession(refreshTokenObj, (err, session) => {
      if (err) {
        //throw err;
        console.log('In the err');
        console.error(err);
        localStorage.setItem('isLoggedIn', 'false');
        this.tokenService.removeCookies();
        setTimeout(() => {
          location.reload();
        }, 200);
      } else {
        console.log('Refreshing');
        const token = session.getIdToken().getJwtToken();
        const date = new Date(session['idToken'].payload.exp * 1000);
        this.tokenService.deleteCookie('refreshToken');
        this.tokenService.deleteCookie('token');
        this.tokenService.deleteCookie('expires_at');
        setTimeout(() => {
          this.tokenService.setCookie('refreshToken', this.refreshToken);
          this.tokenService.setCookie('token', token);
          this.tokenService.setCookie('expires_at', date.toUTCString());
        }, 200);

        // this.tokenService.setCookie('refreshed', true);
        setTimeout(() => {
          this.sessionRefreshed.next(true);
          // window.location.reload(true);
        }, 400);
      }
    });
  }
}
