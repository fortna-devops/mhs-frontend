import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FavoritesService } from './favorites.service';
import { Subscription } from 'rxjs';
import { TokenService } from '../../shared/token.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { FavLimitPopupComponent } from './fav-limit-popup/fav-limit-popup.component';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnInit {
  public favList: any = [
    // {
    //   url: '/MHS/United_States',
    //   title: 'United States',
    //   status: true
    // },
    // {
    //   url: '/MHS/United_States/Southern/mhs-emulated/Sorter/SS1-4',
    //   title: 'SS1-4',
    //   status: true
    // }
  ];
  public href = '';
  subscription: Subscription;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    public favoritesService: FavoritesService,
    private toasterService: ToasterService,
    private token: TokenService,
    private topNavService: TopNavService,
    private dialog: MatDialog
  ) {
    // this.toasterService.pop(
    //   'error',
    //   '',
    //   `An error occurred. Please try again later`
    // );
    this.getFavorites();
    // this.subscription = favoritesService.favoritesAnnounce$.subscribe((status) => {
    //   if (status) {
    //     this.getFavorites();
    //   }
    // }); 
    this.subscription = favoritesService.maxStackAnnounce$.subscribe((obj: any) => {
      if (obj.url) {
        this.showLimitPopup(obj);
      }
    });
  }

  public showLimitPopup(obj) {
    const dialogRef = this.dialog.open(FavLimitPopupComponent, {
      data: {
        list: this.favList,
      },
      width: '320px',
      panelClass: this.topNavService.getTheme(),
    });

    dialogRef.afterClosed().subscribe((favObj) => {
      if (favObj.url) {
        this.favoritesService.deleteFavorite(favObj.url).subscribe((res) => {
          this.favoritesService.getFavoriteList().subscribe((res: any) => {
            // console.log(res);
            this.toasterService.pop('success', '', `Waiting for server response...`);
            this.favList = res.fav_list ? res.fav_list : [];
            this.favoritesService.deleteFavoritesCookies();
            this.token.setFavorites(JSON.stringify(this.favList));
            this.favoritesService.loadFavorite(true);

            this.toasterService.pop(
              'success',
              '',
              `${favObj.title} page was deleted from favorites`
            );
            this.toasterService.pop('success', '', `Waiting for server response...`);
            this.favoritesService.addNewFavorite(obj.url, obj.title).subscribe((res) => {
              this.favoritesService.announceFavorite(true);
              this.toasterService.pop('success', '', `${obj.title} page was added to favorites`);
            });
          });
        });
      }
    });
  }

  goToPage(item) {
    console.log(item);
    this.router.navigate([item.url.replace(/\s/gi, '_'), { tab: item.selectedIndex }], {
      relativeTo: this.route,
    });
  }

  ngOnInit() {}

  getFavorites() {
    this.favoritesService.getFavoriteList().subscribe((res: any) => {
      // console.log(res);
      this.favList = res && res.fav_list ? res.fav_list : [];
      this.favoritesService.deleteFavoritesCookies();
      this.token.setFavorites(JSON.stringify(this.favList));
      this.favoritesService.loadFavorite(true);
    });
    // console.log(this.favList);
  }
}
