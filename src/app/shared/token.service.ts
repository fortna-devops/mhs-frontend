import { Injectable } from '@angular/core';
import { CookieStorage } from 'cookie-storage';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class TokenService {
  private userGroups: Array<string> = [];
  private cookieStorage = new CookieStorage();

  constructor(private cookieService: CookieService) {}

  public toString(): string {
    return this.getCookie('token');
  }

  public getVersion(): string {
    return this.getCookie('version');
  }

  public setVersion(version) {
    this.setCookie('version', version);
  }

  public getSiteId(): string {
    return this.getCookie('site');
  }

  public getCustomer(): string {
    return this.getCookie('customer');
  }

  public setCustomer(customer): void {
    this.setCookie('customer', customer);
  }

  public getLanguage(): string {
    return this.getCookie('lang');
  }

  public setLanguage(lang): void {
    this.setCookie('lang', lang);
  }
  
  public getTimezone(): string {
    return this.getCookie('tz');
  }

  public setTimezone(lang): void {
    this.setCookie('tz', lang);
  }

  public getTimezoneOffset(): string {
    return this.getCookie('tz_offset');
  }

  public setTimezoneOffset(lang): void {
    this.setCookie('tz_offset', lang);
  }

  public setSiteId(siteId): void {
    this.setCookie('site', siteId);
  }

  public getInit() {
    return this.getCookie('init');
  }

  public setInit() {
    this.setCookie('init', 'false');
  }

  public setCustomerId(customerId): void {
    this.setCookie('customer', customerId);
  }

  public setFavorites(list): void {
    this.setCookie('favorites', list);
  }

  public getFavorites() {
    return this.getCookie('favorites');
  }

  public setOptimizations(list): void {
    this.setCookie('optimizations', list);
  }

  public getAlarmsCount(): string {
    return this.getCookie('alarmsCount');
  }

  public setAlarmsCount(alarmsCount): void {
    this.setCookie('alarmsCount', alarmsCount);
  }

  public getOptimizations() {
    return this.getCookie('optimizations');
  }

  public getSiteList(): string {
    return this.getCookie('sites');
  }

  public getCustomerList(): string {
    return this.getCookie('customers');
  }

  public getRefreshToken(): string {
    return this.getCookie('refreshToken');
  }

  public getCookieExpirationTime() {
    return this.getCookie('expires_at');
  }

  public decodeToken(name = 'token'): any {
    const token = this.cookieService.get(name);
    if (token === null || token === '') {
      return {};
    }

    const parts = token.split('.');
    if (parts.length !== 3) {
      return {};
    }
    let json = parts[1].replace(/-/g, '+').replace(/_/g, '/');
    switch (json.length % 4) {
      case 0:
        break;
      case 2:
        json += '==';
        break;
      case 3:
        json += '=';
        break;
      default:
        return {};
    }
    return JSON.parse(decodeURIComponent((<any>window).escape(window.atob(json))));
  }

  public setUserGroups() {
    this.userGroups = this.decodeToken()['cognito:groups'];
  }

  public getUser() {
    return this.decodeToken()['cognito:username'];
  }

  public getUserGroups() {
    return this.userGroups && this.userGroups.length > 0
      ? this.userGroups
      : this.decodeToken()['cognito:groups'];
  }

  public getAccessKey() {
    return this.getCookie('access_key');
  }

  public getSecretAccessKey() {
    return this.getCookie('secret_access_key');
  }

  public setCookie(tokenName, tokenValue) {
    this.deleteCookie(tokenName);
    this.cookieService.set(tokenName, tokenValue, 30, '/');
  }

  public getCookie(cookie) {
    return this.cookieStorage.getItem(cookie) || null;
  }

  public deleteCookie(cookie): void {
    this.cookieService.delete(cookie, '/');
  }

  public deleteMHSPathCookies() {
    this.cookieService.deleteAll('/MHS');
  }

  public removeCookies() {
    this.cookieService.deleteAll('/');
    this.cookieService.deleteAll('/MHS');
  }
}
