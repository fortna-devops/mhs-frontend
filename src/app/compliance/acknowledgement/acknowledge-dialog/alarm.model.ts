export interface Alarm {
  actual_value: number;
  asset: string;
  component_type: string;
  key_label: string;
  description: string;
  sensor: number;
  severity: string;
  tag_limit: number;
  metric: string;
  tag_name: string;
  timestamp: string;
}
