import { Injectable } from '@angular/core';
import { LeftMenuService } from '../../../left-menu/left-menu.service';

const DEFAULT_PAGE_SIZE_DESKTOP = 8;
const DEFAULT_PAGE_SIZE_TABLET = 9;

export const PIE_CHART_COLORS = {
  critical: '#FF5C6F',
  moderate: '#FFB231',
  normal: '#3B7DFF',
};

@Injectable({
  providedIn: 'root',
})
export class FacilityOverviewService {
  constructor(private leftMenuService: LeftMenuService) {}

  public getSites() {
    const leftMenu = this.leftMenuService.getLeftMenuData();
    const siteList = [];

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenu['facilities'].forEach((item) => {
        siteList.push({
          name: item['name'],
          site: item['site'],
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenu['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((item) => {
            siteList.push({
              name: item['name'],
              site: item['site'],
            });
          });
        });
      });
    }
    return siteList;
  }

  public getTitle(params) {
    const leftMenu = this.leftMenuService.getLeftMenuData();

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const name = leftMenu['facilities']
        .filter((item) => item['site'] === params['site'])
        .map((item) => item.name)[0];

      return `${name.charAt(0).toUpperCase() + name.slice(1)}`;
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      let result: string;

      leftMenu['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((item) => {
            if (item['site'] === params['site']) {
              result = item.name;
            }
          });
        });
      });

      return `${result.charAt(0).toUpperCase() + result.slice(1)}`;
    }
  }

  public getPageSize(chartList, isTablet) {
    if (isTablet && Object.keys(chartList).length > 3) {
      return DEFAULT_PAGE_SIZE_TABLET;
    } else {
      return DEFAULT_PAGE_SIZE_DESKTOP;
    }
  }

  public getDonutChartD3Options(): any {
    return {
      chart: {
        type: 'pieChart',
        showLegend: true,
        height: 200,
        width: 450,
        margin: {
          top: 20,
          right: 0,
          bottom: 0,
          left: 20,
        },
        x: function (d) {
          return d.label;
        },
        y: function (d) {
          return d.value;
        },
        text: function (d) {
          return d.value;
        },
        showLabels: false,
        labelType: 'percent',
        donut: true,
        donutRatio: 0.81,
        legendPosition: 'right',
        legend: {
          margin: {
            top: 60,
          },
          updateState: false,
        },
        tooltip: {
          contentGenerator: (d) => {
            return `
              <table>
                <tbody>
                  <tr>
                  <td class="legend-color-guide">
                    <div style="background-color: ${d.color};"></div>
                  </td>
                  <td class="key">${d.data.tooltipLabel}</td>
                  <td class="value">${d.data.value} %</td>
                </tr>
                </tbody>
              </table>`;
          },
        },
      },
    };
  }

  public getPieChartData(data, diameter, width = 0, header = '') {
    return {
      critical: data['critical'],
      criticalColor: PIE_CHART_COLORS['critical'],
      moderate: data['moderate'],
      moderateColor: PIE_CHART_COLORS['moderate'],
      normal: data['normal'],
      normalColor: PIE_CHART_COLORS['normal'],
      diameter: diameter,
      width: width,
      header: header,
    };
  }
}
