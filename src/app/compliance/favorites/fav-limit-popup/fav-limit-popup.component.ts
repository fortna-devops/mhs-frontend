import { Component, OnInit, Inject } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-fav-limit-popup',
  templateUrl: './fav-limit-popup.component.html',
  styleUrls: ['./fav-limit-popup.component.scss']
})
export class FavLimitPopupComponent implements OnInit {
  selectedFavUrl: string;
  selectedFavTitle: string;

  constructor(
    private toasterService: ToasterService,
    public dialogRef: MatDialogRef<FavLimitPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  toggleFavorite(url, title) {
    this.selectedFavUrl = url;
    this.selectedFavTitle = title;
  }

  onSubmit(): void {
    const favObject = {
      url: this.selectedFavUrl,
      title: this.selectedFavTitle
    };

    this.dialogRef.close(favObject);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
