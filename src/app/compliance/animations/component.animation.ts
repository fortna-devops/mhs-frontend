import { trigger, transition, state, animate, style } from '@angular/animations';

export const componentAnimation = trigger('componentAnimation', [
  state(
    'zoomin',
    style({
      transform: 'scale(4)',
      // filter: 'invert(44%) sepia(90%) saturate(2860%) hue-rotate(165deg) brightness(100%) contrast(102%)'
      // filter: 'invert(46%) sepia(21%) saturate(993%) hue-rotate(182deg) brightness(91%) contrast(87%)',
      filter:
        'invert(54%) sepia(10%) saturate(553%) hue-rotate(180deg) brightness(89%) contrast(92%)',
      opacity: 0,
    })
  ),
  // state('zoomout', style({
  //   transform: 'scale(1)'
  // })),

  transition('* => zoomin', [animate('0.5s')]),
  // transition('* => zoomout', [
  //   animate('7s')
  // ]),
]);
