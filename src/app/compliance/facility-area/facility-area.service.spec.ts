import { TestBed, inject } from '@angular/core/testing';

import { FacilityAreaService } from './facility-area.service';

describe('FacilityAreaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FacilityAreaService]
    });
  });

  it('should be created', inject([FacilityAreaService], (service: FacilityAreaService) => {
    expect(service).toBeTruthy();
  }));
});
