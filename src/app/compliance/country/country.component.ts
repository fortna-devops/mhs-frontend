import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { ToasterService } from 'angular2-toaster';
import { Subscription, Observable, ReplaySubject } from 'rxjs';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { CountryService } from './country.service';
import { ComplianceService } from '../compliance.service';
import { FavoritesService } from '../favorites/favorites.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss'],
})
export class CountryComponent implements OnInit, OnDestroy {
  @ViewChild('mapContainer') mapContainer: ElementRef;

  public alertAssetList: {};
  public regions;
  private themeSubscription: Subscription;
  private routeSubscription: Subscription;
  public regionList = [];
  public assetsWithErrors = [];
  public country: string;
  public countryHealthInfo = {};
  public countryLabel: string;
  public countryHealthTitle: string = 'All Regions';
  private theme: string;
  // favorites part
  public href: string;
  public isFavorite = false;
  public showCountryHealth = true;
  private favoritesubscription: Subscription;

  public uploadFileInfo = {};
  layoutMap: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private topNavService: TopNavService,
    private toasterService: ToasterService,
    private leftMenuService: LeftMenuService,
    private countryService: CountryService,
    private complianceService: ComplianceService,
    private favoritesService: FavoritesService
  ) {}

  ngOnInit() {
    this.routeSubscription = this.activatedRoute.params.subscribe((params) => {
      // favorites part
      this.href = this.router.url;
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;

      const leftMenuData = this.leftMenuService.getLeftMenuData();
      this.countryLabel = params['country'].replace(/_/gi, ' ');
      this.country = params['country'];
      if (this.country.toLowerCase() === 'europe') {
        if(this.regionList.length > 1) {
          this.countryHealthTitle = 'All Countries';
          this.showCountryHealth = true;
        } else {
          this.showCountryHealth = false;
        }
      } else {
        if(this.regionList.length > 1) {
          this.countryHealthTitle = 'All Regions';
          this.showCountryHealth = true;
        } else {
          this.showCountryHealth = false;
        }
      }
      this.theme = this.topNavService.getTheme();
      this.setData(this.theme, leftMenuData, params['country']);

      const { overviewData, countryData } = this.countryService.getOverviewData(
        leftMenuData,
        params['country']
      );

      this.regionList = this.countryService.getRegions(overviewData, countryData);
      this.countryHealthInfo = this.complianceService.getLevelHealthInfo(this.regionList);
      console.log(this.countryHealthInfo)
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
      }
    });

    this.themeSubscription = this.topNavService.getThemeAsObservable().subscribe((theme) => {
      this.setData(theme, this.leftMenuService.getLeftMenuData(), this.country);
    });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }

  private setData(theme, leftMenuData, country) {
    const layoutMap = this.countryService.getLayoutMap(theme, country);
    this.setMapLayout(layoutMap, leftMenuData, country, theme);
  }

  private setMapLayout(layoutMap, menuData, country, theme) {
    const that = this;
    const getActiveRegionColor = this.complianceService.getActiveRegionColor(theme);
    const {
      facilities,
      activeRegions,
    } = this.countryService.getFacilities(menuData, country);

    this.layoutMap = layoutMap;
    d3.xml(layoutMap)
      .mimeType('image/svg+xml')
      .get(function (error, xml) {
        if (error) {
          that.toasterService.pop('error', '', `An error occurred. Please try again later`);
          return;
        }

        const tooltip = d3
          .select('body')
          .append('div')
          .attr('class', 'assettooltip')
          .style('opacity', 0);

        if (that.mapContainer.nativeElement['hasChildNodes']()) {
          that.mapContainer.nativeElement['removeChild'](
            that.mapContainer.nativeElement['lastChild']
          );
        }
        that.mapContainer.nativeElement['appendChild'](xml.documentElement);
        const svg = d3.select(that.mapContainer.nativeElement.children[0]);

        svg.selectAll('path, polygon').each(function (d, i) {
          if (d3.select(this).attr('class')) {
            if (activeRegions.indexOf(d3.select(this).attr('class')) > -1) {
              d3.select(this).style('cursor', 'pointer');
              d3.select(this).style('fill', () => getActiveRegionColor);
            }
            d3.select(this).on('click', function () {
              if (activeRegions.indexOf(d3.select(this).attr('class')) > -1) {
                that.router.navigate([`./${d3.select(this).attr('class')}`], {
                  relativeTo: that.activatedRoute,
                });
              }
            });
          }
        });

        svg.selectAll('circle').each(function (d, i) {
          if (activeRegions.indexOf(this['id'].split('@@')[0]) === -1) {
            return;
          }

          d3.select(this)
            .style('fill', () => {
              const facility = facilities.find(facility => {
                return facility.site === d3.select(this).attr('id').split('@@')[1]
              });              
              return facility ? facility.color.toLowerCase() : null;
            })
            .attr('r', '6')
            // .attr('stroke', 'gray')
            .on('click', function () {
              tooltip.transition().style('opacity', 0);

              const regionAndFacility = d3.select(this).attr('id').split('@@');
              const region = regionAndFacility[0];
              const facility = regionAndFacility[1];

              that.router.navigate([`./${region}/${facility}`], {
                relativeTo: that.activatedRoute,
              });
            })
            .on('mouseover', function () {
              d3.select(this).attr('stroke', 'gray');
              d3.select(this).attr('r', '7');
              d3.select(this).style('cursor', 'hand');

              const facilityName = d3.select(this).attr('id').split('@@')[1];
              const facility = facilities.find(facility => facility.site === facilityName);
              const assetCount = facility['num_yellow_assets'] + facility['num_green_assets'] + facility['num_red_assets'];
              const facilityHealth = Math.floor((facility['num_green_assets'] / assetCount) * 100);
 
              tooltip.transition().duration(300).style('opacity', 0.9).style('z-index', 400);
              tooltip
                .html(
                  `${facilityName} </br>
                    Health: ${!Number.isNaN(facilityHealth) && facilityHealth ? facilityHealth : '0'}%</br>
                    Severe Condition: ${facility['num_red_assets']} </br>
                    Moderate Condition: ${facility['num_yellow_assets']} </br>
                    Normal Condition: ${facility['num_green_assets']}`
                )
                .style('left', d3.event['pageX'] + 10 + 'px')
                .style('top', d3.event['pageY'] - 60 + 'px');
            })
            .on('mouseout', function () {
              d3.select(this).attr('stroke', 'none');
              d3.select(this).attr('r', '6');
              tooltip.transition().duration(500).style('opacity', 0).style('z-index', 0);
            });
        });
      });
  }

  public goToPage(page) {}
}
