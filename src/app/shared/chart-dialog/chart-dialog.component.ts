import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-chart-dialog',
  templateUrl: './chart-dialog.component.html',
  styleUrls: ['./chart-dialog.component.scss'],
})
export class ChartDialogComponent implements OnInit {
  public chartData: any = {
    options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };

  constructor(
    public dialogRef: MatDialogRef<ChartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.chartData['data'] = this.data.chartData
      ? JSON.parse(JSON.stringify(this.data.chartData))
      : [];
    this.chartData['options'] = this.data.options;
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }
}
