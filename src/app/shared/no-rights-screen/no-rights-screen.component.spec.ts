import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoRightsScreenComponent } from './no-rights-screen.component';

describe('NoRightsScreenComponent', () => {
  let component: NoRightsScreenComponent;
  let fixture: ComponentFixture<NoRightsScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoRightsScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoRightsScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
