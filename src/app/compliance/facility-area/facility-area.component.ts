import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { PRIORITY_COLORS } from '../../shared/mapping';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { ComplianceService } from '../compliance.service';
import { Subscription } from 'rxjs';
import { FavoritesService } from '../favorites/favorites.service';
import { FacilityAreaService } from './facility-area.service';
import { TimezoneService } from '../../shared/timezone/timezone.service';

const COLOR_STATUS_KEYS = {
  red: 3,
  yellow: 2,
  green: 1,
};

@Component({
  selector: 'app-facility-area',
  templateUrl: './facility-area.component.html',
  styleUrls: ['./facility-area.component.scss'],
})
export class FacilityAreaComponent implements OnInit, OnDestroy {
  public alertList: Array<any> = [];
  public modalMessage: string;
  public modalVisible = false;
  public asset: string;
  public title: string;
  public alertAssetList = {};
  public alarms: Array<any>;

  public healthInfo = {};

  // favorites part
  public href: string;
  public isFavorite = false;
  private favoritesubscription: Subscription;
  private routeSubscription: Subscription;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toasterService: ToasterService,
    private leftMenuService: LeftMenuService,
    private timezoneService: TimezoneService,
    private topNavService: TopNavService,
    private complianceService: ComplianceService,
    private facilityAreaService: FacilityAreaService,
    private favoritesService: FavoritesService
  ) {}

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe((paramList) => {
      // favorites part
      this.href = this.router.url;
      const favObj = this.favoritesService.checkIsFavorite(this.href);
      this.isFavorite = favObj.favorite;
      this.timezoneService.setSite(paramList['site'])
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const levels = this.leftMenuService.getLeftMenuLevels();
      const assets = this.getAssets(leftMenuData, levels, paramList);
      const levelName = paramList['facilityArea'].replace('_', ' ');
      this.healthInfo = this.getLevelHealthInfo(assets, levelName)
      let title = this.getTitle(paramList);
      this.title = title.charAt(0).toUpperCase() + title.slice(1);
      this.alertList = this.getAlerts(assets);
    });

    this.favoritesubscription = this.favoritesService.favoritesLoaded$.subscribe((status) => {
      if (status) {
        const favObj = this.favoritesService.checkIsFavorite(this.href);
        this.isFavorite = favObj.favorite;
      }
    });
  }

  public getLevelHealthInfo(childrenList, levelName) {
    const level = {
      num_red_assets: 0,
      num_yellow_assets: 0,
      num_green_assets: 0,
    };
    childrenList.map((child) => {
      level[`num_${child.color.toLowerCase()}_assets`] += 1;
      return child;
    });
    level['normal'] = this.getHealthPercent(level);
    level['color'] = this.getHealthColor(level);
    level['class'] = `health-${level['color']}`;
    level['name'] = levelName;
    return level;
  }

  public getHealthPercent(item) {
    const assetCount = item['num_red_assets'] + item['num_yellow_assets'] + item['num_green_assets'];
    return Math.floor((item['num_green_assets'] / assetCount) * 100);
  }

  public getHealthColor(item) {
    if (item['num_red_assets'] > 0) {
      return PRIORITY_COLORS['3'];
    } else if (item['num_yellow_assets'] > 0) {
      return PRIORITY_COLORS['2'];
    }
    return PRIORITY_COLORS['1'];
  }

  private getTitle(params) {
    const leftMenu = this.leftMenuService.getLeftMenuData();

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const name = leftMenu['facilities']
        .filter((item) => item['site'] === params['site'])
        .map((item) => item.name)[0];

      return `${name.replace(/_/gi, ' ')}`;
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      let result: string;

      leftMenu['countries'].forEach((country) => {
        country['regions'].forEach((region) => {
          region['facilities'].forEach((item) => {
            if (item['site'] === params['site']) {
              result = item.name.replace(/_/gi, ' ');
            }
          });
        });
      });

      return `${result} > <span class="facility-area">${params['facilityArea'].replace(/_/gi, ' ')}</span>`;
    }
  }

  private getAlerts(assets) {
    const alertList = [];
    assets.forEach((item) => {
      // HOT FIX
      const color = PRIORITY_COLORS[item['color']] ? PRIORITY_COLORS[item['color']] : item['color'];
      alertList.push({
        assetName: item['name'],
        assetId: item['id'],
        color: color,
        icon: `./assets/images/compliance/icons/asset-${color.toLowerCase()}.svg`,
      });
    });

    alertList.sort((a, b) => {
      return COLOR_STATUS_KEYS[b.color] - COLOR_STATUS_KEYS[a.color];
    });

    debugger
    return alertList;
  }

  private getAssets(menuResult, leves, paramList) {
    const result = [];

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      menuResult['facilities']
        .filter((item) => item['site'] === paramList['site'])
        .forEach((facility) => {
          facility['facility_areas'].forEach((facilityArea) => {
            if (decodeURI(paramList['facilityArea']) === facilityArea['name']) {
              result.push(...facilityArea['assets']);
            }
          });
        });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      menuResult['countries']
        .filter(
          (country) =>
            country['name'].replace(/\s/gi, '_') === paramList['country'].replace(/\s/gi, '_')
        )
        .forEach((country) => {
          country['regions']
            .filter(
              (region) =>
              region['name'].replace(/\s/gi, '_') ===
                paramList['region'].replace(/\s/gi, '_')
            )
            .forEach((region) => {
              region['facilities']
                .filter((facility) => facility['site'] === paramList['site'])
                .forEach((facility) => {
                  facility['facility_areas'].forEach((facilityArea) => {
                    if (
                      facilityArea['name'].replace(/\s/gi, '_') ===
                      paramList['facilityArea'].replace(/\s/gi, '_')
                    ) {
                      result.push(...facilityArea['assets']);
                    }
                  });
                });
            });
        });
    }

    return result;
  }

  // TODO: refactor
  private getRegion(leftMenuData, assetName) {
    let activeRegion: string;

    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      leftMenuData['facilities'].forEach((facility) => {
        facility['facility_areas'].forEach((facility_area) => {
          facility_area['assets'].forEach((asset) => {
            if (asset['name'] === assetName) {
              activeRegion = facility_area['name'];
            }
          });
        });
      });
    }

    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      leftMenuData['countries'].forEach((country) => {
        country['regions'].forEach((mainRegion) => {
          mainRegion['facilities'].forEach((facility) => {
            facility['facility_areas'].forEach((facility_area) => {
              facility_area['assets'].forEach((asset) => {
                if (asset['name'] === assetName) {
                  activeRegion = facility_area['name'];
                }
              });
            });
          });
        });
      });
    }

    return activeRegion;
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.favoritesubscription.unsubscribe();
  }
}
