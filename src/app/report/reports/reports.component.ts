import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService } from '../report.service';
import * as moment from 'moment';
import { ComplianceService } from '../../compliance/compliance.service';
import { AssetService } from '../../compliance/asset/asset.service';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { PRIORITY_COLORS, PRIORITY_COLORS_HEX } from '../../shared/mapping';
import { FacilityOverviewService } from '../../compliance/facility/facility-overview/facility-overview.service';
import * as svgToPng from 'save-svg-as-png';

import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { LeftMenuService } from '../../left-menu/left-menu.service';
import { jsPDF } from 'jspdf';
import { ReportGenerateService } from '../report-generate.sevice';

import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { PromptDialogComponent } from '../../shared/prompt-dialog/prompt-dialog.component';
import { concat, from, of, pipe } from 'rxjs';
import { concatMap, map } from 'rxjs/operators';
import { flatten } from '@angular/compiler';
import { TimezoneService } from '../../shared/timezone/timezone.service';

const PERIOD_TYPES = [
  {
    name: 'Last 24 Hrs',
    value: 'Last24Hrs',
    days: '1',
  },
  {
    name: 'Last 3 Days',
    value: 'lastThreeDays',
    days: '3',
  },
  {
    name: 'Last 5 Days',
    value: 'lastFiveDays',
    days: '5',
  },
  {
    name: 'Last Week',
    value: 'lastWeek',
    days: '7',
  },
  {
    name: 'Last 2 Weeks',
    value: 'lastTwoWeeks',
    days: '14',
  }
];

const MINUTES = [
  {
    name: '6 hr.',
    data: { value: 360, type: 'minute' },
  },
  {
    name: '12 hr.',
    data: { value: 720, type: 'minute' },
  },
  {
    name: '7 days',
    data: { value: 10080, type: 'hour' },
  },
];

const ML_TYPES = ['ml_spike', 'ml_persistent '];

const FACTORY_FILTER_SETTINGS = {
  analyticTypes: {
    iso_persistent: true,
    ml_spike: false,
    ml_persistent: false,
    iso_spike: false,
  },
  eventLevels: {
    YELLOW: false,
    RED: true,
  },
};

const CONDITION_TYPES = {
  YELLOW: 'Potential Failures',
  RED: 'Functional Failures',
};

const ACTION_TYPES = {
  open: 'open',
  download: 'download',
};

const METRIC_TYPE = 'iso_persistent';

const ERROR_STATUS = 'error';
const SUCCESS_STATUS = 'success';
const PDF_GENERATING = "Generating PDF... Please don't close the page";
const PDF_GENERATED_SUCCESSFULLY = 'PDF was generated successfully. Download will start shortly';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
})
export class ReportsComponent implements OnInit {
  public Editor = ClassicEditor;

  public periodTypes: Array<any> = [];
  public activePeriodType = 'lastWeek';
  public minutes = MINUTES;
  public rangeBeforeAndAfterEvent = { value: 10080, type: 'hour' };
  public period = { value: 10080, type: 'hour' };
  public site: string;
  public siteTitle: string;
  private factoryFilterSettings = FACTORY_FILTER_SETTINGS;

  public startD: any;
  public endD: any;
  public rangeType: any = 'hour';

  private openDialogRef: any;

  public activeCustomerName = '';
  public assetSelectedList = {};
  public allCriticalityLevelSelected = false;
  public indeterminateCriticalityLevel = false;
  public allEventLevelsSelected = false;
  public indeterminateEventLevels = false;
  public allEventTypesSelected = false;
  public indeterminateEventTypes = false;
  public allAnalyticTypesSelected = false;
  public indeterminateAnalyticTypes = false;

  public conditionTypes = CONDITION_TYPES;
  public actionTypes = ACTION_TYPES;

  public analyticTypes = [
    {
      id: 'iso_persistent',
      val: 'Consistently Over Threshold',
    },
    {
      id: 'ml_spike',
      val: 'Anomalous Spikes',
    },
    {
      id: 'ml_persistent',
      val: 'Consistently Anomalous',
    },
    {
      id: 'iso_spike',
      val: 'Spike Over Threshold',
    },
  ];

  public eventTypes = [];
  public metricList = [];

  public eventLevels = [
    {
      id: 'RED',
      val: 'Functional Failure',
    },
    {
      id: 'YELLOW',
      val: 'Potential Failure',
    },
  ];

  public criticalityLevels = [
    {
      id: 'low',
      val: 'low',
    },
    {
      id: 'medium',
      val: 'medium',
    },
    {
      id: 'high',
      val: 'high',
    },
  ];

  private filterSettings = FACTORY_FILTER_SETTINGS;

  public showScroll: boolean;
  public assets: Array<any> = [];
  public assetIndexArr: Array<any> = [];
  private pdfTitleLines = [];

  public sensorChart1: any = {
    d3Options: '',
    data: [],
    title: '',
    trend: '',
    loading: false,
  };
  alarmsList: any;

  public alarmGroupSelected = false;
  public devidedMetricList = [];
  public alarmedMetricList = [];
  public pages = [];
  public metricsPerPage = 15;
  public lookupDays = 7;
  public offset = 0;
  public metricCount = 0;
  public pageIsLoading = false;
  page: number = 1;
  pageIndex: number = 0;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private reportService: ReportService,
    private complianceService: ComplianceService,
    private assetService: AssetService,
    private timezoneService: TimezoneService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private topNavService: TopNavService,
    private facilityOverviewService: FacilityOverviewService,
    private toasterService: ToasterService,
    private reportGenerateService: ReportGenerateService,
    private leftMenuService: LeftMenuService
  ) {
    iconRegistry.addSvgIcon(
      'red-circle',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/red-circle.svg')
    );
    iconRegistry.addSvgIcon(
      'yellow-circle',
      sanitizer.bypassSecurityTrustResourceUrl('assets/images/compliance/icons/yellow-circle.svg')
    );
  }

  ngOnInit() {
    this.periodTypes = this.getPeriodType();
    const lookupDays = this.periodTypes.find(item => item.value === this.activePeriodType)
    this.lookupDays = lookupDays ? lookupDays.days : 7;

    this.activeCustomerName = this.topNavService.getActiveCustomerName();
    this.activatedRoute.params.subscribe((urlParams) => {
      this.site = urlParams['site'];
      this.timezoneService.setSite(urlParams['site'])
      this.assets = [];
      this.assetSelectedList = {};
      this.alarmedMetricList = [];
      this.pages = [];
      this.changePage(0);
      const siteTitle = this.facilityOverviewService.getTitle({ site: urlParams['site'] });
      this.siteTitle = this.capitalize(siteTitle.replace(/_/g, ' '));
      this.getFilters()
    });

   if (this.checkTouchDevice()) {
      this.showScroll = false;
   } else {
      this.showScroll = true;
   }
  }

  private getFilters(refreshAlarms = false) {
    this.pageIsLoading = true;

    const apiParams = {
      site: this.site,
      period: this.lookupDays
    };

    this.reportService.getFilters(apiParams).subscribe(
      (result) => {
        this.eventTypes = [];
        const metrics = result['metrics'];
        const eventTypes = {};
        metrics.map((metric) => {
          this.eventTypes.push({
            id: metric['display_name'],
            val: metric['display_name'],
            halfList: true
          });
          eventTypes[metric['display_name']] = true;
        });
        this.factoryFilterSettings['eventTypes'] = eventTypes;
        this.resetToFactory();
        this.getPages();
      });
  }

  private getPages() {
    this.assets = [];
    this.assetSelectedList = {};
    this.pageIsLoading = true;

    const apiParams = {
      site: this.site,
      period: this.lookupDays,
      display_names: this.setFilters('eventTypes'),
      alarm_type_names: this.setFilters('analyticTypes'),
      alarm_colors: this.setFilters('eventLevels'),
    };

    this.reportService.getMetricCount(apiParams).subscribe(
      (result) => {
        const count = result['count']
        this.metricCount = count;
        this.offset = 0;
        this.getMetrics(true);
      });
  }

  private getMetrics(refreshAlarms = false) { 
    this.offset = this.pageIndex * this.metricsPerPage;
    this.pageIsLoading = true;

    const apiParams = {
      site: this.site,
      period: this.lookupDays,
      limit: this.metricsPerPage,
      offset: this.offset,
      display_names: this.setFilters('eventTypes'),
      alarm_type_names: this.setFilters('analyticTypes'),
      alarm_colors: this.setFilters('eventLevels'),
    };

    this.reportService.getMetricList(apiParams).subscribe(
      (result) => {
        const metrics = result['metrics'];
        const metricIds = metrics.map((item) => item.id);
        this.alarmedMetricList = metricIds;
        if(this.alarmedMetricList.length > 0) {
          this.getAlarms(refreshAlarms);
        } else {
          this.assets = [];
          this.assetSelectedList = {};
          this.pageIsLoading = false;
        }
      });
  }

  private checkTouchDevice() {
    return 'ontouchstart' in document.documentElement;
 }

  setFilters(filterName) {
    const settings = this.filterSettings;
    if (settings.hasOwnProperty(filterName)) {
      const filtered = Object.keys(settings[filterName]).filter(function (key) {
        return settings[filterName][key];
      });

      return filtered
        .map(function (item) {
          return item;
        })
        .toString();
    }
    return '';
  }

  public  getAlarms(refresh = false) {

    if(!refresh && this.assets[this.pageIndex])  {
      this.pageIsLoading = false;
      return
    } else if(refresh) {
      this.assets = [];
      this.assetSelectedList = {};
    }

    const offset = this.timezoneService.getTimezoneOffset();
    const { startDdef, endDdef } = this.getDateByType(); 
    let {startD, endD} = this.timezoneService.getDateRangeInUTC({startD: startDdef, endD: endDdef}, offset)

    this.alarmsList = [];
    const needDataArr = [];
    const defaultApiParams = {
      site: this.site,
      startDate: startD,
      endDate: endD,
      colors: this.setFilters('eventLevels'), // Event Level
      types: this.setFilters('analyticTypes'), // Analytic Type
      metric_ids: this.alarmedMetricList.toString(), // Event Type
    }; 

    this.reportService
        .getAlarmsCount({ 
          ...defaultApiParams,
          count: true 
        }).subscribe((res) => {
          // sending alarms request using set limit so the request won't fail
          let limit = 300;
          let parts = 0;
          let diff = +res['count'] / limit;
          parts = diff > 0 && diff < 1 ? 1 : diff;

          const apiParams = {
            ...defaultApiParams,
            limit: limit,
            offset: 0
          };

          this.reportService
          .getAlarms(apiParams, parts).subscribe((result) => {
            const alarms = flatten(result.map((item) => item['alarms']))
            this.alarmsList.push(alarms);
        
            for (const alarmGroup of alarms) { 
              alarmGroup['loading'] = true;
              alarmGroup['metricId'] = alarmGroup['metric']['id'];
              alarmGroup['assetId'] = alarmGroup['asset']['id'];
              alarmGroup['checked'] = false;
              alarmGroup['comment'] = '';
              alarmGroup['description'] = alarmGroup[METRIC_TYPE][alarmGroup[METRIC_TYPE].length - 1].description;
              alarmGroup['list'] = alarmGroup[METRIC_TYPE];
              alarmGroup['color'] = alarmGroup[METRIC_TYPE][alarmGroup[METRIC_TYPE].length - 1].color;
              alarmGroup['class'] = `circle circle--${alarmGroup['color'].toLowerCase()}`;
              alarmGroup['chartId'] = this.setChartId( 
                alarmGroup['metricId']
              );
    
              // IF asset is in the list, then add data to this asset
              const foundIndex = this.assets[this.pageIndex] ? this.assets[this.pageIndex].findIndex((element) => element.asset['id'] == alarmGroup['assetId']) : null;
              if (foundIndex >= 0 && foundIndex !== null) {
                this.assets[this.pageIndex][foundIndex]['alarmGroups'].push(alarmGroup);
              } else {
                // IF asset is not in the list, than add new asset object
                const obj = {
                  asset: alarmGroup.asset,
                  alarmGroups: [alarmGroup],
                };
                if(!this.assets[this.pageIndex]) this.assets[this.pageIndex] = [];
                this.assets[this.pageIndex].push(obj);
              }
    
              const foundAsset = this.assetSelectedList[alarmGroup['assetId']];
    
              if (foundAsset) {
                this.assetSelectedList[alarmGroup['assetId']]['metrics'][alarmGroup['metricId']] = alarmGroup;
              } else {
                // IF asset is not in the list, than add new asset object
                this.assetSelectedList[alarmGroup['assetId']] = {
                  value: alarmGroup.asset,
                  metrics: {},
                  index: this.assets.length - 1,
                }
                this.assetSelectedList[alarmGroup['assetId']]['metrics'][alarmGroup['metricId']] = alarmGroup;
              }
    
              // ! TODO VFD ALARMS
              // if (alarm['type'] === 'WARNING' || alarm['type'] === 'ERROR') {
              //   alarm['VFDChartError'] =
              //     'This alarm was calculated by the VFD. There are presently no data to display.';
              //   alarm['loading'] = false;
              //   continue;
              // }
    
              // filter options if metric is in list to reduce count of requests
              const dataObj = {
                metricId: alarmGroup['metricId'],
                metric_ids: [alarmGroup['metricId']],
                chartId: alarmGroup['chartId'],
              };
    
              const foundObj = needDataArr.find((item, i) => {
                return (
                  item.metric_ids.indexOf(alarmGroup['metricId'])
                );
              });
    
              if (!foundObj) {
                needDataArr.push(dataObj);
              } else {
                // if metric is not in the metrics list
                foundObj.metric_ids.push(alarmGroup['metricId']);
              }
            }
            needDataArr.forEach((dataObj) => {
              this.getChartsData(dataObj, apiParams);
            });
    
            this.pageIsLoading = false;
          });
        });
  }

  public compareObjects(o1: any, o2: any): boolean {
    return o1.value === o2.value && o1.type === o2.type;
  }

  private getChartsData(dataObj, params) {
    // TODO: check
    const metricChartList = [];

    const apiParams = {
      site: this.site,
      metric_ids: dataObj.metric_ids.join(), 
      startDate: params.startDate,
      endDate: params.endDate,
      sample_rate: this.rangeType,
    };

    this.reportService.getChartData(apiParams).subscribe((response) => {
      const responseList = response['data'];
      responseList.forEach((item) => {
          const resultData = this.setChartData(item, item['metric_id']);
          const decimalPlaceObj = resultData['meta'].find((item) => item['decimal_places']);
          const result = {
            metricId: item['metric_id'],
            data: resultData['data'],
            options: this.assetService.getReportChartOptions({
              xValue: dataObj.data_timestamp,
              chartId: this.setChartId(item['metric_id']),
              criteria: dataObj.criteria,
              isAlarmed: resultData['isAlarmed'], 
              meta: this.getMaxMinValues(resultData['meta']),
              decimalPlace: decimalPlaceObj ? decimalPlaceObj['decimal_places'] : null 
            }),
          };
          metricChartList.push(result);
      });

      this.assets[this.pageIndex].map((asset) => {
        return asset.alarmGroups.map((group, index) => {
            if(!group['chartData']) {
              group['chartData'] = {};
              const data = metricChartList.find(
                (metricData) =>
                  metricData.metricId === group['metricId']
              );
              
              Object.assign(group['chartData'], {}, data);
              group['units'] = group['metric']['units'],
              group['loading'] = false;
            }
          return group;
        })
      }
      );
    });
  }

  private getMaxMinValues(meta) {
    const min = Math.min.apply(
      Math,
      meta.map(function (o) {
        return o.min_value;
      })
    );
    const max = Math.max.apply(
      Math,
      meta.map(function (o) {
        return o.max_value;
      })
    );
    return {max, min}
  }

  setAlarmsOnChart(alarms, data, meta){
    const itemList = [[],[]];
    const thresholdList = [];
    const result = [];
    const maxValue = meta['max_value'];
    const minValue = meta['min_value'];
    let minThreshold = null;
    let maxThreshold = null;

    const thresholds = alarms.map(item => item.trigger_criteria)

    if(thresholds && thresholds.length > 1) {
      minThreshold = Math.min.apply(
        Math,
        thresholds.map(function (o) {
          return o;
        })
      );
      maxThreshold = Math.max.apply(
        Math,
        thresholds.map(function (o) {
          return o;
        })
      );
    } else if(thresholds.length == 1){
      minThreshold = thresholds[0];
      maxThreshold = thresholds[0];
    }

    const minY = minThreshold && minThreshold < minValue ? minThreshold : minValue;
    const maxY = maxThreshold && maxThreshold > maxValue ? maxThreshold : maxValue;

    let index = 0;
    alarms.forEach((alarm) => {
      if(alarm.color.toLowerCase() == 'red') index = 0
      if(alarm.color.toLowerCase() == 'yellow') index = 1
      itemList[index].push({ x: alarm.started_at, y: minY * 0.99 });
      itemList[index].push({ x: alarm.started_at, y: maxY });
      itemList[index].push({ x: alarm.ended_at, y: maxY });
      itemList[index].push({ x: alarm.ended_at, y: minY * 0.99 });
      // debugger
      if (thresholdList.length == 0 || thresholdList.findIndex(i => i.thresholdValue === alarm.trigger_criteria) < 0) {
        thresholdList.push(this.setThresholds(data[0].x, data[data.length - 1].x, alarm.trigger_criteria, alarm.color))
      }
    });

    const { name, color, classed } = this.getComponentChartSettings('peak');
    itemList.map((list, index) => {
      if(list.length !== 0) {
        result.push({
          values: list,
          key: name,
          name: name,
          area: true,
          // metricName: alarms[0]['metric']['display_name'],
          // metricId: alarms[0]['metric']['id'],
          color: index === 0 ? PRIORITY_COLORS_HEX['red'] : PRIORITY_COLORS_HEX['yellow'],
          classed: classed,
        });
      }
    })
    result.push(...thresholdList)
    return {data: result, meta: {max_value: maxY, min_value: minY}};
  }

  

  setChartId(metric) {
    return `metric${metric}`;
  }

  private setThresholds(start, end, criteria, color) {
    // debugger    
    return {
      values: [
        { x: start, y: criteria },
        { x: end, y: criteria },
      ],
      thresholdValue: criteria,
      key: 'Threshold ' + color,
      name: 'Threshold',
      color: PRIORITY_COLORS_HEX[color.toLowerCase()],
      classed: 'dashed stroke-width-one',
    };
  }

  private setChartData(item, metricId) {
    const detailData = [];

    const { name, color, classed } = this.complianceService.getAlarmChartSettings('measured');

    detailData.push({
      values: item['data'].sort(this.complianceService.dateCompare),
      key: name,
      name: 'measured',
      color: color,
      classed: classed,
    });

      // debugger
    // one element
    const alarmList = flatten(
      this.assets[this.pageIndex].map((asset) => {
        return asset.alarmGroups.map((group) => {
          return group;
        });
      })
    )

    const meta = [item['meta']];

    const activeAlarmGroup = alarmList ? alarmList.find((group) => metricId == group['metric']['id']    ) : null; 
    let isAlarmed = false;

    if (activeAlarmGroup) {
      isAlarmed = true;
      const activeAlarms = activeAlarmGroup[METRIC_TYPE].filter(alarm => alarm['type_name'].toUpperCase() !== 'WARNING'&& alarm['type_name'].toUpperCase() !== 'ERROR' )
      const alarmsData = this.setAlarmsOnChart(activeAlarms, item['data'], item['meta']);
      meta.push(alarmsData.meta)
      detailData.push(...alarmsData['data']);
    }

    return {data: detailData, meta, isAlarmed: isAlarmed};
  }

  public getPeriodType() {
    return PERIOD_TYPES.map((item) => {
      return {
        value: item['value'],
        days: item['days'],
        label: item['name'],
        checked: true,
      };
    });
  }

  public applyFilters() {
    this.getPages();
    this.changePage(0);
  }

  public changePeriod(type) {
    this.activePeriodType = type;
    const lookupDays = this.periodTypes.find(item => item.value === this.activePeriodType)
    this.lookupDays = lookupDays ? lookupDays.days : 7;
    this.changePage(0);
    this.getPages();
  }


  public resetToFactory() {
    this.filterSettings = JSON.parse(JSON.stringify(this.factoryFilterSettings));
    Object.keys(this.filterSettings).map(listKey => {
      const totalSelected = Object.keys(this.filterSettings[listKey]).filter((key) => this.filterSettings[listKey][key]).length;
      const checkedFilter = this.setCheckedFilter(totalSelected, this.filterSettings[listKey]);
      const capitalized = listKey.charAt(0).toUpperCase() + listKey.slice(1);
      const nameAll = `all${capitalized}Selected`
      const nameInd = `indeterminate${capitalized}`
      this[nameAll] = checkedFilter.allSelected;
      this[nameInd] = checkedFilter.indeterminate;
    })
  }

  public clearAll() {
    for (const type in this.filterSettings) {
      if (this.filterSettings.hasOwnProperty(type)) {
        for (const property in this.filterSettings[type]) {
          if (this.filterSettings[type].hasOwnProperty(property)) {
            this.filterSettings[type][property] = false;
          }
        }
      }
    }
    this.allAnalyticTypesSelected = false;
    this.indeterminateAnalyticTypes = false;
    this.allCriticalityLevelSelected = false;
    this.indeterminateCriticalityLevel = false;
    this.allEventLevelsSelected = false;
    this.indeterminateEventLevels = false;
    this.allEventTypesSelected = false;
    this.indeterminateEventTypes = false;
  }

  public itemChanged(item, event, list, type) {
    this.filterSettings[type][item.id] = event.checked;

    const totalSelected = Object.keys(list).filter((key) => list[key]).length;
     
    switch (type) {
      case 'eventLevels':
        const eventLevels = this.setCheckedFilter(totalSelected, list);
        this.allEventLevelsSelected = eventLevels.allSelected;
        this.indeterminateEventLevels = eventLevels.indeterminate;
        break;
      case 'eventTypes':
        const eventTypes = this.setCheckedFilter(totalSelected, list);
        this.allEventTypesSelected = eventTypes.allSelected;
        this.indeterminateEventTypes = eventTypes.indeterminate;
        break;
      case 'analyticTypes':
        const analyticTypes = this.setCheckedFilter(totalSelected, list);
        this.allAnalyticTypesSelected = analyticTypes.allSelected;
        this.indeterminateAnalyticTypes = analyticTypes.indeterminate;
        break;
    }
  }

  public getDateByType() {
    // item.isChecked = event.checked;
    switch (this.activePeriodType) {
      case 'Last24Hrs':
        return {
          startDdef: moment().toISOString(),
          endDdef: moment().toISOString(),
        };
      case 'lastThreeDays':
        return {
          startDdef: moment().subtract('2', 'days').toISOString(),
          endDdef: moment().toISOString(),
        };
      case 'lastFiveDays':
        return {
          startDdef: moment().subtract('4', 'days').toISOString(),
          endDdef: moment().toISOString(),
        };
      case 'lastWeek':
        return {
          startDdef: moment().subtract('6', 'days').toISOString(),
          endDdef: moment().toISOString(),
        };
      case 'lastTwoWeeks':
        return {
          startDdef: moment().subtract('13', 'days').toISOString(),
          endDdef: moment().toISOString(),
        };
      default:
        return {
          startDdef: moment().subtract('6', 'days').toISOString(),
          endDdef: moment().toISOString(),
        };
    }
  }

  private setCheckedFilter(totalSelected, list) {
    let allSelected: any;
    let indeterminate: any;
    const totalCount = Object.keys(list).length;

    if (totalSelected === 0) {
      allSelected = false;
      indeterminate = false;
    } else if (totalSelected > 0 && totalSelected < totalCount) {
      allSelected = false;
      indeterminate = true;
    } else if (totalSelected == totalCount) {
      allSelected = true;
      indeterminate = false;
    }

    return { allSelected, indeterminate };
  }

  public toggleSelectAll(event, type) {
    for (const property in this.filterSettings[type]) {
      if (this.filterSettings[type].hasOwnProperty(property)) {
        this.filterSettings[type][property] = event.checked;
      }
    }

    switch (type) {
      case 'eventLevels':
        this.allEventLevelsSelected = event.checked;
        break;
      case 'eventTypes':
        this.allEventTypesSelected = event.checked;
        break;
      case 'analyticTypes':
        this.allAnalyticTypesSelected = event.checked;
        break;
    }
  }

  public changePage(index) {
    this.page = index + 1 ;
    this.pageIndex = index;
  }

  public onChangePage(page) { 
    const checkedGroups = this.filterCheckedGroupsFromPage();
    const foundNotLoaded = checkedGroups.find(group => group.loading)

    if (foundNotLoaded) {
      const dialogRef = this.dialog.open(PromptDialogComponent, {
        width: '40%',
        panelClass: this.topNavService.getTheme(),
        data: {text: 'Charts are being generated for some assets. \n They may be lost after going to another page. Do you want to continue without them?', answer: null}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          checkedGroups.map(metric => {
            metric['svg'] = d3.select('#' + metric['chartId'] + ' svg').node().cloneNode(true);
            return metric
          });
          this.page = page;
          this.pageIndex = page - 1;
          this.getMetrics()
        } else {
          this.page = page - 1;
        }
      });
    } else {
      checkedGroups.map(metric => {
        metric['svg'] = d3.select('#' + metric['chartId'] + ' svg').node().cloneNode(true);
        return metric
      });
      this.page = page;
      this.pageIndex = page - 1;
      this.getMetrics()
    }
  }

  private filterCheckedGroupsFromPage() {
    const assets = []
    const pageMetrics = flatten(this.assets[this.pageIndex].map(asset => asset.alarmGroups))
    const metrics = flatten(Object.keys(this.assetSelectedList).map(
      (key) => {
        const groups = [];
        const obj = {};
        Object.keys(this.assetSelectedList[key].metrics).filter(
          (metricKey) => {
            const foundMetric = pageMetrics.find(metric => metric['metricId'] == metricKey);
            if(foundMetric && this.assetSelectedList[key].metrics[metricKey].checked 
              ) {
                groups.push(this.assetSelectedList[key].metrics[metricKey])
            }
          }
        )
        return groups;
      }
    )).filter(item => item);
    return metrics
  }



  // PDF GENERATION START

  public generateSelectedData(action = 'download') {
    const checkedKeys = flatten(Object.keys(this.assetSelectedList).map(
      (key) => 
        Object.keys(this.assetSelectedList[key].metrics).filter(
          (metricKey) => this.assetSelectedList[key].metrics[metricKey].checked
        )
      ));
    const checkedGroups = this.filterCheckedGroupsFromPage();
    const foundNotLoaded = checkedGroups.filter(group => group.loading)
    checkedGroups.map(metric => {
      metric['svg'] = d3.select('#' + metric['chartId'] + ' svg').node().cloneNode(true);
      return metric
    });

    if (checkedKeys.length == 0 ) {
      this.toasterService.pop(ERROR_STATUS, ``, 'Please select at least one event to include in the report');
      return;
    }

    if (foundNotLoaded.length > 0 ) {
      const dialogRef = this.dialog.open(PromptDialogComponent, {
        width: '40%',
        panelClass: this.topNavService.getTheme(),
        data: {text: 'Charts are being generated for some assets. \n Do you want to continue without them?', answer: null}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.generatePDF(action);
        }
      });
    } else {
      this.generatePDF(action);
    }

  }

  private generatePDF (action = 'download') {
    this.pdfTitleLines = [];

    this.toasterService.pop(SUCCESS_STATUS, ``, PDF_GENERATING);
    const pdf = new jsPDF();
    const date = moment(new Date()).format('MMM Do, YYYY, H:mm');
    this.pdfTitleLines.push({
      text: `${this.siteTitle} Events Report`,
      fontType: 'bold',
      align: 'center',
      fontSize: 20,
      marginBottom: 20,
    });

    const assetList = Object.keys(this.assetSelectedList);
    const alarmAll = this.filterCheckedAndLoadedGroups()
    const { rows, promises } = this.getAlarmsLines(alarmAll);

    Promise.all(promises).then(() => {
      switch (action) {
        case 'download':
          this.downloadPDF(rows, pdf, date);
          break;
        case 'open':
          this.openPDF(rows, pdf, date);
          break;
      }
    });
  }

  private filterCheckedAndLoadedGroups(loading = false) {
    const assets = []
    const checkedAssets = flatten(Object.keys(this.assetSelectedList).map(
      (key) => {
        const groups = [];
        const obj = {};
        Object.keys(this.assetSelectedList[key].metrics).filter(
          (metricKey) => {
            if(this.assetSelectedList[key].metrics[metricKey].checked 
              && this.assetSelectedList[key].metrics[metricKey].loading == loading) {
                groups.push(this.assetSelectedList[key].metrics[metricKey])
            }
          }
        )
        obj['alarmGroups'] = groups;
        obj['value'] = this.assetSelectedList[key]['value']
        return groups.length > 0 ? obj : null;
      }
    )).filter(item => item);
    return checkedAssets
  }

  public changeSelected(assetId, metricId, chartId) {
    const checkedGroups = this.filterCheckedAndLoadedGroups();
    this.assetSelectedList[assetId].metrics[metricId]['svg'] = d3.select('#' + chartId + ' svg').node().cloneNode(true);

    if (checkedGroups.length == 0 ) {
      this.alarmGroupSelected = false;  
    } else {
      this.alarmGroupSelected = true; 
    }
  }
  

  private capitalize(str) {
    const words = str.split(' ');

    for (let i = 0; i < words.length; i++) {
      words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    }

    return words.join(' ');
  }

  private openPDF(rows, pdf, date) {
    const document = this.reportGenerateService.getDocumentDefinition(rows, pdf, date);
    this.toasterService.pop(SUCCESS_STATUS, ``, PDF_GENERATED_SUCCESSFULLY);
    window.open(document.output('bloburl'));
  }

  private downloadPDF(rows, pdf, date) {
    const document = this.reportGenerateService.getDocumentDefinition(rows, pdf, date);
    this.toasterService.pop(SUCCESS_STATUS, ``, PDF_GENERATED_SUCCESSFULLY);
    // <><>><><>><>><><><><><>>><><<><><><><>
    // PRINT
    // <><>><><>><>><><><><><>>><><<><><><><>
    document.save(
      `${this.activeCustomerName}_${this.siteTitle}-Events_Report_${moment
        .utc(new Date())
        .format('MMM_Do_YYYY_H:mm')}.pdf`
    );
  }

  private getAlarmsLines(assets) {
    const rows = [];
    const promises = [];
    let tempAsset = null;
    let sectionNum = 1;
    rows.push(...this.pdfTitleLines);
    const alarms = flatten(assets.map(a => a['alarmGroups']));
    alarms.forEach((group: any) => {
      promises.push(
        this.getSvgToPng(group).then(function (arr: any) {
          if (!tempAsset || tempAsset['id'] !== group.asset['id']) {
            tempAsset = group.asset;
            rows.push({
              text: sectionNum + '. Asset: ' + group.asset['name'],
              align: 'left',
              fontType: 'bold',
              fontSize: 20 * 0.75,
              marginBottom: 10 * 0.75,
              section: sectionNum,
            });
            sectionNum += 1;
          }
          rows.push(...arr);
          return;
        })
      );
    });
    return { rows, promises };
  }

  private getSvgToPng(alarm) {
    const multipl = 0.7487922705314;
    const chart = {
      image: null,
      width: 668 * multipl,
      height: 300,
      align: 'left',
    };
    const svg = alarm.svg;
    const positionInfo = document.getElementsByClassName('line-chart-container')[0].getBoundingClientRect();
    const width = positionInfo.width;

    return new Promise((resolve, reject) => {
      svgToPng.svgAsPngUri(svg, {scale: 3, width: width}, (uri) => {
        chart.image = uri;
        const objArr = [
          {
            fontSize: 18 * 0.75,
            fontType: 'bold',
            align: 'left',
            color: PRIORITY_COLORS_HEX[alarm['color'].toLowerCase()],
            text: alarm.component['name'],
            subtitle: true,
            marginBottom: 5 * 0.75,
          },
          {
            fontSize: 15 * 0.75,
            fontType: 'normal',
            align: 'left',
            text: 'Last event: ' + alarm.description + '.',
          },
          chart,
          {
            fontSize: 16 * 0.75,
            lineHeight: 1.5,
            fontType: 'bold',
            align: 'left',
            text: alarm.comment.length > 0 ? 'Comment' : '',
          },
          {
            fontSize: 14 * 0.75,
            lineHeight: 1.5,
            fontType: 'normal',
            align: 'left',
            text: alarm.comment,
            marginBottom: 20 * 0.75,
          },
        ];
        resolve(objArr);
      });
    });
  }

  // PDF GENERATION END

  public goToHomePage() {
    if (this.leftMenuService.getLeftMenuLevels() === 4) {
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const country = leftMenuData['countries'][0]['name'].replace(/\s/gi, '_');
      this.router.navigate([`./MHS/${country}`]);
    }
    if (this.leftMenuService.getLeftMenuLevels() === 2) {
      const leftMenuData = this.leftMenuService.getLeftMenuData();
      const site = leftMenuData['facilities'][0]['site'].replace(/\s/gi, '_');
      this.router.navigate([`./MHS/${site}`]);
    }
  }

  private getComponentChartSettings(chartType) {
    let name = '';
    let color = '';
    let classed = '';

    switch (chartType) {
      case 'measured':
        name = 'Measured';
        color = '#1897F2';
        classed = 'stroke-width-two';
        break;
      case 'predicted':
        name = 'Expected';
        color = '#9AD5FF';
        classed = 'dashed stroke-width-two';
        break;
      case 'red':
        name = 'Threshold Red';
        color = '#FF5252';
        classed = 'dashed stroke-width-one';
        break;
      case 'yellow':
        name = 'Threshold Yellow';
        color = '#FFCD0B';
        classed = 'dashed stroke-width-one';
        break;
      case 'peak':
        name = 'Alarm peak';
        color = '#D7292E';
        classed = 'stroke-width-zero';
        break;
    }

    return {
      name,
      color,
      classed,
    };
  }
}
