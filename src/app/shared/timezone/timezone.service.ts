import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { TokenService } from '../token.service';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TimezoneService {
  private timezoneOffsetList = {};
  private timezoneOffset = 0;
  private timezone = 'utc';
  private site:any;
  params$: Observable<any>;
  
  constructor(private http: HttpClient, private tokenService: TokenService) { 
    const cookieTimezone = this.tokenService.getTimezone()
    if(cookieTimezone) {
      this.setTimezoneOffset(cookieTimezone)
    }
  }

  public updateTimezone(timezone) {
    const favs = this.tokenService.getFavorites();
    const favoritesList = favs && favs.length > 0 ? JSON.parse(this.tokenService.getFavorites()) : [];
    const json = { fav_list: favoritesList, timezone: timezone ? timezone : 'utc' };
    return this.http.post(`${environment.apiURL}/user-pref`, JSON.stringify(json), {
      headers: this.createHttpHeader(),
    });
  }

  public getTimezone() {
    const url = `${environment.apiURL}/user-pref`;
    return this.http.get(url, {
      headers: this.createHttpHeader(),
    });
  }

  public getDateWithOffset(date: string) {
    return moment(date).add(this.timezoneOffset, 'hours');
  }

  public getDateNoOffset(date: string) {
    return moment(date).subtract(this.timezoneOffset, 'hours');;
  }

  public setTimezoneOffset(timezone) {
    this.timezone = timezone;
    switch(timezone) {
      case 'utc':
        this.timezoneOffset = 0;
        break;
      case 'local':
        this.setLocalTimezoneOffset();
        break;
      case 'site':
        this.setSiteTimezoneOffset();
        break;
      default:
        this.timezoneOffset = 0;
        break; 
    }
    this.saveTimezone(this.timezoneOffset)
  }

  public getTimezoneOffset() {
    return this.timezoneOffset ? this.timezoneOffset : 0;
  }

  public saveTimezone(timezoneOffset){
    
  }

  public setSite(site) {
    this.site = site;
    if(this.timezone === 'site' && this.site) {
      this.setSiteTimezoneOffset();
    }
  }

  private setSiteTimezoneOffset() {
    this.timezoneOffset = this.timezoneOffsetList[this.site];
  }

  private setLocalTimezoneOffset() {
    const timezoneOffset = (new Date).getTimezoneOffset() //in minutes
    this.timezoneOffset = (timezoneOffset * -1) / 60;
  }

  public getDateRangeInUTC(globalDates, offset = null, dates = null) {
    const minDate = new Date();
    const localTzOffset = offset * -1 * 60 || minDate.getTimezoneOffset(); 
    const startDate = moment(minDate).subtract(7, 'days'); 
    const endDate = moment(minDate);
    let startD;
    let endD;
    let start = moment(globalDates && globalDates.startD ? globalDates.startD : startDate);
    let end = moment(globalDates && globalDates.endD ? globalDates.endD : endDate);
    
    if(dates) {
      // dates need to have 0 seconds and milliseconds for minute data of the first and last data points will be a minute later than needed
      start = start.set({second:0,millisecond:0});
      end = end.set({hour:0,minute:0});
    } else {
      // dates need to have 00:00 and 23:59 hours for the hour data chart so user can see full day of data
      start = start.set({hour:0,minute:0,second:0,millisecond:0});
      end = end.set({hour:23,minute:59,second:0,millisecond:0});   
    } 

    if(this.tokenService.getTimezone() !== 'utc') {
      start = start.add(localTzOffset, 'minutes');
      end = end.add(localTzOffset, 'minutes');
    }

    startD = start;
    endD = end;

    return {startD, endD}
  }

  public getDateCountRangeInUTC(globalDates, offset = null) {
    const minDate = new Date();
    const localTzOffset = offset * -1 * 60 || minDate.getTimezoneOffset(); 
    const startDate = moment(minDate).subtract(7, 'days'); 
    const endDate = moment(minDate);
    let startD;
    let endD;
    let start = moment(globalDates && globalDates.startD ? globalDates.startD : startDate);
    let end = moment(globalDates && globalDates.endD ? globalDates.endD : endDate);
    
    // dates need to have 00:00 and 23:00 hours for the hour data chart so user can see full day of data
    start = start.set({hour:0,minute:0,second:0,millisecond:0});
    end = end.set({date: end.date() + 1, hour:0,minute:0,second:0,millisecond:0});  

    startD = start;
    endD = end;

    return {startD, endD}
  }

  public setTimezoneOffsetList(currentCust) {
    const offsetList = {};
    currentCust['sites'].map(site => {
      offsetList[site['id']] = site['timezone_offset_hours'];
    });
    this.timezoneOffsetList = offsetList;
  }
  
  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.tokenService.toString(),
    });
  }
}
