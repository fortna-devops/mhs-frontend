import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityAreaComponent } from './facility-area.component';

describe('FacilityAreaComponent', () => {
  let component: FacilityAreaComponent;
  let fixture: ComponentFixture<FacilityAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
