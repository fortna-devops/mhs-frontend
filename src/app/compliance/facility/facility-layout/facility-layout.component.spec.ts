import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilityLayoutComponent } from './facility-layout.component';

describe('FacilityLayoutComponent', () => {
  let component: FacilityLayoutComponent;
  let fixture: ComponentFixture<FacilityLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilityLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilityLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
