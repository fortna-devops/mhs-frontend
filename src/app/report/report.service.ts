import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TokenService } from '../shared/token.service';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(private http: HttpClient, private token: TokenService) {}

  public getAlarmsCount(params) {
    const countUrl = params.count ? `&count=true` : '';
    const type_names = params['types'] ? `&type_names=${encodeURIComponent(params['types'])}` : '';
    const severities = params['colors'] ? `&colors=${encodeURIComponent(params['colors'])}` : '';
    const metrics = params['metric_ids'] ? `&metric_ids=${encodeURIComponent(params['metric_ids'])}` : '';
    // tslint:disable-next-line:max-line-length
    let urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])}&start=${params['startDate'].toISOString()}&end=${params[
    'endDate'].toISOString()}&group_by_metric=true${type_names}${severities}${metrics}${countUrl}`;
    return this.http.get<any>(urlAlarms, { headers: this.createHttpHeader() });
  }

  public getAlarms(params, parts) {
    const type_names = params['types'] ? `&type_names=${encodeURIComponent(params['types'])}` : '';
    const severities = params['colors'] ? `&colors=${encodeURIComponent(params['colors'])}` : '';
    const metrics = params['metric_ids'] ? `&metric_ids=${encodeURIComponent(params['metric_ids'])}` : '';
    // tslint:disable-next-line:max-line-length
    let urlAlarms = `${environment.apiURL}/alarms?site=${encodeURIComponent(params['site'])}&start=${params['startDate'].toISOString()}&end=${params[
    'endDate'].toISOString()}&group_by_metric=true${type_names}${severities}${metrics}`;

    const alarms = [];
    for(let i = 0; i < parts; i++) {
      const limitOffset = params.limit ? `&offset=${+params['offset'] * i}&limit=${params['limit']}` : '';
      alarms.push(this.http.get<any>(urlAlarms + limitOffset, { headers: this.createHttpHeader() }));
    }
    return forkJoin(alarms);
  }

  public getMetricList(params) {
    // tslint:disable-next-line:max-line-length
    const alarm_type_names = params['alarm_type_names'] ? `&alarm_type_names=${encodeURIComponent(params['alarm_type_names'])}` : '';
    const alarm_colors = params['alarm_colors'] ? `&alarm_colors=${encodeURIComponent(params['alarm_colors'])}` : '';
    const display_names = params['display_names'] ? `&display_names=${encodeURIComponent(params['display_names'])}` : '';
    const urlMetrics = `${environment.apiURL}/metrics?site=${encodeURIComponent(
      params['site']
    )}&lookup_days=${params['period']}&limit=${params['limit']}&offset=${params['offset']
    }${alarm_type_names}${alarm_colors}${display_names}`;
    return this.http.get<any>(urlMetrics, { headers: this.createHttpHeader() });
  }

  public getMetricCount(params) {
    // tslint:disable-next-line:max-line-length
    const alarm_type_names = params['alarm_type_names'] ? `&alarm_type_names=${encodeURIComponent(params['alarm_type_names'])}` : '';
    const alarm_colors = params['alarm_colors'] ? `&alarm_colors=${encodeURIComponent(params['alarm_colors'])}` : '';
    const display_names = params['display_names'] ? `&display_names=${encodeURIComponent(params['display_names'])}` : '';
    const urlMetrics = `${environment.apiURL}/metrics?site=${encodeURIComponent(
      params['site']
    )}&lookup_days=${params['period']}&count=true${alarm_type_names}${alarm_colors}${display_names}`;
    return this.http.get<any>(urlMetrics, { headers: this.createHttpHeader() });
  }

  public getFilters(params) {
    // tslint:disable-next-line:max-line-length
    const urlMetrics = `${environment.apiURL}/report-filters?site=${encodeURIComponent(params['site'])}`;
    return this.http.get<any>(urlMetrics, { headers: this.createHttpHeader() });
  }

  public getChartData(params) {
    // tslint:disable-next-line:max-line-length
    const url = `${environment.apiURL}/data?site=${params.site}&sample_rate=${
      params.sample_rate
    }&metric_ids=${encodeURIComponent(
      params.metric_ids
    )}&group_by=component_id&start_datetime=${params.startDate.toISOString()}&end_datetime=${params.endDate.toISOString()}`;
    return this.http.get<Array<any>>(url, { headers: this.createHttpHeader() });
  }

  private createHttpHeader() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.token.toString()
    });
  }

  public sharePDF(emailParams, activeSite) {
    const url = `${environment.apiURL}/send-email`;
    const requestBody = emailParams;

    return this.http.post(url, JSON.stringify(requestBody), {
      headers: this.createHttpHeader()
    });
  }
}
