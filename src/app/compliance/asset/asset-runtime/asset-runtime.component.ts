import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CacheService } from '../../../shared/cache.service';
import * as moment from 'moment';
import { TimezoneService } from '../../../shared/timezone/timezone.service';

@Component({
  selector: 'app-asset-runtime',
  templateUrl: './asset-runtime.component.html',
  styleUrls: ['./asset-runtime.component.scss'],
})
export class AssetRuntimeComponent implements OnInit {
  @Input() runtimeStats;
  @Input() transitions;

  public currentDayChart: any;
  public lastSevenDaysChart: any;
  public lastMonthChart: any;
  public asset: string;
  public site: string;
  public assetStatuses = {
    data: [],
    loading: false,
  };
  timeLabels: any = [];
  startDateTime: any;
  endDateTime: any;
  transitionsNum: any;
  onTime: any;
  offTime: any;

  constructor(
    private timezoneService: TimezoneService,
    private activatedRoute: ActivatedRoute,
    private cacheService: CacheService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.asset = params['asset'].replace('asset-', '');
      this.site = params['site'];
      this.getAssetStatus();
    });
  }

  private getAssetStatus() {
    const offsetTime = (date) => this.timezoneService.getDateWithOffset(date);
    this.startDateTime = moment(offsetTime(this.transitions[0]['start_date_time']));
    this.endDateTime = moment(offsetTime(this.transitions[this.transitions.length - 1]['end_date_time']));
    const durationMinutes = moment(this.endDateTime).diff(this.startDateTime, 'minutes');
    this.assetStatuses.data = [];
    this.transitionsNum = this.transitions.length - 1;

      this.assetStatuses.data = this.transitions.map((assetStatus) => {
        let obj = {};
        let start = moment(offsetTime(assetStatus['start_date_time'])).set({second: 0, millisecond: 0})
        let end = moment(offsetTime(assetStatus['end_date_time'])).set({second: 0, millisecond: 0})
        obj['start'] = start.format('MMM DD HH:mm');
        obj['end'] = end.format('MMM DD HH:mm');
        obj['status'] = assetStatus['status']; 
        obj['marginTime'] = start.diff(this.startDateTime, 'minutes');
        obj['margin'] =  (obj['marginTime'] / durationMinutes) * 100;
        obj['duration'] = end.diff(start, 'minutes');
        obj['durationTime'] = this.formatDurationSeconds(obj['duration']);
        obj['percent'] = (obj['duration'] / durationMinutes) * 100; 
        return obj;
      });

      let startTime = this.startDateTime.clone();
      const dateRange = [];
      let day = startTime;

      while (day <= this.endDateTime) {
        dateRange.push(day.set({second: 0, millisecond: 0}));
        day = day.clone().add(12, 'h');
      }

      this.timeLabels = dateRange.map((day, index) => {
        let obj = {};
        const duration = moment(dateRange[index + 1]).diff(day, 'minutes');
        obj['percent'] = (duration / durationMinutes) * 100;
        obj['label'] = moment(day).format('MMM DD HH:mm');
        return obj;
      })

      const sumHours = this.runtimeStats['on_hours'] + this.runtimeStats['off_hours'];
      this.onTime = {
        duration: this.formatDuration(this.runtimeStats['on_hours']),
        percent: (this.runtimeStats['on_hours'] / sumHours) * 100
      };
      this.offTime = {
        duration: this.formatDuration(this.runtimeStats['off_hours']),
        percent: (this.runtimeStats['off_hours'] / sumHours) * 100
      };
    
  }

  formatDuration(decimalHours) {
    const hours = parseInt(decimalHours);
    const minutes = Math.round((decimalHours - hours) * 60);

    return  hours.toString() + ' hrs ' + (minutes > 0 ? minutes.toString() + ' mins' : '');
  }

  formatDurationSeconds(timestamp) {
    const hours = Math.floor(timestamp / 60 / 60);
    const minutes = Math.floor(timestamp / 60) - (hours * 60);

    return  hours.toString() + ' hrs ' + (minutes > 0 ? minutes.toString() + ' mins' : '');
  }
}
